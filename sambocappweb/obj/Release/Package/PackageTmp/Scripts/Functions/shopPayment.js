﻿//const { momentProperties } = require("../moment-with-locales");

$(document).ready(function () {
    GetShopPayment();
});

$("#btnprivacy").click(function () {
    document.getElementById('btnshopPayment').innerText = "Save";
    $("#txtbtnShopPayment").val("Save");
});
var tableShopPayment = [];
function GetShopPayment() {
    tableCase = $('#tblShopPayment').DataTable({
        ajax: {
            url: "/api/ShopPayment",
            dataSrc: ""
        },
        columns: [
            //{
            //    data: "id",
            //},
            {
                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "shop"
            },
            //{
            //    data: "paytype"
            //},
            {
                data: "startdate",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "enddate",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "amount"
            },
            {
                data: "note"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button type='button' onclick='ShopPaymentEdits(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>"
                        + "<button type='button' onclick='ShopPaymentDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": true
    });
}
function readURLShopPay(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#shopPayPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function BtnShopPaymentAction() {
    //alert('hi');
    var action = '';
    action = document.getElementById('btnShopPayment').innerText;
    //alert(action);
    if (action == "Add New") {
        document.getElementById('btnShopPayment').innerText = 'Save';
        EnableControl();
        $('#shopid').focus();
        $('#startDate').val(moment().format('YYYY-MM-DD'));
        $('#endDate').val(moment().format('YYYY-MM-DD'));
        $('#date').val(moment().format('YYYY-MM-DD'));
        $('#amount').val('0');
        $('#currency').val('0');
        $('#qtymonth').val('0');
    }
    if (action === "Save") {
        
        var data = new FormData();
        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("screenshot", files[0]);
        }
        data.append("date", $('#date').val());
        data.append("shopid", $('#shopid').val());
        data.append("paytype", $('#payType').val());
        data.append("startdate", $('#startDate').val());
        data.append("enddate", $('#endDate').val());
        data.append("amount", $('#amount').val());
        data.append("note", $('#note').val());
        data.append("qtymonth", $('#qtymonth').val());
        data.append("currentcyid", $('#currency').val());
        $.ajax({
            url: "/api/ShopPayment",
            data: data,
            type: "POST",
            contentType: false,
            processData: false,
            success: function (result) {
                toastr.success("New Banner has been Created", "Server Respond");
                $('#tblShopPayment').DataTable().ajax.reload();
                $("#shopPaymentModel").modal('hide');
                ClearControl();

            },
            error: function (errormesage) {
                $('#bannerdate').focus();
                toastr.error("This Name is exist in Database", "Server Respond")
            }

        });
    }
    else if (action === "Update") {
        
        var data = new FormData();
        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("screenshot", files[0]);
        }
        data.append("date", $('#date').val());
        data.append("id", $('#shopPaymentid').val());
        data.append("shopid", $('#shopid').val());
        data.append("paytype", $('#payType').val());
        data.append("startdate", $('#startDate').val());
        data.append("enddate", $('#endDate').val());
        data.append("amount", $('#amount').val());
        data.append("note", $('#note').val());
        data.append("currentcyid", $('#currency').val());
        data.append("qtymonth", $('#qtymonth').val());
        data.append("file_old", $("#file_old").val());

        $.ajax({
            url: "/api/ShopPayment/" + $("#shopPaymentid").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("exchang has been Updated", "Server Respond");
                $('#tblShopPayment').DataTable().ajax.reload();
                document.getElementById('btnShopPayment').innerText = "Add New";
                $("#shopPaymentModel").modal('hide');
                ClearControl();

            },
            error: function (errormesage) {
                toastr.error("exchang hasn't Updated in Database", "Server Respond")
            }
        });
    }
    
}
function ShopPaymentEdits(id) {
    document.getElementById('btnShopPayment').innerText = 'Update';
    $("#shopPaymentModel").modal('show');
    //action = document.getElementById('btnshopPayment').innerText = "Update";
    $.ajax({
        url: "/api/ShopPayment/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#shopPaymentid').val(result.id);
            //var date = new Date(result.date);
            //var datetime = moment(date).format('YYYY-MM-DD');
            ////$('#date').val(datetime);
            var pd = moment(result.date).format("YYYY-MM-DD");
            $('#date').val(pd);
            $('#shopid').val(result.shopid);
            $('#payType').val(result.paytype);
            var sd = moment(result.startdate).format("YYYY-MM-DD");
            $('#startDate').val(sd);
            var ed = moment(result.enddate).format("YYYY-MM-DD");
            $('#endDate').val(ed);
            $('#amount').val(result.amount);
            $('#note').val(result.note);
            $('#currency').val(result.currentcyid);
            $('#qtymonth').val(result.qtymonth);
            $('#file_old').val(result.screenshot);

            if (result.screenshot == "") {
                $("#shopPayPhoto").attr('src', '../Images/company.png');
            } else {
                $("#shopPayPhoto").attr('src', '../Images/' + result.screenshot);
            }
            //$("#txtbtnShopPayment").val("Update");
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happenn.", "Server Response");
        }
    });
}
function ShopPaymentDelete(id) {
    // alert('hi');
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/ShopPayment/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblShopPayment').DataTable().ajax.reload();
                        toastr.success("Item Deleted successfully!", "Service Response");
                        tblShopPayment.ajax.reload();
                        window.location.reload();
                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });

}
function DisableControl() {
    document.getElementById('file').disabled = true;
    document.getElementById('shopid').disabled = true;
    document.getElementById('payType').disabled = true;
    document.getElementById('startDate').disabled = true;
    document.getElementById('endDate').disabled = true;
    document.getElementById('amount').disabled = true;
    document.getElementById('note').disabled = true;

}
function EnableControl() {
    document.getElementById('file').disabled = false;
    document.getElementById('shopid').disabled = false;
    document.getElementById('payType').disabled = false;
    document.getElementById('startDate').disabled = false;
    document.getElementById('endDate').disabled = false;
    document.getElementById('amount').disabled = false;
    document.getElementById('note').disabled = false;

}
function ClearControl() {
    //$('#date').val('');
    $('#shopid').val('');
    $('#payType').val('');
    $('#startDate').val('');
    $('#endDate').val('');
    $('#amount').val('');
    $('#note').val('');
    $('#shopPayPhoto').attr('src', '../Images/company.png');
}
function ShopPaymentAction() {
    document.getElementById('btnShopPayment').innerText = "Add New";
    DisableControl();
    ClearControl();
}