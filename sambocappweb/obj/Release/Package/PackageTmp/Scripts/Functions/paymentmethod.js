﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });

    $('#PaymentMethodModal').on('show.bs.modal', function () {
        //alert('hi');
        GetPaymentMethod();
        document.getElementById('methodname').disabled = true;
        document.getElementById('btnMethod').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        //$('#methodname').val('');
    });
});

var tableCase = [];

function GetPaymentMethod() {
    tableCase = $('#PaymentMethodTable').DataTable({
        ajax: {
            url: "/api/paymentmethods",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "paymentmethodname"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='MethodEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='MethodDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function MethodAction() {
    var action = '';
    action = document.getElementById('btnMethod').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#methodname').val().trim() === "") {
            $('#methodname').css('border-color', 'red');
            $('#methodname').focus();
            toastr.info("Please enter Pament Method's name", "Required");
        }
        else {
            $('#methodname').css('border-color', '#cccccc');

            var data = {
                paymentmethodname: $('#methodname').val(),
            };
            $.ajax({
                url: "/api/paymentmethods",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableCase.ajax.reload();
                    document.getElementById('methodname').disabled = true;
                    document.getElementById('btnMethod').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#methodname').val('');
                },
                error: function (errormessage) {
                    toastr.error("This case is already exists.", "Server Response");
                    document.getElementById('methodname').disabled = true;
                    document.getElementById('btnMethod').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#methodname').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('methodname').disabled = false;
        document.getElementById('btnMethod').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#methodname').val('');
        $('#methodname').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#methodname').css('border-color', '#cccccc');

        var data = {
            id: $('#methodid').val(),
            paymentmethodname: $('#methodname').val(),
        };
        $.ajax({
            url: "/api/paymentmethods/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                document.getElementById('methodname').disabled = true;
                document.getElementById('btnMethod').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#methodname').val('');
            },
            error: function (errormessage) {
                toastr.error("This Payment Method is already exists.", "Server Response");
                document.getElementById('methodname').disabled = true;
                document.getElementById('btnMethod').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#methodname').val('');
            }
        });
    }
}

function MethodEdit(id) {

    $('#methodname').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/paymentmethods/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#methodid').val(result.id);
            $('#methodname').val(result.paymentmethodname);
            document.getElementById('btnMethod').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('methodname').disabled = false;
            $('#methodname').focus();

        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function MethodDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/paymentmethods/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This case is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}