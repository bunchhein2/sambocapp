﻿$(document).ready(function () {

    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });

    GetDashboardPerformance($("#filterDepartmentDashboard option:selected").text());

    $('#filterDepartmentDashboard').on('change', function () {
        GetDashboardPerformance($("#filterDepartmentDashboard option:selected").text());
    })

});

var tableDashboards = [];

function GetDashboardPerformance(department) {
    tableDashboards = $('#performanceTable').DataTable({
        ajax: {
            url: "/api/dashboards?name=" + department,
            dataSrc: ""
        },
        columns: [
            {
                data: "staffName"
            },
            {
                data: "ct"
            },
            {
                data: "cmct"
            },
            {
                data: "tmct"
            },
            {
                data: "smict"
            },
            {
                data: "oyct"
            }
        ],
        destroy: true,
        responsive: true,
        "order": [[5, "desc"]],
        "language": {
            "search": "<span class='glyphicon glyphicon-search'></span> <span class='kh'>ស្វែងរក</span> Search:"
        }
    });
}