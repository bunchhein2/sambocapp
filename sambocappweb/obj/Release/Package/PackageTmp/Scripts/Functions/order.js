﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("#loadingGif").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("#loadingGif").removeClass('show');
    });
    GetOrders();
    $('#OrderModal').on('show.bs.modal', function () {
        exchangname();
    });
});
function ShopInfo() {
    
    $.get("/api/orders/?a=1&b=2&c=3&d=4", function (data, status) {
        //// alert("Data: " + data + "\nStatus: " + status);
        //var arrayData = data.split();
        var ex = Math.round(data + 0);
        $('#exchangename').val(ex);

    });
}
$("#shopid").change(function () {
    $.get("/api/orders/?aa=1&bb=2&cc=3", function (data, status) {
        var exid = Math.round(data + 0);
        $('#exchangename1').val(exid);

    });

});
$("#bankname").change(function () {
    
    var bankname = $(this).val();
    var paymenttype = $("#paymenttypeid").val();
    
    var paymenttypeString = bankname;
    $("#paymenttypeid").val(paymenttypeString);
});
function exchangname(){

    $.get("/api/orders/?aa=1&bb=2&cc=3", function (data, status) {
        var exid = Math.round(data + 0);
        $('#exchange').val(exid);

    });
}
$('#btnModalAddUpdateOrder').click(function () {
    $('#btnOrderUpdadate').hide();
    $('#btnOrderSaveUpdate').show();
    $("input").prop('disabled', false);
    $("textarea").prop('disabled', false);
    $("select").prop('disabled', false);

})

var tableOrder = [];
function GetOrders() {
    tableOrder = $('#orderTable').DataTable({
        ajax: {
            url: "/api/orders",
            dataSrc: ""
        },
        columns: [

            
            {
                data: "id",
                render: function (data) {

                    return "" + ("");
                }
            },
            {
                data: "invoiceNo"
            },
            {
                data: "shopName"
            },

            {
                data: "customerId"
            },

            {
                data: "phone"
            },
            {
                data: "amountTobePaid"
            },
            {
                data: "currencyid"
            },
            {
                data: "Date",
                render: function (data) {
                    return moment(data).format('DD-MMM-YYYY');
                }
            },
            {
                data: "status"
            },
            {
                data: "id",
                render: function (data, type, meta, full) {
                    return "<button onclick='OrderDetailid(" + data + ")'  class='btn btn-primary btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-shopping-cart'></span>Order</button>" +
                        "<button onclick='OrderEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span>Edit</button>" +
                        "<button onclick='OrderDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span>Delete</button>";
                   
                },

            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false,
        "paging": true,
        lengthMenu: [
            [50, -1],
            [100, 'All'],
        ],
    });
}

function SaveOrderAction() {
    var action = '';
    //action = document.getElementById('btnOrderSaveUpdate').innerText;
    action = $("#txtButtonOrder").val();
    // រក្សាទុក / Save
    if (action === "save") {
        var response = validationForm();
        if (response == false) {
            return false;
        }
        DisableFormOrder();
        var data = new FormData();
        var files = $("#file").get(0).files;

        if (files.length > 0) {
            data.append("ReceiptUpload", files[0]);
        }
        var files = $("#qrcodeshopname").get(0).files;
        if (files.length > 0) {
            data.append("QrcodeShopName", files[0]);
        }
        data.append("InvoiceNo", $('#invoiceno').val());
        data.append("ShopId", $('#shopid').val());
        data.append("CustomerId", $('#customerid').val());
        data.append("Phone", $('#phone').val());
        data.append("BankName", $('#bankname').val());
        data.append("AccountNumber", $('#accountnumber').val());
        data.append("AccountName", $('#accountname').val());
        data.append("CurrentLocation", $('#currentlocation').val());
        data.append("DeliveryTypeIn", $('#abc').val());
        data.append("PaymentType", $('#paymenttype').val());
        data.append("AmountTobePaid", $('#amounttobepaid').val());
        data.append("ExchangeId", $('#exchangename').val());
        data.append("Date", $('#date').val());
        data.append("Status", $('#status').val());

        var ajaxRequest = $.ajax({
            url: "/api/orders",
            data: data,
            contentType: false,
            processData: false,
            type: "POST",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                tableOrder.ajax.reload();
                ResetForm();
                //DisableFormOrder();
                //document.getElementById('btnOrderSaveUpdate').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#orderModal').hide();
                //$("#txtButtonOrder").val("addnew");
            },
            error: function (errormessage) {
                toastr.error("This order is already exists.", "Server Response");
                $('#invoiceno').focus();
            }
        });
    }
    // បង្កើតថ្មី / Add New
    else if (action === "addnew") {

        //EnableForm();
        //document.getElementById('btnOrderSaveUpdate').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#invoiceno').focus();
        $("#txtButtonOrder").val("save");
    }
    // កែប្រែ / Update
    else if (action === "update") {

        var response = validationForm();
        if (response == false) {
            return false;
        }

        var data = new FormData();
        var files = $("#file").get(0).files;

        if (files.length > 0) {
            data.append("ReceiptUpload", files[0]);
        }
        var files = $("#qrcodeshopname").get(0).files;
        if (files.length > 0) {
            data.append("QrcodeShopName", files[0]);
        }
        data.append("receip_old", $('#receip_old').val());
        data.append("qrcode_old", $('#qrcode_old').val());
        data.append("Id", $("#orderid").val());
        data.append("InvoiceNo", $('#invoiceno').val());
        data.append("ShopId", $('#shopid').val());
        data.append("CustomerId", $('#customerid').val());
        data.append("Phone", $('#phone').val());
        data.append("BankName", $('#bankname').val());
        data.append("AccountNumber", $('#accountnumber').val());
        data.append("AccountName", $('#accountname').val());
        data.append("CurrentLocation", $('#currentlocation').val());
        data.append("DeliveryTypeIn", $('#abc').val());
        data.append("PaymentType", $('#paymenttype').val());
        data.append("AmountTobePaid", $('#amounttobepaid').val());
        data.append("ExchangeId", $('#exchangename').val());
        data.append("Date", $('#date').val());
        data.append("Status", $('#status').val());

        var ajaxRequest = $.ajax({
            url: "/api/orders/" + $('#orderid').val(),
            type: "PUT",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableOrder.ajax.reload();
                ResetForm();
                $('#OrderModal').modal('hide');

            },
            error: function (errormessage) {
                toastr.error("This order is already exists.", "Server Response");
            }
        });
    }
}
let exchangenameex = document.getElementById("exchangename1");
let exchangenameexid1 = document.getElementById("exchangeid1");
function OrderEdit(Id) {
    exchangenameex.style.display = "none";
    exchangenameexid1.style.display = "none";
    $('#btnOrderSaveUpdate').hide();
    $('#btnOrderUpdadate').show();
    $("#txtButtonOrder").val("update");
    EnableForm();
    $.ajax({
        url: "/api/orders/" + Id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#orderid').val(result.id);
            $('#invoiceno').val(result.invoiceNo);
            var pd = moment(result.date).format("YYYY-MM-DD");
            $('#date').val(pd);
            $('#shopid').val(result.shopId);
            $('#customerid').val(result.customerId);
            $('#abc').val(result.deliveryTypeIn);
            $('#currentlocation').val(result.currentLocation);
            $('#phone').val(result.phone);
            $('#paymenttype').val(result.paymentType);
            //$('#qrcodeshopname').val(result.qrcodeShopName);
            $('#bankname').val(result.bankName);
            $('#accountnumber').val(result.accountNumber);
            $('#accountname').val(result.accountName);
            $('#amounttobepaid').val(result.amountTobePaid);
            $('#exchangename').val(result.exchangeId);
            $('#status').val(result.status);

            if (result.Photo == "") {
                $('#receiptupload').attr('src', '../Content/product.jpg');
            }
            else {
                $('#receiptupload').attr('src', '../Images/' + result.receiptUpload);
            }
            if (result.qrcodeShopName == "") {
                $('#qrcodeimg').attr('src', '../Content/product.jpg');
            }
            else {
                $('#qrcodeimg').attr('src', '../Images/' + result.qrcodeShopName);
            }


            $("#receip_old").val(result.receiptUpload);
            $("#qrcode_old").val(result.qrcodeShopName);

            $("#OrderModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function OrderDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/orders/" + id,
                    method: "DELETE",
                    success: function () {
                        tableOrder.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This order is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function OrderDetailid(Id) {
    $("#iddiatal").val(Id);
    $("#orderid2").val(Id);
    $("#OrderdetailFrm").modal('show');
}

function ResetForm() {
    $('#invoiceno').val('');
    $('#date').val('');
    $('#shopid').val('');
    $('#customerid').val('');
    $('#abc').val('');
    $('#currentlocation').val('');
    $('#phone').val('');
    $('#paymenttype').val('');
    $('#qrcodeshopname').attr('src', '../Content/product.jpg');
    $('#bankname').val('');
    $('#accountnumber').val('');
    $('#accountname').val('');
    $('#receiptupload').val('');
    $('#amounttobepaid').val('');
    $('#exchangename').val('');
    $('#receiptupload').attr('src', '../Content/product.jpg');
}

function DisableFormOrder() {
    document.getElementById('invoiceno').disabled = true;
    document.getElementById('date').disabled = true;
    document.getElementById('shopid').disabled = true;
    document.getElementById('customerid').disabled = true;
    document.getElementById('abc').disabled = true;
    document.getElementById('currentlocation').disabled = true;
    document.getElementById('phone').disabled = true;
    document.getElementById('paymenttype').disabled = true;
    document.getElementById('qrcodeshopname').disabled = true;
    document.getElementById('bankname').disabled = true;
    document.getElementById('accountnumber').disabled = true;
    document.getElementById('accountname').disabled = true;
    document.getElementById('receiptupload').disabled = true;
    document.getElementById('amounttobepaid').disabled = true;
    document.getElementById('exchangename').disabled = true;
    document.getElementById('status').disabled = true; 
    document.getElementById('exchangename1').disabled = true;
}

function EnableForm() {
    document.getElementById('invoiceno').disabled = false;
    document.getElementById('date').disabled = false;
    document.getElementById('shopid').disabled = false;
    document.getElementById('customerid').disabled = false;
    document.getElementById('abc').disabled = false;
    document.getElementById('currentlocation').disabled = false;
    document.getElementById('phone').disabled = false;
    document.getElementById('paymenttype').disabled = false;
    document.getElementById('qrcodeshopname').disabled = false;
    document.getElementById('bankname').disabled = false;
    document.getElementById('accountnumber').disabled = false;
    document.getElementById('accountname').disabled = false;
    document.getElementById('receiptupload').disabled = false;
    document.getElementById('amounttobepaid').disabled = false;
    document.getElementById('exchangename').disabled = false;
    document.getElementById('status').disabled = false;
    document.getElementById('exchangename1').disabled = false;
}

function validationForm() {
    var isValid = true;
    if ($('#invoiceno').val().trim() == "") {
        $('#invoiceno').css('border-color', 'red');
        $('#invoiceno').focus();
        toastr.info('Please enter invoice no.');
        isValid = false;
    } else {
        $('#invoiceno').css('border-color', '#cccccc');
        if ($('#shopid').val().trim() == "") {
            $('#shopid').css('border-color', 'red');
            $('#shopid').focus();
            toastr.info('Please choose shop.');
            isValid = false;
        } else {
            $('#shopid').css('border-color', '#cccccc');
            //    if ($('#qrcodeshopname').val().trim() == "") {
            //        $('#qrcodeshopname').css('border-color', 'red');
            //        $('#qrcodeshopname').focus();
            //        toastr.info('Please enter qrcode shop name.');
            //        isValid = false;
            /*}*/
            //    else {
            //        $('#qrcodeshopname').css('border-color', '#cccccc');
            if ($('#customerid').val().trim() == "") {
                $('#customerid').css('border-color', 'red');
                $('#customerid').focus();
                toastr.info('Please choose customer.');
                isValid = false;
            } else {
                $('#customerid').css('border-color', '#cccccc');
                if ($('#phone').val().trim() == "") {
                    $('#phone').css('border-color', 'red');
                    $('#phone').focus();
                    toastr.info('Please enter phone.');
                    isValid = false;
                }
                else {
                    $('#phone').css('border-color', '#cccccc');
                    if ($('#bankname').val().trim() == "") {
                        $('#bankname').css('border-color', 'red');
                        $('#bankname').focus();
                        toastr.info('Please enter bank name.');
                        isValid = false;
                    }
                    else {
                        $('#bankname').css('border-color', '#cccccc');
                        if ($('#accountnumber').val().trim() == "") {
                            $('#accountnumber').css('border-color', 'red');
                            $('#accountnumber').focus();
                            toastr.info('Please enter account number.');
                            isValid = false;
                        }
                        else {
                            $('#accountnumber').css('border-color', '#cccccc');
                            if ($('#accountname').val().trim() == "") {
                                $('#accountname').css('border-color', 'red');
                                $('#accountname').focus();
                                toastr.info('Please enter account name.');
                                isValid = false;
                            }
                            else {
                                $('#accountname').css('border-color', '#cccccc');
                                if ($('#currentlocation').val().trim() == "") {
                                    $('#currentlocation').css('border-color', 'red');
                                    $('#currentlocation').focus();
                                    toastr.info('Please enter current location.');
                                    isValid = false;
                                }
                                else {
                                    $('#currentlocation').css('border-color', '#cccccc');
                                    if ($('#abc').val().trim() == "") {
                                        $('#abc').css('border-color', 'red');
                                        $('#abc').focus();
                                        toastr.info('Please enter delivery type in.');
                                        isValid = false;
                                    } else {
                                        $('#abc').css('border-color', '#cccccc');
                                        if ($('#paymenttype').val().trim() == "") {
                                            $('#paymenttype').css('border-color', 'red');
                                            $('#paymenttype').focus();
                                            toastr.info('Please enter payment type.');
                                            isValid = false;
                                        }
                                        else {
                                            $('#paymenttype').css('border-color', '#cccccc');
                                            if ($('#amounttobepaid').val().trim() == "") {
                                                $('#amounttobepaid').css('border-color', 'red');
                                                $('#amounttobepaid').focus();
                                                toastr.info('Please enter amount to be paid.');
                                                isValid = false;
                                            }
                                            else {
                                                $('#amounttobepaid').css('border-color', '#cccccc');
                                                if ($('#exchangename').val().trim() == "") {
                                                    $('#exchangename').css('border-color', 'red');
                                                    $('#exchangename').focus();
                                                    toastr.info('Please choose exchange id.');
                                                    isValid = false;
                                                }
                                                else {
                                                    $('#exchangename').css('border-color', '#cccccc');
                                                    if ($('#date').val().trim() == "") {
                                                        $('#date').css('border-color', 'red');
                                                        $('#date').focus();
                                                        toastr.info('Please choose date.');
                                                        isValid = false;
                                                    } else {
                                                        $('#date').css('border-color', '#cccccc');
                                                        if ($('#status').val().trim() == "") {
                                                            $('#status').css('border-color', 'red');
                                                            $('#status').focus();
                                                            toastr.info('Please choose status.');
                                                            isValid = false;
                                                        }
                                                        else {
                                                            $('#status').css('border-color', '#cccccc');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //}
        }
    }
    return isValid;
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#receiptupload').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function readURLQR(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#qrcodeimg').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$('#btnCloseOrder').click(function () {
    $('#invoiceno').val('');
    $('#date').val('');
    $('#shopid').val('');
    $('#customerid').val('');
    $('#abc').val('');
    $('#currentlocation').val('');
    $('#phone').val('');
    $('#paymenttype').val('');
    $('#qrcodeimg').attr('src', '../Content/product.jpg');
    $('#bankname').val('');
    $('#accountnumber').val('');
    $('#accountname').val('');
    $('#receiptupload').val('');
    $('#amounttobepaid').val('');
    $('#exchangename').val('');
    $('#status').val('');
    $('#exchangename1').val('');
    $('#receiptupload').attr('src', '../Content/product.jpg');

    $('#OrderModal').modal('hide');
});
function Closebtn() {
    $('#invoiceno').val('');
    $('#date').val('');
    $('#shopid').val('');
    $('#customerid').val('');
    $('#abc').val('');
    $('#currentlocation').val('');
    $('#phone').val('');
    $('#paymenttype').val('');
    $('#qrcodeimg').attr('src', '../Content/product.jpg');
    $('#bankname').val('');
    $('#accountnumber').val('');
    $('#accountname').val('');
    $('#receiptupload').val('');
    $('#amounttobepaid').val('');
    $('#exchangename1').val('');
    $('#exchangename').val('');
    $('#status').val('');
    $('#receiptupload').attr('src', '../Content/product.jpg');
};
