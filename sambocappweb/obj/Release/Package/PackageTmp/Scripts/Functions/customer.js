﻿$(document).ready(function () {
    let Display = document.getElementById("tblCustomer_paginate");
    //Display.style.display = "none";
    GetCompany();
    //$('#companyModal').on('show.bs.modal', function () {

    //    //document.getElementById('companyTypeName').disable = true;
    //});
    $('#customerModal').on('show.bs.modal', function () {
        
        //$.get("/api/Customers/?aa=1&bb=2", function (data, status) {
        //    var exid = Math.round(data + 0);
        //    $('#tokenid').val(exid);

        //});
        //document.getElementById('tokenid').disable = true;
    });
});


var tableCompany = [];

function GetCompany() {
    tableCompany = $('#tblCustomer').dataTable({
        ajax: {
            url: "/api/Customers",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id",
                    render: function (data) {

                        return "" + ("").slice(-4);
                    }
                },
                {
                    data: "tokenid"
                },
                {
                    data: "customerName"
                },
                {
                    data: "gender"
                },
                {
                    data: "currentLocation"
                },
                {
                    data: "phone"
                },
                {
                    data: "date",
                    render: function (data) {
                        return moment(data).format("DD-MMM-YYYY");
                    }
                },

                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='CustomerEdit (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='CustomerDelete (" + data + ")' class='btn btn-danger btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],

    });
}

function CustomerAction() {
    
    var action = '';
    action = document.getElementById('btnSaveCustomer').innerText;


    if (action == "Save") {

        Validate();
        var res = ValidateCustomer();

        if (res == false) {
            return false;
        }
        if ($(' #currentlocation').val().trim() == "Select Location") {
            $(' #currentlocation').css('border-color', 'red');
        } else {
            $(' #currentlocation').css('border-color', '#cccccc');
        }
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("imageProfile", files[0]);
        }
        data.append("date", $('#date').val());
        data.append("phone", $('#phone').val());
        data.append("tokenid", $('#tokenid').val());
        data.append("currentLocation", $('#currentlocation').val());
        data.append("customerName", $('#customerName').val());
        data.append("gender", $('#gender').val());
        data.append("password", $('#password').val());

        $.ajax({
            url: "/api/Customers",
            data: data,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("New Customer has been Created", "Server Respond");
                $('#tblCustomer').DataTable().ajax.reload();
                // $('#customerName').val('');
                $("#customerModal").modal('hide');
                ClearControl();
            },
            error: function (errormesage) {
                //$('#date').focus();
                toastr.error("This Name is exist in Database", "Server Respond")
            }

        });

    } else if (action == "Update") {
        //alert('hi');
        var res = ValidateCustomer();

        if (res == false) {
            return false;
        }
        if ($(' #currentlocation').val().trim() == "Select Location") {
            $(' #currentlocation').css('border-color', 'red');
        } else {
            $(' #currentlocation').css('border-color', '#cccccc');
        }
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("imageProfile", files[0]);
        }

        data.append("id", $('#customerID').val());
        data.append("date", $('#date').val());
        data.append("phone", $('#phone').val());
        data.append("tokenid", $('#tokenid').val());
        data.append("currentLocation", $('#currentlocation').val());
        data.append("customerName", $('#customerName').val());
        data.append("gender", $('#gender').val());
        data.append("password", $('#password').val());
        data.append("photo_old", $('#photo_old').val());
        $.ajax({
            url: "/api/Customers/" + $("#customerID").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Customer has been Updated", "Server Respond");
                $('#tblCustomer').DataTable().ajax.reload();
                $("#customerModal").modal('hide');
            },
            error: function (errormesage) {
                toastr.error("Customer hasn't Updated in Database", "Server Respond")
            }
        });
    }
}

function CustomerEdit(id) {
    action = document.getElementById('btnSaveCustomer').innerText = "Update";

    $.ajax({
        url: "/api/Customers/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#customerID').val(result.id);

            ///console.log(result.id);
            var date = new Date(result.date);
            var datetime = moment(date).format('YYYY-MM-DD');
            $("#date").val(datetime);
            $("#phone").val(result.phone);
            $("#tokenid").val(result.tokenid);
            $("#currentlocation").val(result.currentLocation);
            $("#customerName").val(result.customerName);
            $("#gender").val(result.gender);
            $("#password").val(result.password);

            if (result.imageProfile == "") {
                $("#customerPhoto").attr('src', '../Images/company.png');
            } else {
                $("#customerPhoto").attr('src', '../Images/' + result.imageProfile);
            }
            $("#photo_old").val(result.imageProfile);
            $("#customerModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });

}

function CustomerDelete(id) {
    // alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Customers/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblCustomer').DataTable().ajax.reload();
                        toastr.success("Customer Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Customer Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}

//function DisableControl() {
//    document.getElementById('date').disabled = true;
//    document.getElementById('phone').disabled = true;
//    document.getElementById('tokenid').disabled = true;
//    document.getElementById('currentlocation').disabled = true;
//    document.getElementById('customerName').disabled = true;

//}

//function EnableControl() {
//    document.getElementById('date').disabled = false;
//    document.getElementById('phone').disabled = false;
//    document.getElementById('tokenid').disabled = false;
//    document.getElementById('currentlocation').disabled = false;
//    document.getElementById('customerName').disabled = false;

//}

function ClearControl() {
    $('#date').val('');
    $('#phone').val('');
    $('#tokenid').val('');
    $('#currentlocation').val('');
    $('#customerName').val('');
    $('#customerPhoto').attr('src', '../Images/company.png');
    

}

function AddnewAction() {
    $('#date').val(moment().format('YYYY-MM-DD'));
    document.getElementById('date').disable = true;
    document.getElementById('btnSaveCustomer').innerText = 'Save';
    
}

function Validate() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#date').val().trim() === "") {
        $('#date').css('border-color', 'red');
        $('#date').focus();
        toastr.info("Please enter date", "Required");
    }
    else {
        $('#date').css('border-color', '#cccccc');
        if ($('#phone').val().trim() === "") {
            $('#phone').css('border-color', 'red');
            $('#phone').focus();
            toastr.info("Please enter your phone", "Required");
        }
        else {
            $('#phone').css('border-color', '#cccccc');
        }
    }
    return isValid;
}

function ValidateCustomer() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#date').val().trim() == "") {
        $(' #date').css('border-color', 'red');
        $(' #date').focus();
        isValid = false;
    } else {
        $(' #date').css('border-color', '#cccccc');
        $(' #date').focus();
    }
    if ($(' #phone').val().trim() == "") {
        $(' #phone').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #phone').css('border-color', '#cccccc');

    }
    if ($(' #currentlocation').val().trim() == "Select Location") {
        $(' #currentlocation').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #currentlocation').css('border-color', '#cccccc');
    }
    if ($(' #customerName').val().trim() == "") {
        $(' #customerName').css('border-color', 'red');
        isValid = false;
    } else {
        $(' #customerName').css('border-color', '#cccccc');
    }
    //if ($(' #phone').val().trim() == "") {
    //    $(' #phone').css('border-color', 'red');
    //    isValid = false;
    //} else {
    //    $(' #phone').css('border-color', '#cccccc');
    //}

    //return isValid;
}

function readURLCustomer(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#customerPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}