﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    onPopulateCaseType();
    //$('#civilcaseModal').on('show.bs.modal', function () {
    //    // Bind Cases Select Options
    //    onPopulateCaseType();
    //});


    $('#casetypeModal').on('show.bs.modal', function () {
        GetCasesType();
        document.getElementById('casetypeName').disabled = true;
        document.getElementById('btnCaseTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#casetypeName').val('');
    });
});

var tableCase = [];

function onPopulateCaseType() {
    $.ajax({
        url: "/api/casetypes?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            //console.log(data);
            $("#zcasetypeid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function GetCasesType() {
    tableCase = $('#CaseTypeTable').DataTable({
        ajax: {
            url: "/api/casetypes",
            dataSrc: ""
        },
        columns: [
            {
                data: "name"
            },
            {
                data: "note"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='CaseTypeEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='CaseTypeDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function CaseTypeAction() {
    var action = '';
    action = document.getElementById('btnCaseTypeAction').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#casetypeName').val().trim() === "") {
            $('#casetypeName').css('border-color', 'red');
            $('#casetypeName').focus();
            toastr.info("Please enter Case's name", "Required");
        }
        else {
            $('#casetypeName').css('border-color', '#cccccc');

            var data = {
                name: $('#casetypeName').val(),
                note: $('#casetypeNote').val()
            };
            $.ajax({
                url: "/api/casetypes",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableCase.ajax.reload();
                    onPopulateCaseType();
                    document.getElementById('casetypeName').disabled = true;
                    document.getElementById('casetypeNote').disabled = true;
                    document.getElementById('btnCaseTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#casetypeName').val('');
                    $('#casetypeNote').val('');
                },
                error: function (errormessage) {
                    toastr.error("This case is already exists.", "Server Response");
                    document.getElementById('casetypeName').disabled = true;
                    document.getElementById('casetypeNote').disabled = true;
                    document.getElementById('btnCaseTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#casetypeName').val('');
                    $('#casetypeNote').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('casetypeName').disabled = false;
        document.getElementById('casetypeNote').disabled = false;
        document.getElementById('btnCaseTypeAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#casetypeName').val('');
        $('#casetypeNote').val('');
        $('#casetypeName').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#casetypeName').css('border-color', '#cccccc');

        var data = {
            Id: $('#casetypeId').val(),
            name: $('#casetypeName').val(),
            note: $('#casetypeNote').val()
        };
        $.ajax({
            url: "/api/casetypes/" + data.Id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateCaseType();
                document.getElementById('casetypeName').disabled = true;
                document.getElementById('casetypeNote').disabled = true;
                document.getElementById('btnCaseTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#casetypeName').val('');
                $('#casetypeNote').val('');
            },
            error: function (errormessage) {
                toastr.error("This department is already exists.", "Server Response");
                document.getElementById('casetypeName').disabled = true;
                document.getElementById('casetypeNote').disabled = true;
                document.getElementById('btnCaseTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#casetypeName').val('');
                $('#casetypeNote').val('');
            }
        });
    }
}

function CaseTypeEdit(id) {

    $('#casetypeName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/casetypes/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#casetypeId').val(result.id);
            $('#casetypeName').val(result.name);
            $('#casetypeNote').val(result.note);
            document.getElementById('btnCaseTypeAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('casetypeName').disabled = false;
            document.getElementById('casetypeNote').disabled = false;
            $('#casetypeName').focus();
            
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function CaseTypeDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/casetypes/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        onPopulateCaseType();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This case is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}
