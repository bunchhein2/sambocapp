﻿
$(document).ready(function () {
    $(document).ajaxStart(function () {
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    
    ViewAllAction();
});
function ViewAction() {
    //alert('hi');
}

$('input[name=btnViewAll]').change(function () {
    var viewAll = $('#btnViewAll').val();
    ViewAllAction(viewAll);
});
$('input[name=btnUnpaid]').change(function () {
    var unPaid = $('#btnUnpaid').val();
    UnpaidAction(unPaid);
});
$('input[name=btnPaid]').change(function () {
    var paid = $('#btnPaid').val();
    PaidAction(paid);
});

var tableViewPayment = [];
function ViewAllAction() {
    //alert('hi');
    tableViewPayment = $('#viewPaymentTable').DataTable({
        ajax: {
            url:"/api/CivilCasePaid?paidId=All",
            dataSrc: ""
        },
        columns: [
            {
                data: "civilcaseno"
            },
            {
                data: "customername",
            },
            //{
            //    data: "date",
            //    render: function (data) {
            //        return moment(new Date(data)).format('DD-MMM-YYYY');
            //    }
            //},
            {
                data: "customerphone"
            },
            {
                data: "charges",
            },
            {
                data: "services",
            },
            {
                data: "paid",
            },
            //{
            //    data: "id",
            //    render: function (data) {
            //        return "<button onclick='InvoiceMoveInsert(" + data + ")'  class='btn btn-success btn-xs pull-center' style='margin-right: 5px;'> Move</button>";
            //    }
            //}
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });
}
function UnpaidAction() {
    //alert('hi');
    tableViewPayment = $('#viewPaymentTable').DataTable({
        ajax: {
            url: "/api/CivilCasePaid?paidId=Unpaid",
            dataSrc: ""
        },
        columns: [
            {
                data: "civilcaseno"
            },
            {
                data: "customername",
            },
            //{
            //    data: "date",
            //    render: function (data) {
            //        return moment(new Date(data)).format('DD-MMM-YYYY');
            //    }
            //},
            {
                data: "customerphone"
            },
            {
                data: "charges",
            },
            {
                data: "services",
            },
            {
                data: "paid",
            },
            //{
            //    data: "id",
            //    render: function (data) {
            //        return "<button onclick='InvoiceMoveInsert(" + data + ")'  class='btn btn-success btn-xs pull-center' style='margin-right: 5px;'> Move</button>";
            //    }
            //}
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });


}
function PaidAction() {
    //alert('hi');
    tableViewPayment = $('#viewPaymentTable').DataTable({
        ajax: {
            url: "/api/CivilCasePaid?paidId",
            dataSrc: ""
        },
        columns: [
            {
                data: "civilcaseno"
            },
            {
                data: "customername",
            },
            //{
            //    data: "date",
            //    render: function (data) {
            //        return moment(new Date(data)).format('DD-MMM-YYYY');
            //    }
            //},
            {
                data: "customerphone"
            },
            {
                data: "charges",
            },
            {
                data: "services",
            },
            {
                data: "paid",
            },
            //{
            //    data: "id",
            //    render: function (data) {
            //        return "<button onclick='InvoiceMoveInsert(" + data + ")'  class='btn btn-success btn-xs pull-center' style='margin-right: 5px;'> Move</button>";
            //    }
            //}
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });


}