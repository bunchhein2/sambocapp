﻿
$(document).ready(function () {
    GetBanner();

});


let Request = document.getElementById("btn_Request");
let filter = document.getElementById("btn_All");
let bannernamesearch = document.getElementById("bannernamesearch");
function FilterRequest() {
    var inputVal1 = document.getElementById("bannernamesearch").value = "New";
    oTable = $('#tblbanner').DataTable();
    oTable.columns(6).search($('#bannernamesearch').val().trim());
    oTable.draw();
    Request.style.display = "none";
    filter.style.display = "block";
    filter.style.display = "auto";

}
function FilterAllData() {
    GetBanner();
    filter.style.display = "none";
    Request.style.display = "block";
    Request.style.display = "auto";

}

var tableBanner = [];
function GetBanner(StatusID) {
    tableBanner = $('#tblbanner').DataTable({
        ajax: {
            url: "/api/Banners",
            //url: (StatusID == "New") ? "/api/Banners?StatusID=New" : "/api/Banners?StatusID=" + StatusID,
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
                render: function (data) {

                    return "" + ("").slice(-4);
                }
            },
            {
                data: "shopid"
            },
            {
                data: "shopname"
            },
            {
                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "exireddate",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "bannerimage",
                render: function (data, type, meta, full) {
                    //console.log(data);
                    if (meta.bannerstatus === 'New') {
                        return '<image width="100px" height="100%" src="../Images/' + data + '"/>';
                    } else if (meta.bannerstatus === 'Disable') {
                        return "No Image";
                    }
                }
            }
            ,
            {
                data: "id",
                render: function (data, type, meta, full) {

                    if (meta.bannerstatus === 'New') {
                        return "<button OnClick='Aprove(" + data + ")' class='btn btn-success btn-xs' style='margin-left: 20px;' id='Aprove'><span class='glyphicon glyphicon-ok'></span> Aprove</button> ";
                    }
                    else if (meta.bannerstatus === 'Disable') {
                        return "<button OnClick='Disable(" + data + ")' class='btn btn-warning btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-ban-circle'></span> Disable</button> ";
                    }

                }
            },
            {
                data: "id",
                render: function (data) {
                    return "<button type='button' onclick='BannerPayment(" + data + ")' class='btn btn-warning btn-xs' style='margin-left: 10px;'><span class='glyphicon glyphicon-usd'></span> Pay</button> " +
                        "<button type='button' onclick='BannerEdits(" + data + ")' class='btn btn-warning btn-xs' style='margin-left: 10px;'><span class='glyphicon glyphicon-edit'></span> Edit</button> " +
                        "<button type='button' onclick='BannerDelete(" + data + ")' class='btn btn-danger btn-xs' style='margin-left: 10px;'><span class='glyphicon glyphicon-remove'></span> Delete</button>"
                }

            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false,
        "paging": true,
        "lengthMenu": [25, 50, 100, "All"]
    });
}

function BannerAction() {
    //ClearControl();
    //disabledControl();
    //EnableControl();
    var action = '';
    action = document.getElementById('btnBanner').innerText;
    if (action == "Save") {
        if ($('#bannerdate').val().trim() === "") {
            $('#bannerdate').focus();
            toastr.info("Please enter date", "Required");
        } else if ($('#bannershopid').val().trim() === "") {
            $('#bannershopid').css('border-color', 'red');
            $('#bannershopid').focus();
            toastr.info("Please enter shopid", "Required");
        } else if ($('#bannerexireddate').val().trim() === "") {
            $('#bannerexireddate').css('border-color', 'red');
            $('#bannerexireddate').focus();
            toastr.info("Please enter exireddate", "Required");
        } else if ($('#bannerstatus').val().trim() === "") {
            $('#bannerstatus').css('border-color', 'red');
            $('#bannerstatus').focus();
            toastr.info("Please enter status", "Required");
        } else if ($('#qtymonth').val().trim() === "") {
            $('#qtymonth').css('border-color', 'red');
            $('#qtymonth').focus();
            toastr.info("Please Select qtymonth", "Required");
        } else if ($('#bannerfile').val().trim() === "") {
            $('#bannerfile').css('border-color', 'red');
            $('#bannerfile').focus();
            toastr.info("Please enter Choise file", "Required");
        } else {
            $('#bannerdate').css('border-color', '#cccccc');
            $('#bannershopid').css('border-color', '#cccccc');
            $('#bannerexireddate').css('border-color', '#cccccc');
            $('#bannerstatus').css('border-color', '#cccccc');
            $('#qtymonth').css('border-color', '#cccccc');
            $('#bannerfile').css('border-color', '#cccccc');
            var data = new FormData();

            var files = $('#bannerfile').get(0).files;

            if (files.length > 0) {
                data.append("bannerimage", files[0]);
            }


            data.append("bannerdate", $('#bannerdate').val());
            data.append("banneruserid", $('#banneruserid').val());
            data.append("bannershopid", $('#bannershopid').val());
            data.append("bannerexireddate", $('#bannerexireddate').val());
            data.append("qtymonth", $('#qtymonth').val());
            data.append("bannerstatus", $('#bannerstatus').val());
            //console.log(JSON.stringify(data));
            $.ajax({
                url: "/api/Banners",
                data: data,
                type: "POST",
                contentType: false,
                processData: false,
                success: function (result) {
                    toastr.success("New Banner has been Created", "Server Respond");
                    tableBanner.ajax.reload();
                    window.location.reload();
                    ClearControl();


                },
                error: function (errormesage) {
                    $('#bannerdate').focus();
                    toastr.error("This Name is exist in Database", "Server Respond")
                }

            });
        }
    } else if (action == "Update") {
        if ($('#bannerdate').val().trim() === "") {
            $('#bannerdate').css('border-color', 'red');
            $('#bannerdate').focus();
            toastr.info("Please enter date", "Required");

        } else if ($('#bannershopid').val().trim() === "") {
            $('#bannershopid').css('border-color', 'red');
            $('#bannershopid').focus();
            toastr.info("Please enter shopid", "Required");
        } else if ($('#bannerexireddate').val().trim() === "") {
            $('#bannerexireddate').css('border-color', 'red');
            $('#bannerexireddate').focus();
            toastr.info("Please enter exireddate", "Required");
        } else if ($('#bannerstatus').val().trim() === "") {
            $('#bannerstatus').css('border-color', 'red');
            $('#bannerstatus').focus();
            toastr.info("Please enter status", "Required");
        } else {
            $('#bannerdate').css('border-color', '#cccccc');
            $('#banneruserid').css('border-color', '#cccccc');
            $('#bannershopid').css('border-color', '#cccccc');
            $('#bannerexireddate').css('border-color', '#cccccc');
            $('#bannerstatus').css('border-color', '#cccccc');
            $('#bannerfile').css('border-color', '#cccccc');
            var data = new FormData();

            var files = $('#bannerfile').get(0).files;

            if (files.length > 0) {
                data.append("bannerimage", files[0]);
            }

            data.append("bannerid", $('#bannerid').val());
            data.append("bannerdate", $('#bannerdate').val());
            data.append("bannershopid", $('#bannershopid').val());
            data.append("bannerexireddate", $('#bannerexireddate').val());
            data.append("qtymonth", $('#qtymonth').val());
            data.append("bannerstatus", $('#bannerstatus').val());
            data.append("banner_old", $('#banner_old').val());

            $.ajax({
                url: "/api/Banners/" + $("#bannerid").val(),
                data: data,
                type: "PUT",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (result) {
                    toastr.success("Customer has been Updated", "Server Respond");
                    $('#tableBanner').DataTable().ajax.reload();
                    $("#bannerModel").modal('hide');
                    ClearControl();
                    tableBanner.ajax.reload();
                    //window.location.reload();

                },
                error: function (errormesage) {
                    toastr.error("Customer hasn't Updated in Database", "Server Respond")
                }
            });
        }
    }

}

function BannerEdits(id) {
    disabledControl();
    $('#txtbtnBanner').val("Update");
    action = document.getElementById('btnBanner').innerText = "Update";


    $.ajax({
        url: "/api/Banners/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#bannerid').val(result.id);

            ///console.log(result.id);
            var date = new Date(result.date);
            var datetime = moment(date).format('YYYY-MM-DD');
            $('#bannerdate').val(datetime);
            $('#banneruserid').val(result.userid);
            $('#bannershopid').val(result.shopid);
            var date = new Date(result.exireddate);
            var datetimes = moment(date).format('YYYY-MM-DD');
            $('#bannerexireddate').val(datetimes);
            $('#qtymonth').val(result.qtymonth);
            $('#bannerstatus').select(result.bannerstatus);
            $('#banner_old').val(result.bannerimage);

            //From Model
            if (result.bannerimage == "") {
                //text image (view)
                $("#BannerPhoto").attr('src', '../Images/sample1-bg.png');
            } else {

                //String Url collum
                $("#BannerPhoto").attr('src', '../Images/' + result.bannerimage);


            }

            $("#bannerModel").modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
            EnableControl();
        }
    });


}
function BannerDelete(id) {
    // alert('hi');
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Banners/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tableBanner').DataTable().ajax.reload();
                        toastr.success("Item Deleted successfully!", "Service Response");
                        tableBanner.ajax.reload();
                        window.location.reload();
                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
} function BannerDelete(id) {
    // alert('hi');
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Banners/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tableBanner').DataTable().ajax.reload();
                        toastr.success("Item Deleted successfully!", "Service Response");
                        tableBanner.ajax.reload();
                        window.location.reload();
                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
}
function Aprove(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure to Disable this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default input-sm"
            },
            confirm: {
                label: "Disable",
                className: "btn-danger input-sm"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Banners/" + id,
                    type: "POST",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {

                        $('#tblbanner').DataTable().ajax.reload();
                        toastr.success("Item Aprove successfully!", "Service Response");
                        tableBanner.ajax.reload();
                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
}
function Disable(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure to Aprove this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default input-sm"
            },
            confirm: {
                label: "Aprove",
                className: "btn-info input-sm"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/Banners/" + id,
                    type: "POST",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {

                        $('#tblbanner').DataTable().ajax.reload();
                        toastr.success("Item Aprove successfully!", "Service Response");
                        tableBanner.ajax.reload();
                        window.location.reload();
                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
}

$("#qtymonth").change(function () {
    var bannerdate = $("#bannerdate").val();
    var qty = $(this).val();
    /*var expire =  Date.parse(bannerdate);*/
    //alert(bannerdate);
    if ($('#bannerdate').val().trim() === "") {
        $('#bannerdate').css('border-color', 'red');
        toastr.warning("Please Select Start Date", "Required");
    } else if ($('#qtymonth').val().trim() === "") {
        toastr.warning("Please Select QtyMonth", "Required");
        $('#qtymonth').css('border-color', 'red');
    } else {
        var expire = moment(bannerdate, "YYYY-MM-DD").add(qty, 'months').format('YYYY-MM-DD');

        //alert(qty + " | " + bannerdate + "| " + expire + " |");
        $("#bannerexireddate").val(expire);
    }

});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#defendentPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}


function disabledControl() {
    document.getElementById('bannerdate').disabled = false;
    document.getElementById('banneruserid').disabled = false;
    document.getElementById('bannershopid').disabled = false;
    document.getElementById('bannerexireddate').disabled = false;
    document.getElementById('bannerstatus').disabled = false;
}
function EnableControl() {
    document.getElementById('bannerdate').disabled = true;
    document.getElementById('banneruserid').disabled = true;
    document.getElementById('bannershopid').disabled = true;
    document.getElementById('bannerexireddate').disabled = true;
    document.getElementById('bannerstatus').disabled = true;

}
function ClearControl() {
    $('#bannerdate').val('');
    $('#banneruserid').val('');
    $('#bannershopid').val('');
    $('#bannerexireddate').val('');
    $('#bannerstastus').val('');
    $('#BannerPhoto').val('');
    document.getElementById('btnBanner').innerText = "Save";
    $('#txtbtnBanner').val("Save");
}
function readURLBanner(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#BannerPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function readURLAdvertisePay(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#advertisePayPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function BannerPayment(id) {
    $("#advertisePayModal").modal('show');
    $('#advertiseId').val(id);
    GetAdvertisePayment(id);
    ClearControlAdv();
    DisableControlAdv();
    $('#advertisePayPhoto').attr('src', '../Images/company.png');
    document.getElementById('btnAdvertisePay').innerText = 'Add New';
}
var tableAdvertisePay = [];
function GetAdvertisePayment(id) {
    tableAdvertisePay = $('#AdvertisePayTbl').DataTable({
        ajax: {
            url: "/api/AdvertisePayment/"+id+"/1",
            //url: (StatusID == "New") ? "/api/Banners?StatusID=New" : "/api/Banners?StatusID=" + StatusID,
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
                render: function (data) {

                    return "" + ("").slice(-4);
                }
            },
            {
                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "amount"
            },
            {
                data: "note"
            },
            
            {
                data: "id",
                render: function (data) {
                    return "<button type='button' onclick='AdvPayEdits(" + data + ")' class='btn btn-warning btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-edit'></span> Edit</button> " +
                        "<button type='button' onclick='AdvPayDelete(" + data + ")' class='btn btn-danger btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-remove'></span> Delete</button>"
                }

            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false,
        "paging": true,
        "lengthMenu": [25, 50, 100, "All"]
    });
}
function AdvertisePayAction() {
    var action = '';
    action = document.getElementById('btnAdvertisePay').innerText;

    if (action == "Add New") {
        document.getElementById('btnAdvertisePay').innerText = 'Save';
        EnableControlAdv();
        ClearControlAdv();
        $('#amount').focus();
        $('#date').val(moment().format('YYYY-MM-DD'));
        $('#advertisePayPhoto').attr('src', '../Images/company.png');
        $('#amount').val('0.00');
    }
    else if (action == "Save") {
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("screenshot", files[0]);
        }

        data.append("date", $('#date').val());
        data.append("amount", $('#amount').val());
        data.append("note", $('#noteAd').val());
        data.append("advertiseid", $('#advertiseId').val());

        $.ajax({
            url: "/api/AdvertisePayment",
            data: data,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Save To Database Successfully", "Server Respond");
                $('#AdvertisePayTbl').DataTable().ajax.reload();
                document.getElementById('btnAdvertisePay').innerText = 'Add New';
                DisableControlAdv();
                //$("#employeeModal").modal('hide');
            },
            error: function (errormesage) {
                $('#amount').focus();
                toastr.error("This Name is exist in Database", "Server Respond")
            }

        });

    } else if (action == "Update") {
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("screenshot", files[0]);
        }

        data.append("id", $('#ID').val());

        data.append("date", $('#date').val());
        data.append("amount", $('#amount').val());
        data.append("note", $('#noteAd').val());
        data.append("advertiseid", $('#advertiseId').val());
        data.append("file_old", $("#file_old").val());

        $.ajax({
            url: "/api/AdvertisePayment/" + $("#ID").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Data has been Updated", "Server Respond");
                $('#AdvertisePayTbl').DataTable().ajax.reload();
                document.getElementById('btnAdvertisePay').innerText = 'Add New';
                DisableControlAdv();
                //$("#employeeModal").modal('hide');
            },
            error: function (errormesage) {
                toastr.error("Data hasn't Updated in Database", "Server Respond")
            }
        });
    }
}
function AdvPayEdits(id) {
    ClearControlAdv();
    EnableControlAdv();
    action = document.getElementById('btnAdvertisePay').innerText = "Update";

    $.ajax({
        url: "/api/AdvertisePayment/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#ID').val(result.id);

            ///console.log(result.id);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $("#date").val(dr);

            $("#amount").val(result.amount);
            $("#noteAd").val(result.note);
            $("#advertiseId").val(result.advertiseid);
            $('#file_old').val(result.screenshot);

            if (result.screenshot == "") {
                $("#advertisePayPhoto").attr('src', '../Images/company.png');
            } else {
                $("#advertisePayPhoto").attr('src', '../Images/' + result.screenshot);
            }

            //$("#employeeModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
}
function AdvPayDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/AdvertisePayment/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#AdvertisePayTbl').DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}

function DisableControlAdv() {
    document.getElementById('amount').disabled = true;
    document.getElementById('noteAd').disabled = true;
    document.getElementById('date').disabled = true;
    document.getElementById('file').disabled = true;
};
function EnableControlAdv() {
    document.getElementById('amount').disabled = false;
    document.getElementById('noteAd').disabled = false;
    document.getElementById('date').disabled = false;
    document.getElementById('file').disabled = false;
};
function ClearControlAdv() {
    $('#amount').val('0.00');
    $('#noteAd').val('');
    $('#advertisePayPhoto').attr('src', '../Images/company.png');
}