﻿$(document).ready(function () {
    //$("#bannerModel").on('show.modal.bs', function () {
    //    //GetBanner();

    //});
    GetExchange();

});

$("#btnprivacy").click(function () {
    document.getElementById('btnExchange').innerText = "Save";
    $("#txtbtnExchange").val("Save");
});
var tableexchange = [];
function GetExchange() {
    tableCase = $('#tblExchange').DataTable({
        ajax: {
            url: "/api/Exchanges",
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
                render: function (data) {

                    return "" + ("").slice(-4);
                }
            },
            {
                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "currencyname"
            },
            {
                data: "shopname"
            },
            {
                data: "rate"
            },

            {
                data: "id",
                render: function (data) {
                    return "<button type='button' onclick='ExchangeEdits(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>"
                        + "<button type='button' onclick='ExchangeDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": true
    });
}
function ExchangeAction() {
    
    var action = '';
    action = $("#txtbtnExchange").val();
    if (action == "Save") {
        if ($('#exchangedate').val().trim() === "") {
            $('#exchangedate').css('border-color', 'red');
            $('#exchangedate').focus();
            toastr.info("Please enter exchangedate", "Required");
        } else if ($('#exchangecurrencyid').val().trim() === "") {
            $('#exchangecurrencyid').css('border-color', 'red');
            $('#exchangecurrencyid').focus();
            toastr.info("Please enter currencyid", "Required");
        } else if ($('#exchangeshopid').val().trim() === "") {
            $('#exchangeshopid').css('border-color', 'red');
            $('#exchangeshopid').focus();
            toastr.info("Please enter shopid", "Required");
        } else if ($('#exchangerate').val().trim() === "") {
            $('#exchangerate').css('border-color', 'red');
            $('#exchangerate').focus();
            toastr.info("Please enter rate", "Required");
        } else {
            $('#exchangedate').css('border-color', '#cccccc');
            $('#exchangecurrencyid').css('border-color', '#cccccc');
            $('#exchangeshopid').css('border-color', '#cccccc');
            $('#exchangerate').css('border-color', '#cccccc');
                    var data = new FormData();
                    data.append("exchangedate", $('#exchangedate').val());
                    data.append("exchangecurrencyid", $('#exchangecurrencyid').val());
                    data.append("exchangeshopid", $('#exchangeshopid').val());
                    data.append("exchangerate", $('#exchangerate').val());
                    $.ajax({
                        url: "/api/Exchanges",
                        data: data,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            toastr.success("New Banner has been Created", "Server Respond");
                            $('#tblExchange').DataTable().ajax.reload();
                            $("#ExchangeModel").modal('hide');
                            ClearControl();
                            
                        },
                        error: function (errormesage) {
                            $('#bannerdate').focus();
                            toastr.error("This Name is exist in Database", "Server Respond")
                        }

                    });
                }
        
    } else if (action == "Update") {
        if ($('#exchangedate').val().trim() === "") {
            $('#exchangedate').css('border-color', 'red');
            $('#exchangedate').focus();
            toastr.info("Please enter exchangedate", "Required");
        } else if ($('#exchangecurrencyid').val().trim() === "") {
            $('#exchangecurrencyid').css('border-color', 'red');
            $('#exchangecurrencyid').focus();
            toastr.info("Please enter currencyid", "Required");
        } else if ($('#exchangeshopid').val().trim() === "") {
            $('#exchangeshopid').css('border-color', 'red');
            $('#exchangeshopid').focus();
            toastr.info("Please enter shopid", "Required");
        } else if ($('#exchangerate').val().trim() === "") {
            $('#exchangerate').css('border-color', 'red');
            $('#exchangerate').focus();
            toastr.info("Please enter rate", "Required");
        } else {
                    $('#exchangedate').css('border-color', '#cccccc');
                    $('#exchangecurrencyid').css('border-color', '#cccccc');
                    $('#exchangeshopid').css('border-color', '#cccccc');
                    $('#exchangerate').css('border-color', '#cccccc');
                    var data = new FormData();

                    //data.append("exchangeid", $('#exchangeid').val());
                    data.append("exchangedate", $('#exchangedate').val());
                    data.append("exchangecurrencyid", $('#exchangecurrencyid').val());
                    data.append("exchangeshopid", $('#exchangeshopid').val());
                    data.append("exchangerate", $('#exchangerate').val());

                    $.ajax({
                        url: "/api/Exchanges/" + $("#exchangeid").val(),
                        data: data,
                        type: "PUT",
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        success: function (result) {
                            toastr.success("exchang has been Updated", "Server Respond");
                            $('#tblExchange').DataTable().ajax.reload();
                            action = document.getElementById('btnExchange').innerText = "Save";
                            $("#txtbtnExchange").val("Save");
                            $("#ExchangeModel").modal('hide');
                            ClearControl();

                        },
                        error: function (errormesage) {
                            toastr.error("exchang hasn't Updated in Database", "Server Respond")
                        }
                    });
               }
    }

}
function ExchangeEdits(id) {
    action = document.getElementById('btnExchange').innerText = "Update";
    $.ajax({
        url: "/api/exchanges/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#exchangeid').val(result.id);
            var date = new Date(result.date);
            var datetime = moment(date).format('YYYY-MM-DD');
            $('#exchangedate').val(datetime);
            $('#exchangecurrencyid').val(result.currencyid);
            $('#exchangeshopid').val(result.shopid);
            $('#exchangerate').val(result.rate);
            $("#ExchangeModel").modal('show');
            $("#txtbtnExchange").val("Update");
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happenn.", "Server Response");
        }
    });
}
function ExchangeDelete(id) {
    // alert('hi');
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/exchanges/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblExchange').DataTable().ajax.reload();
                        toastr.success("Item Deleted successfully!", "Service Response");
                        tblExchange.ajax.reload();
                        window.location.reload();
                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
   
}
function EnableControl() {
    document.getElementById('exchangedate').disabled = true;
    document.getElementById('exchangecurrencyid').disabled = true;
    document.getElementById('exchangeshopid').disabled = true;
    document.getElementById('exchangerate').disabled = true;

}
function ClearControl() {
    $('#exchangedate').val('');
    $('#exchangecurrencyid').val('');
    $('#exchangeshopid').val('');
    $('#exchangerate').val('');
}