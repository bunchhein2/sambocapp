﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    GetTransfer();
    document.getElementById('transferdate').disabled = true;
    document.getElementById('fromid').disabled = true;
    document.getElementById('toid').disabled = true;
    document.getElementById('transferamount').disabled = true;
    document.getElementById('transfernote').disabled = true;
    document.getElementById('btnTransfer').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
       
});

var tableTransfer = [];

function GetTransfer() {
    tableTransfer = $('#transferTable').DataTable({
        ajax: {
            url: "/api/transfers",
            dataSrc: ""
        },
        columns: [
             {
                 data: "id"
             },
            {
                data: "transferdate",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "fromname"
            },
            {
                data: "toname"
            },
             {
                 data: "transferamount"
             },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='TransferEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>"
                        + "<button onclick='TransferDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function TransferAction() {
    var action = '';
    action = document.getElementById('btnTransfer').innerText;
    if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('transferdate').disabled = false;
        document.getElementById('fromid').disabled = false;
        document.getElementById('toid').disabled = false;
        document.getElementById('transferamount').disabled = false;
        document.getElementById('transfernote').disabled = false;
        document.getElementById('btnTransfer').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#transferdate').val(moment().format('YYYY-MM-DD'));
        $('#transferamount').val('0');
        $('#transferamount').focus();
    }else if(action === " រក្សាទុក / Save") {
        if ($('#transferamount').val().trim() === "") {
            $('#transferamount').css('border-color', 'red');
            $('#transferamount').focus();
            toastr.info("Please enter Transfer's Amount", "Required");
        }
        else {
            $('#transferamount').css('border-color', '#cccccc');
            var data = {
                transferdate: $('#transferdate').val(),
                fromid: $('#fromid').val(),
                toid: $('#toid').val(),
                transferamount: $('#transferamount').val(),
                transfernote: $('#transfernote').val()
            };
            $.ajax({
                url: "/api/transfers",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableTransfer.ajax.reload();
                    //alert('Hi');
                    document.getElementById('transferdate').disabled = true;
                    document.getElementById('fromid').disabled = true;
                    document.getElementById('toid').disabled = true;
                    document.getElementById('transferamount').disabled = true;
                    document.getElementById('transfernote').disabled = true;
                    document.getElementById('btnTransfer').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#transferamount').val('');
                    $('#transfernote').val('');
                    $('#transferModel').modal('hide');
                },
                error: function (errormessage) {
                    toastr.error("This case is already exists.", "Server Response");
                    
                }
            });
        }
    }else if (action === " កែប្រែ / Update") {
        $('#transferamount').css('border-color', '#cccccc');

        var data = {
            id: $('#transferid').val(),
            transferdate: $('#transferdate').val(),
            fromid: $('#fromid').val(),
            toid: $('#toid').val(),
            transferamount: $('#transferamount').val(),
            transfernote: $('#transfernote').val()
        };
        $.ajax({
            url: "/api/transfers/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableTransfer.ajax.reload();
                document.getElementById('transferdate').disabled = true;
                document.getElementById('fromid').disabled = true;
                document.getElementById('toid').disabled = true;
                document.getElementById('transferamount').disabled = true;
                document.getElementById('transfernote').disabled = true;
                document.getElementById('btnTransfer').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#transferamount').val('');
                $('#transfernote').val('');
                $('#transferModel').modal('hide');
            },
            error: function (errormessage) {
                toastr.error("This Begining Balance is already exists.", "Server Response");
                
            }
        });
    }
}

function TransferEdit(id) {
    
    $('#transferModel').modal('show');
    $('#transferamount').css('border-color', '#cccccc');
    $.ajax({
        url: "/api/transfers/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#transferid').val(result.id);
            var dr = moment(result.transferdate).format("YYYY-MM-DD");
            $('#transferdate').val(dr);
            $('#fromid').val(result.fromid).change();
            $('#toid').val(result.toid).change();
            $('#transferamount').val(result.transferamount);
            $('#transfernote').val(result.transfernote);
            //alert(result.paymentmethodid);
            document.getElementById('btnTransfer').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('transferdate').disabled = false;
            document.getElementById('fromid').disabled = false;
            document.getElementById('toid').disabled = false;
            document.getElementById('transferamount').disabled = false;
            document.getElementById('transfernote').disabled = false;

            $('#transferamount').focus();
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function TransferDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/transfers/" + id,
                    method: "DELETE",
                    success: function () {
                        tableTransfer.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                        $('#transferamount').val('');
                        $('#transfernote').val('');
                    },
                    error: function () {
                        toastr.error("This transfer is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}