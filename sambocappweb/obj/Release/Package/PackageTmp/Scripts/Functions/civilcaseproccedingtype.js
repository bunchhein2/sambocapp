﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    onPopulateCaseProcceedingType();
    //$('#civilcaseModal').on('show.bs.modal', function () {
    //    // Bind Cases Select Options
    //    onPopulateCaseProcceedingType();
    //});

    $('#caseproccedingtypeModal').on('show.bs.modal', function () {
        GetCasesProccedingType();
        document.getElementById('caseproccedingtype').disabled = true;
        document.getElementById('caseproccedingnote').disabled = true;
        document.getElementById('btnCaseProccedingTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#caseproccedingtype').val('');
    });
});

var tableCase = [];

function onPopulateCaseProcceedingType() {
    $.ajax({
        url: "/api/caseproccedingtypes?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zcaseprocedingtypeid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function GetCasesProccedingType() {
    tableCase = $('#CaseProccedingTypeTable').DataTable({
        ajax: {
            url: "/api/CaseproccedingTypes",
            dataSrc: ""
        },
        columns: [
            {
                data: "caseproccedingtype"
            },
            {
                data: "caseproccedingnote"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='CaseProccedingTypeEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='CaseProccedingTypeDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function CaseProccedingTypeAction() {
    var action = '';
    action = document.getElementById('btnCaseProccedingTypeAction').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#caseproccedingtype').val().trim() === "") {
            $('#caseproccedingtype').css('border-color', 'red');
            $('#caseproccedingtype').focus();
            toastr.info("Please enter Case's name", "Required");
        }
        else {
            $('#caseproccedingtype').css('border-color', '#cccccc');

            var data = {
                caseproccedingtype: $('#caseproccedingtype').val(),
                caseproccedingnote: $('#caseproccedingnote').val()
            };
            $.ajax({
                url: "/api/CaseproccedingTypes",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableCase.ajax.reload();
                    onPopulateCaseProcceedingType();
                    document.getElementById('caseproccedingtype').disabled = true;
                    document.getElementById('caseproccedingnote').disabled = true;
                    document.getElementById('btnCaseProccedingTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#caseproccedingtype').val('');
                    $('#caseproccedingnote').val('');
                },
                error: function (errormessage) {
                    toastr.error("This case procceding type is already exists.", "Server Response");
                    document.getElementById('caseproccedingtype').disabled = true;
                    document.getElementById('caseproccedingnote').disabled = true;
                    document.getElementById('btnCaseProccedingTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#caseproccedingtype').val('');
                    $('#caseproccedingnote').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('caseproccedingtype').disabled = false;
        document.getElementById('caseproccedingnote').disabled = false;
        document.getElementById('btnCaseProccedingTypeAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#caseproccedingtype').val('');
        $('#caseproccedingnote').val('');
        $('#caseproccedingtype').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#caseproccedingtype').css('border-color', '#cccccc');
        
        var data = {
            id: $('#caseproccedingtypeId').val(),
            caseproccedingtype: $('#caseproccedingtype').val(),
            caseproccedingnote: $('#caseproccedingnote').val()
        };
        $.ajax({
            url: "/api/CaseproccedingTypes/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateCaseProcceedingType();
                document.getElementById('caseproccedingtype').disabled = true;
                document.getElementById('caseproccedingnote').disabled = true;
                document.getElementById('btnCaseProccedingTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#caseproccedingtype').val('');
                $('#caseproccedingnote').val('');
            },
            error: function (errormessage) {
                toastr.error("This department is already exists.", "Server Response");
                document.getElementById('caseproccedingtype').disabled = true;
                document.getElementById('caseproccedingnote').disabled = true;
                document.getElementById('btnCaseProccedingTypeAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#caseproccedingtype').val('');
                $('#caseproccedingnote').val('');
            }
        });
    }
}

function CaseProccedingTypeEdit(id) {

    $('#caseproccedingtype').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/CaseproccedingTypes/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#caseproccedingtypeId').val(result.id);
            $('#caseproccedingtype').val(result.caseproccedingtype);
            $('#caseproccedingnote').val(result.caseproccedingnote);
            document.getElementById('btnCaseProccedingTypeAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('caseproccedingtype').disabled = false;
            document.getElementById('caseproccedingnote').disabled = false;
            $('#caseproccedingtype').focus();

        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function CaseProccedingTypeDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/CaseproccedingTypes/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        onPopulateCaseProcceedingType();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This case is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}
