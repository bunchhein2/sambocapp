namespace sambocappweb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migrateOum : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Currencies", newName: "currency_tbl");
            RenameTable(name: "dbo.Exchange", newName: "exchange_tbl");
            RenameTable(name: "dbo.Privacies", newName: "privacy_tbl");
            CreateTable(
                "dbo.banner_tbl",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        date = c.DateTime(nullable: false),
                        userid = c.String(),
                        shopid = c.Int(nullable: false),
                        exireddate = c.DateTime(nullable: false),
                        bannerimage = c.String(),
                        bannerstatus = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.orderdetail_tbl",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        orderid = c.Int(nullable: false),
                        productid = c.Int(nullable: false),
                        qty = c.Int(nullable: false),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.exchange_tbl", "date", c => c.DateTime(nullable: false));
            AddColumn("dbo.exchange_tbl", "currencyid", c => c.Int(nullable: false));
            AddColumn("dbo.exchange_tbl", "shopid", c => c.Int(nullable: false));
            AddColumn("dbo.exchange_tbl", "rate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.currency_tbl", "currencyname", c => c.String());
            AlterColumn("dbo.currency_tbl", "sign", c => c.String());
            DropColumn("dbo.exchange_tbl", "RateIn");
            DropColumn("dbo.exchange_tbl", "RateOut");
            DropColumn("dbo.exchange_tbl", "ExchangeDate");
            DropColumn("dbo.exchange_tbl", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.exchange_tbl", "Description", c => c.String(maxLength: 100));
            AddColumn("dbo.exchange_tbl", "ExchangeDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.exchange_tbl", "RateOut", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.exchange_tbl", "RateIn", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.currency_tbl", "sign", c => c.String(maxLength: 50));
            AlterColumn("dbo.currency_tbl", "currencyname", c => c.String(maxLength: 50));
            DropColumn("dbo.exchange_tbl", "rate");
            DropColumn("dbo.exchange_tbl", "shopid");
            DropColumn("dbo.exchange_tbl", "currencyid");
            DropColumn("dbo.exchange_tbl", "date");
            DropTable("dbo.orderdetail_tbl");
            DropTable("dbo.banner_tbl");
            RenameTable(name: "dbo.privacy_tbl", newName: "Privacies");
            RenameTable(name: "dbo.exchange_tbl", newName: "Exchange");
            RenameTable(name: "dbo.currency_tbl", newName: "Currencies");
        }
    }
}
