﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
namespace sambocappweb.Dtos
{
    [Table("orderdetail_tbl")]
    public class OrderdetailDto
    {
        public int id { get; set; }
        [Required]
        public int orderid { get; set; }
        public int productid { get; set; }
        public int qty { get; set; }
        public decimal price { get; set; }
        public decimal discount { get; set; }
    }
}
