﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class PropertyDto
    {
        public int id { get; set; }
        public string property { get; set; }
        public string note { get; set; }
    }
}