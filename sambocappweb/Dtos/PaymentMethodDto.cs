﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("pamentmethod_tbl")]
    public class PaymentMethodDto
    {
        public int id { get; set; }
        [Required]
        public string methodname { get; set; }
        public string createby { get; set; }
        public DateTime createdate { get; set; }
        public bool status { get; set; }
    }
}