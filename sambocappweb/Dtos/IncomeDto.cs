﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{

    [Table("income_tbl")]
    public class IncomeDto
    {
        [Key]
        public int id { get; set; }
        public DateTime date { get; set; }
        [Required]
        public int incometypeid { get; set; }
        public IncomeType IncomeType { get; set; }
        [Required]
        public int subincometypeid { get; set; }
        public SubIncomeType SubIncomeTypes { get; set; }
        public int paymentmethodid { get; set; }
        public double amount { get; set; }
        public string note { get; set; }
        public string createby { get; set; }
        public DateTime createdate { get; set; }
    }
}