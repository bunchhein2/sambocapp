﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using sambocappweb.Models;

namespace sambocappweb.Dtos
{
	[Table("caseexpense_tbl")]

	public class CaseexpenseDto
    {
		public int id { get; set; }
		public int civilcaseid { get; set; }

		public CivilCase CivilCase { get; set; }
		public DateTime? datetime { get; set; }
		public string userid { get; set; }
		[Required]
		[StringLength(100)]
		public string note { get; set; }
		public DateTime? startdate { get; set; }
		public DateTime? enddate { get; set; }
		public string offerby { get; set; }
		public bool active { get; set; }
		public DateTime? createdate { get; set; }
	}
}