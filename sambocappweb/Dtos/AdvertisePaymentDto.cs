﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class AdvertisePaymentDto
    {
        public int id { get; set; }
        public DateTime? date { get; set; }
        public int advertiseid { get; set; }
        public decimal amount { get; set; }
        public string screenshot { get; set; }
        public string note { get; set; }
    }
}