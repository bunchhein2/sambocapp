﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("caseposition_tbl")]
    public class CasepositionDto
    {
        public int id { get; set; }
        [Required]
        [StringLength(50)]
        public string casepositionname { get; set; }
        public string casepositionnote { get; set; }
    }
}