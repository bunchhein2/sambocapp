﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
   
    [Table("incometype_tbl")]
    public class IncomeTypeDto
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string incometypename { get; set; }

    }
}