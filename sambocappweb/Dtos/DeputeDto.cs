﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    [Table("Depute_tbl")]
    public class DeputeDto
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string deputenNme { get; set; }
        public string deputeNote { get; set; }
        public Boolean status { get; set; }
    }
}