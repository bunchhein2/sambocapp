﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class QrcodeDto
    {
        public int id { get; set; }
        public string qrcode { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}