﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Dtos
{
    public class ContractExpanseDto
    {
		public int id { get; set; }
		public int contractid { get; set; }
		public DateTime? date { get; set; }
		public string userid { get; set; }
		public string note { get; set; }
		public DateTime? startdate { get; set; }
		public DateTime? enddate { get; set; }
		public string offerby { get; set; }
		public bool active { get; set; }
		public DateTime? createdate { get; set; }
	}
}