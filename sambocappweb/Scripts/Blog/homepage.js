﻿var app = {
    Posts: function () {
        $(document).ajaxStart(function () {
            Pace.start();
            $("div#divLoadingModal").addClass('show');
        }).ajaxStop(function () {
            Pace.stop();
            $("div#divLoadingModal").removeClass('show');
        });

        this.init = function () {
            this.get_all_items_pagination();
        }

        this.get_all_items_pagination = function () {

            _this = this;

            if ($('form.post-list input').val()) {
                data = JSON.parse($('form.post-list input').val());
                _this.ajax_get_all_items_pagination(data.page, data.name, data.sort);
            } else {
                _this.ajax_get_all_items_pagination(1, $('.post_name').val(), $('.post_sort').val());
            }

            $('body').on('click', '.post_search_submit', function () {
                _this.ajax_get_all_items_pagination(1, $('.post_name').val(), $('.post_sort').val());
            });

            $(".post_search_text").keyup(function (e) {
                if (e.keyCode == 13) {
                    _this.ajax_get_all_items_pagination(1, $('.post_name').val(), $('.post_sort').val());
                }
            });

            $('body').on('click', '.pagination-nav li.active', function () {
                var page = $(this).attr('p');
                _this.ajax_get_all_items_pagination(page, $('.post_name').val(), $('.post_sort').val());
            });
        }

        this.ajax_get_all_items_pagination = function (page, order_by_name, order_by_sort) {

            if ($(".pagination-container").length > 0 && $('.products-view-all').length > 0) {

                var post_data = {
                    page: page,
                    search: $('.post_search_text').val(),
                    name: order_by_name,
                    sort: order_by_sort,
                    max: $('.post_max').val(),
                };

                $('form.post-list input').val(JSON.stringify(post_data));

                var data = {
                    action: 'get-all-products',
                    data: JSON.parse($('form.post-list input').val())
                };

                $.ajax({
                    url: '/BlogPosts/HomePage',
                    type: 'POST',
                    data: data,
                    success: function (response) {
                        response = JSON.parse(response);

                        if ($(".pagination-container").html(response.content)) {
                            $('.pagination-nav').html(response.navigation);
                            $('.table-post-list th').each(function () {
                                $(this).find('span.glyphicon').remove();
                                if ($(this).hasClass('active')) {
                                    if (JSON.parse($('form.post-list input').val()).th_sort == 'DESC') {
                                        $(this).append(' <span class="glyphicon glyphicon-chevron-down pull-right"></span>');
                                    } else {
                                        $(this).append(' <span class="glyphicon glyphicon-chevron-up pull-right"></span>');
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    }
}

jQuery(document).ready(function () {
    posts = new app.Posts();
    posts.init();

});

function submitFeedback() {
    if ($('#feedbackName').val().trim() === "") {
        $('#feedbackName').css('border-color', 'red');
        $('#feedbackName').focus();
        toastr.info("Please enter your name", "Required");
    }
    else {
        $('#feedbackName').css('border-color', '#505050');
        if ($('#feedbackEmail').val().trim() === "") {
            $('#feedbackEmail').css('border-color', 'red');
            $('#feedbackEmail').focus();
            toastr.info("Please enter your email", "Required");
        }
        else {
            $('#feedbackEmail').css('border-color', '#505050');
            if ($('#feedbackSubject').val().trim() === "") {
                $('#feedbackSubject').css('border-color', 'red');
                $('#feedbackSubject').focus();
                toastr.info("Please enter your subject", "Required");
            }
            else {
                $('#feedbackSubject').css('border-color', '#505050');
                if ($('#feedbackMessage').val().trim() === "") {
                    $('#feedbackMessage').css('border-color', 'red');
                    $('#feedbackMessage').focus();
                    toastr.info("Please enter your message", "Required");
                }
                else {
                    $('#feedbackMessage').css('border-color', '#505050');

                    var data = {
                        Name: $('#feedbackName').val(),
                        Email: $('#feedbackEmail').val(),
                        Subject: $('#feedbackSubject').val(),
                        Message: $('#feedbackMessage').val()
                    };
                    $.ajax({
                        url: "/api/feedbacks",
                        data: JSON.stringify(data),
                        type: "POST",
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        success: function (result) {
                            toastr.success("Your feedback has been submitted successfully, we will get back to you soon.", "Success");
                            $('#feedbackName').val('');
                            $('#feedbackEmail').val('');
                            $('#feedbackSubject').val('');
                            $('#feedbackMessage').val('');
                        },
                        error: function (errormessage) {
                            toastr.error("This feedback is already exists.", "Server Response");
                            $('#feedbackName').val('');
                            $('#feedbackEmail').val('');
                            $('#feedbackSubject').val('');
                            $('#feedbackMessage').val('');
                        }
                    });
                }
            }
        }
    }
}