﻿function submitFeedback() {
    if ($('#feedbackName').val().trim() === "") {
        $('#feedbackName').css('border-color', 'red');
        $('#feedbackName').focus();
        toastr.info("Please enter your name", "Required");
    }
    else {
        $('#feedbackName').css('border-color', '#505050');
        if ($('#feedbackEmail').val().trim() === "") {
            $('#feedbackEmail').css('border-color', 'red');
            $('#feedbackEmail').focus();
            toastr.info("Please enter your email", "Required");
        }
        else {
            $('#feedbackEmail').css('border-color', '#505050');
            if ($('#feedbackSubject').val().trim() === "") {
                $('#feedbackSubject').css('border-color', 'red');
                $('#feedbackSubject').focus();
                toastr.info("Please enter your subject", "Required");
            }
            else {
                $('#feedbackSubject').css('border-color', '#505050');
                if ($('#feedbackMessage').val().trim() === "") {
                    $('#feedbackMessage').css('border-color', 'red');
                    $('#feedbackMessage').focus();
                    toastr.info("Please enter your message", "Required");
                }
                else {
                    $('#feedbackMessage').css('border-color', '#505050');

                    var data = {
                        Name: $('#feedbackName').val(),
                        Email: $('#feedbackEmail').val(),
                        Subject: $('#feedbackSubject').val(),
                        Message: $('#feedbackMessage').val()
                    };
                    $.ajax({
                        url: "/api/feedbacks",
                        data: JSON.stringify(data),
                        type: "POST",
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        success: function (result) {
                            toastr.success("Your feedback has been submitted successfully, we will get back to you soon.", "Success");
                            $('#feedbackName').val('');
                            $('#feedbackEmail').val('');
                            $('#feedbackSubject').val('');
                            $('#feedbackMessage').val('');
                        },
                        error: function (errormessage) {
                            toastr.error("This feedback is already exists.", "Server Response");
                            $('#feedbackName').val('');
                            $('#feedbackEmail').val('');
                            $('#feedbackSubject').val('');
                            $('#feedbackMessage').val('');
                        }
                    });
                }
            }
        }
    }
}