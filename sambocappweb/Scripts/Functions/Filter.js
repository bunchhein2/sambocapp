﻿$(document).ready(function () {
    
    getshops();

    $(document).ajaxStart(function () {
        $('#loadingGif').addClass('show');
    }).ajaxStop(function () {
        $('#loadingGif').removeClass('show');
    });

    $('#managecompany').on('change', function () {
      
        var shopId = this.value;
        //alert(id);
        if (shopId == "---Select_Status---") {
            getshops("all");
        } else {
            getshops(shopId);
        }
    })
    //getshops("all");
    //$('#companyModal').on('show.bs.modal', function () {

    //    //document.getElementById('companyTypeName').disable = true;
    //});
});

function getshops(shopId) {
    tableShop = $('#tblShops').dataTable({
        ajax: {
            //url: "/api/Shops?status=Register",
            url: (shopId == "all") ? "/api/Shops/all/none" : "/api/Shops/" + shopId+"/Active",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "shopName"
                },
                {
                    data: "ownerName"
                },

                {
                    data: "location"
                },

                {
                    data: "status"
                    
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='ApproveEdite (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span>Edit</button>" +
                            "<button OnClick='UpdateRegister (" + data + ")' class='btn btn-success btn-xs' ><span class='glyphicon glyphicon-check'></span>Approve</button>" +
                        "<button OnClick='ApproveDelete (" + data + ")' class='btn btn-danger btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,
        responsive: true,
        "order": [[0, "desc"]]

    });
}

function UpdateRegister(id) {
    $("#filterModal").modal('hide');
        /*ClearControl();*/
        EnableControl();
        action = document.getElementById('btnSaveShopRegister').innerText = "Update";

        $.ajax({
            url: "/api/Shops/" + id,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            success: function (result) {
                $('#shopID').val(result.id);

                ///console.log(result.id);            
                $("#shopname").val(result.shopName);
                $("#ownername").val(result.ownerName);
                $("#phone").val(result.phone);
                $("#password").val(result.password);
                $("#tokenid").val(result.tokenid);
                $("#facebookpage").val(result.facebookPage);
                $("#location").val(result.location).change();
                $("#paymenttype").val(result.paymentType).change();

                $("#qrcode_old").val(result.qrCodeImage);
                $("#logoShop_old").val(result.logoShop);
                //QRCODE
                var files = $('#qrCodeImage').get(0).files;
                if (result.qrcodeimage == "") {
                    $("#QrPhoto").attr('src', '../Images/company.png');
                } else {
                    $("#QrPhoto").attr('src', '../Images/' + result.qrCodeImage);

                }
                //Logo Shop
                var files = $('#logoShop').get(0).files;
                if (result.logoShop == "") {
                    $("#QrlogoShop").attr('src', '../Images/company.png');

                } else {
                    $("#QrlogoShop").attr('src', '../Images/' + result.logoShop);


                }

                $("#bankname").val(result.bankName).change();
                $("#accountnumber").val(result.accountNumber);
                $("#accountname").val(result.accountName);
                $("#feecharge").val(result.feecharge);
                var date = new Date(result.shophistorydate);
                var datetime = moment(date).format('YYYY-MM-DD');
                $("#shophistorydate").val(datetime);
                $("#note").val(result.note);
                $("#status").val(result.status).change();
                $("#FilterEdit").modal('show');
            },
            error: function (errormessage) {
                toastr.error("Something unexpected happenn.", "Server Response");
            }
        });
    
}