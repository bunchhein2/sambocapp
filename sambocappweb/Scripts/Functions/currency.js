﻿
$(document).ready(function () {
    $("#currencyModel").on('show.modal.bs', function () {
        GetCurrency();
    });
    GetCurrency();
    //var inputVal1 = document.getElementById("search").value = "Inactive";
    //oTable = $('#tblcurrency').DataTable();
    //oTable.columns(3).search($('#search').val().trim());
    //oTable.draw();
});
var tableCurren = [];
$("#buttonAddNewcurrency").click(function () {
    $('#txtbtnCurrency').val("Save");
    document.getElementById('btnCurrency').innerText = "Save";
});
function GetCurrency() {
    tableCase = $('#tblcurrency').DataTable({
        ajax: {
            url: "/api/Currencies",
            dataSrc: ""
        },
        columns: [
            {

                data: "id",
                render: function (data) {

                    return "" + ("").slice(-4);
                }
            },
            {
                data: "id",
                render: function (data) {
                    return "C" + ("00" + data).slice(-4);
                }
            },
            {

                data: "currencyname"
            },
            {

                data: "sign"
            },
            {

                data: "status"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='CurrencyEdits(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='CurrencyDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false,
        "paginat": false
    });
}

function CurrencyAction() {
    var action = '';
    action = $('#txtbtnCurrency').val();
    if (action == "Save") {
        if ($('#currencyname').val().trim() === "") {
            $('#currencyname').css('border-color', 'red');
            $('#currencyname').focus();
            toastr.info("Please enter currencyname", "Required");
        } else if ($('#currencysign').val().trim() === "") {
            $('#currencysign').css('border-color', 'red');
            $('#currencysign').focus();
            toastr.info("Please enter currencysign", "Required");
        } else if ($('#currencystatus').val().trim() === "") {
            $('#currencystatus').css('border-color', 'red');
            $('#currencystatus').focus();
            toastr.info("Please enter status", "Required");
        } else {
                    $('#currencyname').css('border-color', '#cccccc');
                    $('#currencysign').css('border-color', '#cccccc');
                    $('#currencystatus').css('border-color', '#cccccc');
                    var data = new FormData();
                    data.append("currencyname", $('#currencyname').val());
                    data.append("currencysign", $('#currencysign').val());
            data.append("currencystatus", $('#currencystatus').val());
                    $.ajax({
                        url: "/api/Currencies",
                        data: data,
                        type: "POST",
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            toastr.success("New Currencies has been Created", "Server Respond");
                            $('#tblcurrency').DataTable().ajax.reload();
                            ClearControl();
                            $("#currencyModel").modal('hide');

                        },
                        error: function (errormesage) {
                            toastr.error("Exist in Database", "Server Respond")
                        }

                    });
               }
    } else if (action == "Update") {
        action = document.getElementById('btnCurrency').innerText;
        action = $('#txtbtnCurrency').val();

        var data = new FormData();
        data.append("id", $('#currencyid').val());
        data.append("currencyname", $('#currencyname').val());
        data.append("currencysign", $('#currencysign').val());
        data.append("currencystatus", $('#currencystatus').val());
        $.ajax({
            url: "/api/Currencies/" + $("#currencyid").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Currency has been Updated", "Server Respond");

                action = document.getElementById('btnCurrency').innerText = "Save";
                $('#txtbtnCurrency').val("Save");

                $('#tblcurrency').DataTable().ajax.reload();
                $("#currencyModel").modal('hide');
                ClearControl();

            },
            error: function (errormesage) {
                toastr.error("Exit Updated in Database", "Server Respond")
            }
        });
    }
}

    function CurrencyEdits(id) {
        action = document.getElementById('btnCurrency').innerText = "Update";
        action =$('#txtbtnCurrency').val();

        $.ajax({
            url: "/api/Currencies/" + id,
            type: "GET",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                $('#currencyid').val(result.id);

                $('#currencyname').val(result.currencyname);
                $('#currencysign').val(result.sign);
                $('#currencystatus').select(result.status);
                $("#currencyModel").modal('show');
                $('#txtbtnCurrency').val("Update");
            },
            error: function (errormessage) {
                toastr.error("Something unexpected happenn.", "Server Response");
            }
        });
    }

    function CurrencyDelete(id) {
        // alert('hi');
        bootbox.confirm({
            title: "Confirmation",
            message: "Are you sure to delete this?",
            buttons: {
                cancel: {
                    label: "Cancel",
                    className: "btn-default"
                },
                confirm: {
                    label: "Delete",
                    className: "btn-danger"
                }
            },
            callback: function (result) {
                if (result) {
                    $.ajax({
                        url: "/api/Currencies/" + id,
                        type: "DELETE",
                        contentType: "application/json;charset=utf-8",
                        datatype: "json",
                        success: function (result) {
                            $('#tblcurrency').DataTable().ajax.reload();
                            toastr.success("Item Deleted successfully!", "Service Response");
                            $('#tblcurrency').DataTable().ajax.reload();
                        },
                        error: function (errormessage) {
                            toastr.error("This Item is already exists in Database", "Service Response");
                        }
                    });
                }
            }
        });
    }

    function ClearControl() {
        $('#currencyname').val('');
        $('#currencysign').val('');
    }
