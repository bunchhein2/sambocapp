﻿
$(document).ready(function () {
    GetQrcode();

});
function readURLQrcode(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#qrCodePhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
var tableQrcode = [];
function GetQrcode() {
    tableQrcode = $('#tblQrcode').DataTable({
        ajax: {
            url: "/api/Qrcode",
            dataSrc: ""
        },
        columns: [
            {
                data: "id",
                render: function (data) {

                    return "" + ("").slice(-4);
                }
            },
             {
                 data: "qrcode",
                 render: function (data, type, meta, full) {
                     //console.log(data);

                     return '<image width="100px" height="100%" src="../Images/' + data + '"/>';
                 }
            },
            {
                data: "createdate",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "id",
                render: function (data) {
                    return "<button type='button' onclick='QrcodeEdits(" + data + ")' class='btn btn-warning btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-edit'></span> Edit</button> " +
                        "<button type='button' onclick='QrcodeDelete(" + data + ")' class='btn btn-danger btn-xs' style='margin-left: 20px;'><span class='glyphicon glyphicon-remove'></span> Delete</button>"
                }

            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false,
        "paging": true,
        "lengthMenu": [25, 50, 100, "All"]
    });
}
function QrcodeAction() {
    var action = '';
    action = document.getElementById('btnQrcode').innerText;

    if (action == "Add New") {
        document.getElementById('btnQrcode').innerText = 'Save';
        EnableControlQr();
        ClearControlQr();
        //$('#amount').focus();
        //$('#date').val(moment().format('YYYY-MM-DD'));
        $('#qrCodePhoto').attr('src', '../Images/company.png');
        //$('#amount').val('0.00');
    }
    else if (action == "Save") {
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("qrcode", files[0]);
        }

        $.ajax({
            url: "/api/Qrcode",
            data: data,
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Save To Database Successfully", "Server Respond");
                $('#tblQrcode').DataTable().ajax.reload();
                //document.getElementById('btnQrcode').innerText = 'Add New';
                //DisableControlQr();
                $("#qrcodeModal").modal('hide');
            },
            error: function (errormesage) {
                //$('#amount').focus();
                toastr.error("This Name is exist in Database", "Server Respond")
            }

        });

    } else if (action == "Update") {
        var data = new FormData();

        var files = $('#file').get(0).files;

        if (files.length > 0) {
            data.append("qrcode", files[0]);
        }

        data.append("id", $('#ID').val());

        data.append("file_old", $("#file_old").val());

        $.ajax({
            url: "/api/Qrcode/" + $("#ID").val(),
            data: data,
            type: "PUT",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (result) {
                toastr.success("Data has been Updated", "Server Respond");
                $('#tblQrcode').DataTable().ajax.reload();
                //document.getElementById('btnQrcode').innerText = 'Add New';
                //DisableControlQr();
                $("#qrcodeModal").modal('hide');
            },
            error: function (errormesage) {
                toastr.error("Data hasn't Updated in Database", "Server Respond")
            }
        });
    }
}
function QrcodeEdits(id) {
    ClearControlQr();
    EnableControlQr();
    action = document.getElementById('btnQrcode').innerText = "Update";

    $.ajax({
        url: "/api/Qrcode/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#ID').val(result.id);

            $('#file_old').val(result.qrcode);

            if (result.screenshot == "") {
                $("#qrCodePhoto").attr('src', '../Images/company.png');
            } else {
                $("#qrCodePhoto").attr('src', '../Images/' + result.qrcode);
            }

            $("#qrcodeModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
}
function QrcodeDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Qrcode/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblQrcode').DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}

function DisableControlQr() {
    document.getElementById('file').disabled = true;
};
function EnableControlQr() {
    document.getElementById('file').disabled = false;
};
function ClearControlQr() {
    $('#qrCodePhoto').attr('src', '../Images/company.png');
}
function AddNewActionControl() {
    //DisableControlQr();
    ClearControlQr();
    document.getElementById('btnQrcode').innerText = 'Save';
}