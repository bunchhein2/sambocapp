﻿$(document).ready(function () {
    //$('#productModal').on('show.bs.modal', function () {
    //    var proCode = db.Products.OrderByDescending(c => c.Id).FirstOrDefault();
    //    if (id != 0) {
    //        proCode = db.Products.Where(x => x.Id == id).FirstOrDefault.Products();

    //    }
    //    else if (proCode == null) {
    //        Pro.ProductCode = "Code 001";
    //        $('#productcode').val(Pro);
    //    }
    //    else {
    //        Pro.ProductCode = "Code" + (Convert.ToInt32(Pro.ProductCode.Substring(9, Pro.ProductCode.Length - 9)) + 1).ToString();
    //        $('#productcode').val(Pro);
    //    }

    //});
    $(document).ajaxStart(function () {
        Pace.start();
        $("#loadingGif").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("#loadingGif").removeClass('show');
    });
    GetProducts();
   
    //var Countnumber1 = $("#Countnumber").val();
    //$("#productcode").val(Countnumber1);
    
});




$('#btnModalAddUpdateProduct').click(function () {
    $('#btnProductUpdadate').hide();
    $('#btnProductSave').show();
    $("input").prop('disabled', false);
    $("textarea").prop('disabled', false);
    $("select").prop('disabled', false);
})

var tableProducts = [];
function GetProducts() {
    tableProducts = $('#tableProducts').DataTable({
        ajax: {
            url: "/api/products",
            dataSrc: ""
        },
        columns: [
            
            
            {
                data: "id",
                render: function (data) {

                    return "" + ("").slice(-4);
                }
            },
            {
                data: "productcode"
            },
            
            {
                data: "shopName"
            },
            {
                data: "id",
                render: function (data) {
                    return "P" + ("00" + data).slice(-4);
                }
            },
            {
                data: "productname"
            },
            {
                data: "description"
            },
            {
                data: "qtyInStock"
            },
            {
                data: "price"
            },
            {
                data: "currencyId"
            },
            //{
            //    data: "cutStockType"
            //},
            {
                data: "expiredDate",
                render: function (data) {
                    return moment(data).format('DD-MMM-YYYY');
                }
            },
            //{
            //    data: "linkVideo"
            //},
            //{
            //    data: "imageThumbnail"
            //},
            {
                data: "status"
            },
            
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='ProductImage(" + data + ")' class='btn btn-primary btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-eye-open'></span>Images</button>" +
                        "<button onclick='ProductEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span>Edit</button>" +
                        "<button onclick='ProductDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span>Delete</button>";
                }
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false,
        "paging": true,
        lengthMenu: [
            [50, -1],
            [50, 'All'],
        ],
    });
}
function ProductImage(id) {
    $("#productimageid").val(id);

    $("#Addmoreimages").modal('show');

}
function SaveProductimageAction() {
    var action = '';
    action = document.getElementById('btnuplaodimage').innerText;
    
    if (action == "Save") {
        var data = new FormData();
        var files = $("#productimage").get(0).files;

        if (files.length > 0) {
            data.append("productimage", files[0]);
        }
        data.append("productid", $("#productimageid").val());

        var ajaxRequest = $.ajax({
            type: "POST",
            url: "/api/ProductImages",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("ProductImage has been added to database.");
                tableProducts.ajax.reload();
                //$('#Addmoreimages').modal('hide');
            },
            statusCode: {
                400: function () {
                    $('#productname').focus();
                    toastr.error("This productImage is already exists in database.");
                }
            }
        });


    } else if (action == "Update") {
        alert("Can not UPDATE 'Delet only...!'")
        
    }

}
function SaveProductAction() {
    var res = validateForm();
    if (res == false) {
        return false;
    }
        
    
    var data = new FormData();

    var files = $("#file").get(0).files;
    if (files.length > 0) {
        data.append("ImageThumbnail", files[0]);
    }
    var files = $('#linkvideo').get(0).files;

    if (files.length > 0) {
        data.append("linkvideo", files[0]);
    } else {
        data.append("linkvideo", "");
    }
    data.append("ShopId", $("#shopid").val());     //set id textbox to model
    data.append("ProductCode", $("#productcode").val());
    data.append("ProductName", $("#productname").val());
    data.append("Description", $("#description").val());
    data.append("QtyInStock", $("#qtyinstock").val());
    data.append("Price", $("#price").val());
    data.append("CurrencyId", $("#currencyid").val());
    data.append("CutStockType", $("#cutstocktype").val());
    data.append("ExpiredDate", $("#expireddate").val());
    //data.append("LinkVideo", $("#linkvideo").val());
    data.append("Status", $("#status").val());

    var ajaxRequest = $.ajax({
        type: "POST",
        url: "/api/products",
        contentType: false,
        processData: false,
        data: data,
        success: function (result) {
            toastr.success("Product has been added to database.");
            $('#productModal').modal('hide');
            $('#tableProducts').DataTable().ajax.reload();
        },
        statusCode: {
            400: function () {
                $('#shopid').focus();
                toastr.error("This product is already exists in database.");
            }
        }
    });
}

function UpdateProductAction() {
    var res = validateForm();
    if (res == false) {
        return false;
    }

    var data = new FormData();

    var files = $("#file").get(0).files;

    if (files.length > 0) {
        data.append("imageThumbnail", files[0]);
    }

    var files = $('#linkvideo').get(0).files;
    if (files.length > 0) {
        data.append("LinkVideo", files[0]);
    }
    data.append("Id", $("#productid").val());
    data.append("ShopId", $("#shopid").val());
    data.append("ProductCode", $("#productcode").val());
    data.append("ProductName", $("#productname").val());
    data.append("Description", $("#description").val());
    data.append("QtyInStock", $("#qtyinstock").val());
    data.append("Price", $("#price").val());
    data.append("CurrencyId", $("#currencyid").val());
    data.append("CutStockType", $("#cutstocktype").val());
    data.append("ExpiredDate", $("#expireddate").val());
    //data.append("LinkVideo", $("#linkvideo").val());
    data.append("Status", $("#status").val());
    data.append("file_old", $("#file_old").val());
    data.append("video_old", $("#video_old").val());

    var ajaxRequest = $.ajax({
        type: "Put",
        url: "/api/products/" + $('#productid').val(),
        contentType: false,
        processData: false,
        data: data,
        success: function (result) {
            toastr.success("Product has been updated to database.");
            tableProducts.ajax.reload();
            $('#productModal').modal('hide');
            window.location.reload();
        },
        statusCode: {
            400: function () {
                $('#shopid').focus();
                toastr.error("This product can not be updated");
            }
        }
    });
}

function ProductEdit(id) {
    EnableFormProduct();
    $.ajax({
        url: "/api/products/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#productid').val(result.id);
            
            var pd = moment(result.expiredDate).format("YYYY-MM-DD");
            $('#expireddate').val(pd);
            //$('#linkvideo').val(result.linkVideo);
            $('#productcode').val(result.productCode);
            $('#productname').val(result.productName);
            $('#description').val(result.description);
            $('#qtyinstock').val(result.qtyInStock);
            $('#price').val(result.price);
            $('#cutstocktype').val(result.cutStockType);
            $('#currencyid').val(result.currencyId);
            $('#status').select(result.status);
            $('#shopid').val(result.shopId);
            

            //var files = $('#productImg').get(0).files;
            if (result.productPhoto == "") {
                $('#productImg').attr('src', '../Images/product.jpg');
            }
            else {
                $('#productImg').attr('src', '../Images/' + result.imageThumbnail);
            }

            //var files = $('#videoDemo').get(0).files;
            if (result.linkVideo == null) {
                $("#videoDemo").attr('src', '')
            } else {
                $("#videoDemo").attr('src', '../Video/' + result.linkVideo)
            }
            $('#file_old').val(result.imageThumbnail);
            $('#video_old').val(result.linkVideo);

            $('#productModal').modal('show');
            $('#btnProductUpdadate').show();
            $('#btnProductSave').hide();
        },
        statusCode: {
            400: function () {
                toastr.error("This product is already exists in database.");
            }
        }
    });
    return false;
}
//================================
function saveadd() {
    $('#btnProductUpdadate').hide();
    $('#btnProductSave').show();
    clearControll();
    //var Countnumber1 = $("#Countnumber").val();
    //$("#productcode").val(Countnumber1);
}

//$("#qtymonth").change(function () {
//    var bannerdate = $("#bannerdate").val();
//    var qty = $(this).val();
//    /*var expire =  Date.parse(bannerdate);*/
//    //alert(bannerdate);
//    if ($('#bannerdate').val().trim() === "") {
//        $('#bannerdate').css('border-color', 'red');
//        toastr.warning("Please Select Start Date", "Required");
//    } else if ($('#qtymonth').val().trim() === "") {
//        toastr.warning("Please Select QtyMonth", "Required");
//        $('#qtymonth').css('border-color', 'red');
//    } else {
//        var expire = moment(bannerdate, "YYYY-MM-DD").add(qty, 'months').format('YYYY-MM-DD');

//        //alert(qty + " | " + bannerdate + "| " + expire + " |");
//        $("#bannerexireddate").val(expire);
//    }

//});
    function ProductDetail(id) {
        
        $.ajax({
            url: "/api/products/" + id,
            type: "GET",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
               
                $('#productname').val(result.productid);
               

                $('#Addmoreimages').modal('show');
             
                //var a = document.getElementById("TableProduct");
                //var rows = a.rows.length;
                //alert(rows);
            }
        });
        return false;
    }



    function ProductDelete(Id) {
        

        bootbox.confirm({
            title: "Confirm",
            message: "Are you sure want to delete this?",
            buttons: {
                cancel: {
                    label: "Cancel",
                    className: "btn-default"
                },
                confirm: {
                    label: "Delete",
                    className: "btn-danger"
                }
            },

            callback: function (result) {
                //alert(id);
                if (result) {
                    $.ajax({
                        url: "/api/products/" + Id,
                        type: "DELETE",
                        contentType: "application/json;charset=utf-8",
                        datatype: "json",
                        success: function (result) {
                            $('#tableProducts').DataTable().ajax.reload();
                            toastr.success(" Deleted successfully!", "Service Response");
                        },
                        error: function (errormessage) {
                            toastr.error(" Can't be deleted", "Service Response");
                        }
                    });
                }
            }
        });
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#productImg').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function validateForm() {
        var isValid = true;
        if ($('#productcode').val().trim() == "") {
            $('#productcode').css('border-color', 'red');
            $('#productcode').focus();
            toastr.info('Please enter product code.');
            isValid = false;
        } else {
            $('#productcode').css('border-color', '#cccccc');
            if ($('#productname').val().trim() == "") {
                $('#productname').css('border-color', 'red');
                $('#productname').focus();
                toastr.info('Please enter product name.');
                isValid = false;
            }
            else {
                $('#productname').css('border-color', '#cccccc');
                if ($('#description').val().trim() == "") {
                    $('#description').css('border-color', 'red');
                    $('#description').focus();
                    toastr.info('Please enter description.');
                    isValid = false;
                } else {
                    $('#description').css('border-color', '#cccccc');
                    if ($('#qtyinstock').val().trim() == "") {
                        $('#qtyinstock').css('border-color', 'red');
                        $('#qtyinstock').focus();
                        toastr.info('Please enter qty in stock.');
                        isValid = false;
                    } else {
                        $('#qtyinstock').css('border-color', '#cccccc');
                        if ($('#price').val().trim() == "") {
                            $('#price').css('border-color', 'red');
                            $('#price').focus();
                            toastr.info('Please enter price.');
                            isValid = false;
                        } else {
                            $('#price').css('border-color', '#cccccc');
                            if ($('#currencyid').val().trim() == "") {
                                $('#currencyid').css('border-color', 'red');
                                $('#currencyid').focus();
                                toastr.info('Please choose currency.');
                                isValid = false;
                            }
                            else {
                                $('#currencyid').css('border-color', '#cccccc');
                                if ($('#cutstocktype').val().trim() == "") {
                                    $('#cutstocktype').css('border-color', 'red');
                                    $('#cutstocktype').focus();
                                    toastr.info('Please enter cutstock type.');
                                    isValid = false;
                                }
                                else {
                                    $('#cutstocktype').css('border-color', '#cccccc');
                                    if ($('#shopid').val().trim() == "") {
                                        $('#shopid').css('border-color', 'red');
                                        $('#shopid').focus();
                                        toastr.info('Please choose shop id.');
                                        isValid = false;
                                    }
                                    else {
                                        $('#shopid').css('border-color', '#cccccc');
                                        if ($('#expireddate').val().trim() == "") {
                                            $('#expireddate').css('border-color', 'red');
                                            $('#expireddate').focus();
                                            toastr.info('Please enter expired date.');
                                            isValid = false;
                                        }
                                        else {
                                            $('#expireddate').css('border-color', '#cccccc');

                                            //$('#linkvideo').css('border-color', '#cccccc');
                                            if ($('#status').val().trim() == "") {
                                                $('#status').css('border-color', 'red');
                                                $('#status').focus();
                                                toastr.info('Please enter status.');
                                                isValid = false;
                                            }
                                            else {
                                                $('#status').css('border-color', '#cccccc');
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return isValid;
    }

    function clearControll() {
        //$('#productcode').val('');
        $('#productname').val('');
        $('#description').val('');
        $('#qtyinstock').val('');
        $('#price').val('');
        $('#currencyid').val('');
        $('#cutstocktype').val('');
        $('#expireddate').val('');
        $('#cutstocktype').val('');
        $('#linkvideo').val('');
        $('#status').val('');
        $('#file').val('');
        $('#productImg').attr('src', '../Content/product.jpg')
        $('#shopid').css('border-color', '#cccccc');
    }

    function DisableFormProduct() {
        document.getElementById('productcode').disabled = true;
        document.getElementById('productname').disabled = true;
        document.getElementById('description').disabled = true;
        document.getElementById('qtyinstock').disabled = true;
        document.getElementById('price').disabled = true;
        document.getElementById('currencyid').disabled = true;
        document.getElementById('cutstocktype').disabled = true;
        document.getElementById('expireddate').disabled = true;
        document.getElementById('linkvideo').disabled = true;
        document.getElementById('status').disabled = true;
        document.getElementById('file').disabled = true;
        document.getElementById('shopid').disabled = true;
    }

    function EnableFormProduct() {
        document.getElementById('productcode').disabled = false;
        document.getElementById('productname').disabled = false;
        document.getElementById('description').disabled = false;
        document.getElementById('qtyinstock').disabled = false;
        document.getElementById('price').disabled = false;
        document.getElementById('currencyid').disabled = false;
        document.getElementById('cutstocktype').disabled = false;
        document.getElementById('expireddate').disabled = false;
        document.getElementById('linkvideo').disabled = false;
        document.getElementById('status').disabled = false;
        document.getElementById('file').disabled = false;
        document.getElementById('shopid').disabled = false;
    }

    $('#btnCloseProduct').click(function () {
        //$('#productcode').val('');
        $('#productname').val('');
        $('#description').val('');
        $('#qtyinstock').val('');
        $('#price').val('');
        $('#currencyid').val('');
        $('#cutstocktype').val('');
        $('#expireddate').val('');
        $('#cutstocktype').val('');
        $('#linkvideo').val('');
        $('#file').val('');
        $('#productImg').attr('src', '../Content/product.jpg')
        $('#shopid').css('border-color', '#cccccc');

        $('#productModal').modal('hide');
    });
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#productImg').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function readURLP(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#productAddIMG').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}


//--------------- --------------More Image ----------------------------------------------------------------

function SaveProductimageAction() {
    var action = '';
    action = document.getElementById('btnuplaodimage').innerText;

    if (action == "Save") {
        var data = new FormData();
        var files = $("#productimage").get(0).files;

        if (files.length > 0) {
            data.append("productimage", files[0]);
        }
        data.append("productid", $("#productimageid").val());

        var ajaxRequest = $.ajax({
            type: "POST",
            url: "/api/ProductImages",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("ProductImage has been added to database.");
                tableProducts.ajax.reload();
                $('#productAddIMG').attr('src', '../Content/product.jpg');
                //$('#Addmoreimages').modal('hide');
            },
            statusCode: {
                400: function () {
                    $('#productname').focus();
                    toastr.error("This productImage is already exists in database.");
                }
            }
        });


    } else if (action == "Update") {
        alert("Can not UPDATE 'Delet only...!'")

    }

}