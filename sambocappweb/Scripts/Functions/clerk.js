﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    onPopulateClerk();
    //$('#civilcaseModal').on('show.bs.modal', function () {
    //    // Bind Cases Select Options
    //    onPopulateClerk();
    //});
    $('#clerkModal').on('show.bs.modal', function () {
        GetClerk();
        document.getElementById('clerkName').disabled = true;
        document.getElementById('clerkSex').disabled = true;
        document.getElementById('clerkAge').disabled = true;
        document.getElementById('clerkAddress').disabled = true;
        document.getElementById('clerkIdentityno').disabled = true;
        document.getElementById('clerkPhone').disabled = true;
        document.getElementById('clerkPhoto').disabled = true;
        document.getElementById('btnClerkAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

        $('#clerkName').val('');
    });
});

var tableCase = [];

function onPopulateClerk() {
    $.ajax({
        url: "/api/clerks?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zcaseclerkid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function GetClerk() {
    tableCase = $('#ClerkTable').DataTable({
        ajax: {
            url: "/api/clerks",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "clerkname"
            },
            {
                data: "clerksex"
            },
            {
                data: "clerkaddress"
            },
            {
                data: "clerkidentityno"
            },
            {
                data: "clerkphone"
            },
            //{
            //    data: "photo"
            //},
            //{
            //    data: "status"
            //},
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='ClerkEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='ClerkDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function ClerkEdit(id) {

    $('#clerkName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/clerks/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#clerkId').val(result.id);
            $('#clerkName').val(result.clerkname);
            $('#clerkSex').val(result.clerksex);
            $('#clerkAge').val(result.clerkage);
            $("#clerkPhone").val(result.clerkphone);
            $("#clerkAddress").val(result.clerkaddress);
            $('#clerkIdentityno').val(result.clerkidentityno);
            $('#clerkfile_old').val(result.clerkphoto);
            //console.log(result);
            //alert(result.photo);
            if (result.clerkphoto == "") {
                $('#clerkPhoto').attr('src', '../Images/no_image.png');
            } else {
                //alert(result.img);
                $('#clerkPhoto').attr('src', '../Images/' + result.clerkphoto);
            }
            document.getElementById('clerkName').disabled = false;
            document.getElementById('clerkSex').disabled = false;
            document.getElementById('clerkAge').disabled = false;
            document.getElementById('clerkAddress').disabled = false;
            document.getElementById('clerkIdentityno').disabled = false;
            document.getElementById('clerkPhone').disabled = false;
            document.getElementById('btnClerkAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            $('#clerkModel').modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
    return false;
}

function ClerkAction() {
    var action = '';
    action = document.getElementById('btnClerkAction').innerText;
    if (action === " បង្កើតថ្មី / Add New") {
        //$('#dob').val(moment().format('YYYY-MM-DD'));
        document.getElementById('btnClerkAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        document.getElementById('clerkName').disabled = false;
        document.getElementById('clerkSex').disabled = false;
        document.getElementById('clerkAge').disabled = false;
        document.getElementById('clerkAddress').disabled = false;
        document.getElementById('clerkIdentityno').disabled = false;
        document.getElementById('clerkPhone').disabled = false;
        $('#clerkPhoto').attr('src', '../Images/no_image.png');
        $("#clerkName").focus();
    } else if (action === " រក្សាទុក / Save") {
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
        var data = new FormData();
        var files = $("#clerkfile").get(0).files;
        if (files.length > 0) {
            data.append("clerkphoto", files[0]);
        }

        data.append("clerkname", $("#clerkName").val());
        data.append("clerksex", $("#clerkSex").val());
        data.append("clerkage", $("#clerkAge").val());
        data.append("clerkaddress", $("#clerkAddress").val());
        data.append("clerkidentityno", $("#clerkIdentityno").val());
        data.append("clerkphone", $("#clerkPhone").val());

        //console.log(files[0]);

        $.ajax({
            type: "POST",
            url: "/api/clerks",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateClerk();
                document.getElementById('clerkName').disabled = true;
                //document.getElementById('caseNote').disabled = true;
                document.getElementById('btnClerkAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#clerkName').val('');
            },
            error: function (errormessage) {
                toastr.error("This clerk is already exists.", "Server Response");
            }
        });

        //maritalstatus

    } else if (action === " កែប្រែ / Update") {
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
        var data = new FormData();
        var files = $("#clerkfile").get(0).files;
        if (files.length > 0) {
            data.append("clerkPhoto", files[0]);
        }
        data.append("clerkid", $('#clerkId'));
        data.append("clerkname", $("#clerkName").val());
        data.append("clerksex", $("#clerkSex").val());
        data.append("clerkage", $("#clerkAge").val());
        data.append("clerkaddress", $("#clerkAddress").val());
        data.append("clerkidentityno", $("#clerkIdentityno").val());
        data.append("clerkphone", $("#clerkPhone").val());
        data.append("clerkfile_old", $("#clerkfile_old").val());

        console.log(data);

        $.ajax({
            type: "PUT",
            url: "/api/clerks/" + $('#clerkId').val(),
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateClerk();
                document.getElementById('clerkName').disabled = true;
                document.getElementById('btnClerkAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#clerkName').val('');
                //$('#caseNote').val('');
            },
            error: function (errormessage) {
                toastr.error("This clerk is already exists.", "Server Response");
            }
        });

    }
}

function ClerkDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/clerks/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        onPopulateClerk();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This clerk is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function readURLClerk(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#clerkPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}