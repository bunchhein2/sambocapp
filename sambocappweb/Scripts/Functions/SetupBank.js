﻿
$(document).ready(function () {
    //$("#Model").on('show.modal.bs', function () {
       
    //});
    GetBank();
});
var tblsetupbank = [];
function GetBank() {
    tableCase = $('#tblsetupbank').DataTable({
        ajax: {
            url: "/api/SetUpBanks",
            dataSrc: ""
        },
        columns: [
            {

                data: "id",
                render: function (data) {

                    return "" + ("").slice(-4);
                }
            },
            {

                data: "methodname"
            },
            {

                data: "createdate",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMMM-YYYY');
                }
            },
            
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='Edits(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='Delete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "asc"]],
        "info": true
    });
}
function setupAction() {
    var action = '';
    action = $("#txtbtnSetupbank").val();
    if (action == "Save") {
        if ($('#methodname').val().trim() === "") {
            $('#methodname').css('border-color', 'red');
            $('#methodname').focus();
            toastr.info("Please enter methodname", "Required");
        }
        else {
            $('#methodname').css('border-color', '#cccccc');
            var data = new FormData();
            data.append("methodname", $('#methodname').val());
            $.ajax({
                url: "/api/SetUpBanks",
                data: data,
                type: "POST",
                contentType: false,
                processData: false,
                success: function (result) {
                    toastr.success("New Bank has been Created", "Server Respond");
                    $('#tblsetupbank').DataTable().ajax.reload();
                    ClearControl();
                    //$("#setupbankModel").modal('hide');

                },
                error: function (errormesage) {
                    toastr.error("Exist in Database", "Server Respond")
                }

            });
        }
    } else if (action == "Update") {
        
       
            var data = new FormData();
            data.append("id", $('#id').val());
            data.append("methodname", $('#methodname').val());
            $.ajax({
                url: "/api/SetUpBanks/" + $("#id").val(),
                data: data,
                type: "PUT",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (result) {
                    toastr.success("Updated", "Server Respond");

                    action = document.getElementById('btnSetupbank').innerText = "Save";
                    $('#txtbtnSetupbank').val("Save");

                    $('#tblsetupbank').DataTable().ajax.reload();
                    //$("#setupbankModel").modal('hide');
                    ClearControl();

                },
                error: function (errormesage) {
                    toastr.error("Exit Updated in Database", "Server Respond")
                }
            });
        
        
    }
}

function Edits(id) {
    action = document.getElementById('btnSetupbank').innerText = "Update";
    $('#txtbtnSetupbank').val("Update");
    $.ajax({
        url: "/api/SetUpBanks?id=" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#id').val(result.id);
            $('#methodname').val(result.methodname);
            $("#setupbankModel").modal('show');},
        error: function (errormessage) {
            toastr.error("Something unexpected happenn.", "Server Response");
        }
    });
}

function Delete(id) {
    // alert('hi');
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/SetUpBanks?id=" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblsetupbank').DataTable().ajax.reload();
                        toastr.success("Item Deleted successfully!", "Service Response");
                        
                    },
                    error: function (errormessage) {
                        toastr.error("This Item is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
}

function ClearControl() {
    $('#methodname').val('');
}
