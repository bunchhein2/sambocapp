﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    onPopulateLawyer();
    onPopulateCaseLawyer();
    onPopulateCaseProccedingLawyer();
    //$('#civilcaseModal').on('show.bs.modal', function () {
    //    // Bind Cases Select Options
    //    onPopulateLawyer();
    //    onPopulateCaseLawyer();
    //    onPopulateCaseProccedingLawyer();
    //});

    $('#lawyerModal').on('show.bs.modal', function () {
        GetLawyers();
        
        document.getElementById('lawyerName').disabled = true;
        document.getElementById('lawyerSex').disabled = true;
        document.getElementById('lawyerAge').disabled = true;
        document.getElementById('lawyerAddress').disabled = true;
        document.getElementById('lawyerIdentityno').disabled = true;
        document.getElementById('lawyerPhone').disabled = true;
        document.getElementById('lawyerPhoto').disabled = true;
        document.getElementById('lawyerfile').disabled = true;
        document.getElementById('btnLawyerAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";

        $('#lawyerName').val('');
        $('#lawyerSex').val('');
        $('#lawyerAge').val('');
        $('#lawyerAddress').val('');
        $('#lawyerIdentityno').val('');
        $('#lawyerPhone').val('');
        $('#lawyerPhoto').attr('src', '../Images/no_image.png');
        $('#lawyerfile').val('');
    });
});

var tableCase = [];

function onPopulateLawyer() {
    $.ajax({
        url: "/api/lawyers?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zlawyerid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function onPopulateCaseProccedingLawyer() {
    $.ajax({
        url: "/api/lawyers?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zcaseprocedinglawyerid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function onPopulateCaseLawyer() {
    $.ajax({
        url: "/api/lawyers?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zcaselawyerids").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}



function GetLawyers() {
    tableCase = $('#LawyerTable').DataTable({
        ajax: {
            url: "/api/lawyers",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "name"
            },
            {
                data: "sex"
            },
            {
                data: "address"
            },
            {
                data: "identityno"
            },
            {
                data: "phone"
            },
            //{
            //    data: "photo"
            //},
            //{
            //    data: "status"
            //},
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='LawyerEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='LawyerDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function LawyerEdit(id) {

    $('#lawyerName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/lawyers/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#lawyerId').val(result.id);
            $('#lawyerName').val(result.name);
            $('#lawyerSex').val(result.sex);
            $('#lawyerAge').val(result.age);
            $("#lawyerPhone").val(result.phone);
            $("#lawyerAddress").val(result.address);
            $('#lawyerIdentityno').val(result.identityno);
            $('#lawyerfile_old').val(result.photo);
            //console.log(result);
            //alert(result.photo);
            if (result.photo == "") {
                $('#lawyerPhoto').attr('src', '../Images/no_image.png');
            } else {
                //alert(result.img);
                $('#lawyerPhoto').attr('src', '../Images/' + result.photo);
            }
            document.getElementById('lawyerName').disabled = false;
            document.getElementById('lawyerSex').disabled = false;
            document.getElementById('lawyerAge').disabled = false;
            document.getElementById('lawyerAddress').disabled = false;
            document.getElementById('lawyerIdentityno').disabled = false;
            document.getElementById('lawyerPhone').disabled = false;
            document.getElementById('lawyerfile').disabled = false;
            document.getElementById('btnLawyerAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            $('#lawyerModel').modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
    return false;
}

function LawyerAction() {
    var action = '';
    action = document.getElementById('btnLawyerAction').innerText;
    if (action === " បង្កើតថ្មី / Add New") {
        //$('#dob').val(moment().format('YYYY-MM-DD'));
        document.getElementById('btnLawyerAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        document.getElementById('lawyerName').disabled = false;
        document.getElementById('lawyerSex').disabled = false;
        document.getElementById('lawyerAge').disabled = false;
        document.getElementById('lawyerAddress').disabled = false;
        document.getElementById('lawyerIdentityno').disabled = false;
        document.getElementById('lawyerPhone').disabled = false;
        document.getElementById('lawyerfile').disabled = false;
        $('#lawyerName').val('');
        $('#lawyerSex').val('');
        $('#lawyerAge').val('');
        $('#lawyerAddress').val('');
        $('#lawyerIdentityno').val('');
        $('#lawyerPhone').val('');
        $('#lawyerPhoto').attr('src', '../Images/no_image.png');
        $('#lawyerfile').val('');
        $("#lawyerName").focus();
    } else if (action === " រក្សាទុក / Save") {
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
        var data = new FormData();
        var files = $("#lawyerfile").get(0).files;
        if (files.length > 0) {
            data.append("lawyerphoto", files[0]);
        }

        data.append("lawyername", $("#lawyerName").val());
        data.append("lawyersex", $("#lawyerSex").val());
        data.append("lawyerage", $("#lawyerAge").val());
        data.append("lawyeraddress", $("#lawyerAddress").val());
        data.append("lawyeridentityno", $("#lawyerIdentityno").val());
        data.append("lawyerphone", $("#lawyerPhone").val());

        //console.log(files[0]);

        $.ajax({
            type: "POST",
            url: "/api/lawyers",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateLawyer();
                onPopulateCaseLawyer();
                onPopulateCaseProccedingLawyer();
                document.getElementById('lawyerName').disabled = true;
                document.getElementById('lawyerSex').disabled = true;
                document.getElementById('lawyerAge').disabled = true;
                document.getElementById('lawyerAddress').disabled = true;
                document.getElementById('lawyerIdentityno').disabled = true;
                document.getElementById('lawyerPhone').disabled = true;
                document.getElementById('lawyerPhoto').disabled = true;
                document.getElementById('lawyerfile').disabled = true;
                document.getElementById('btnLawyerAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#lawyerName').val('');
                $('#lawyerSex').val('');
                $('#lawyerAge').val('');
                $('#lawyerAddress').val('');
                $('#lawyerIdentityno').val('');
                $('#lawyerPhone').val('');
                $('#lawyerPhoto').attr('src', '../Images/no_image.png');
                $('#lawyerfile').val('');

            },
            error: function (errormessage) {
                toastr.error("This lawyer is already exists.", "Server Response");
            }
        });

        //maritalstatus

    } else if (action === " កែប្រែ / Update") {
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
        var data = new FormData();
        var files = $("#lawyerfile").get(0).files;
        if (files.length > 0) {
            data.append("lawyerPhoto", files[0]);
        }
        data.append("lawyerid", $('#lawyerId'));
        data.append("lawyername", $("#lawyerName").val());
        data.append("lawyersex", $("#lawyerSex").val());
        data.append("lawyerage", $("#lawyerAge").val());
        data.append("lawyeraddress", $("#lawyerAddress").val());
        data.append("lawyeridentityno", $("#lawyerIdentityno").val());
        data.append("lawyerphone", $("#lawyerPhone").val());
        data.append("lawyerfile_old", $("#lawyerfile_old").val());

        console.log(data);

        $.ajax({
            type: "PUT",
            url: "/api/lawyers/" + $('#lawyerId').val(),
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateLawyer();
                onPopulateCaseLawyer();
                onPopulateCaseProccedingLawyer();
                document.getElementById('lawyerName').disabled = true;
                document.getElementById('lawyerSex').disabled = true;
                document.getElementById('lawyerAge').disabled = true;
                document.getElementById('lawyerAddress').disabled = true;
                document.getElementById('lawyerIdentityno').disabled = true;
                document.getElementById('lawyerPhone').disabled = true;
                document.getElementById('lawyerPhoto').disabled = true;
                document.getElementById('lawyerfile').disabled = true;
                document.getElementById('btnLawyerAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#lawyerName').val('');
                $('#lawyerSex').val('');
                $('#lawyerAge').val('');
                $('#lawyerAddress').val('');
                $('#lawyerIdentityno').val('');
                $('#lawyerPhone').val('');
                $('#lawyerPhoto').attr('src', '../Images/no_image.png');
                $('#lawyerfile').val('');

            },
            error: function (errormessage) {
                toastr.error("This lawyer is already exists.", "Server Response");
            }
        });

    }
}

function LawyerDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/lawyers/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        onPopulateLawyer();
                        onPopulateCaseLawyer();
                        onPopulateCaseProccedingLawyer();
                        toastr.success("Deleted successfully.", "Server Response");
                        $('#lawyerName').val('');
                        $('#lawyerSex').val('');
                        $('#lawyerAge').val('');
                        $('#lawyerAddress').val('');
                        $('#lawyerIdentityno').val('');
                        $('#lawyerPhone').val('');
                        $('#lawyerPhoto').attr('src', '../Images/no_image.png');
                        $('#lawyerfile').val('');
                    },
                    error: function () {
                        toastr.error("This lawyer is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function readURLLLawyer(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#lawyerPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}