﻿$(document).ready(function () {

    $(document).ajaxStart(function () {
        $('#loadingGif').addClass('show');
    }).ajaxStop(function () {
        $('#loadingGif').removeClass('show');
    });

    $('#incometypeModel').on('show.bs.modal', function () {

        GetExpenseType();
        document.getElementById('incometypename').disabled = true;
        document.getElementById('btnIncomeType').innerText = "Add New";

    })
})

var tableDepartment = [];
toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
toastr.options.positionClass = 'toast-bottom-right';

function GetExpenseType() {
    //alert('Hello');
    tableDepartment = $('#incometypeTable').DataTable({
        ajax: {
            url: "/api/IncomeType",
            dataSrc: ""
        },
        columns: [
            //{
            //    data:"id"
            //},
            {
                data: "incometypename"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='incometypeEdit(" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px;'>Edit</button>" + "<button onclick='incomeDelete(" + data + ")' class='btn btn-danger btn-xs'>Delete</button>";
                }
            }
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });
}

function IncomeTypeAction() {
    var action = '';
    action = document.getElementById('btnIncomeType').innerText;
    if (action == "Add New") {
        document.getElementById('btnIncomeType').innerText = "Save";
        document.getElementById('incometypename').disabled = false;
        $('#incometypename').focus();

    } else if (action == "Save") {
        if ($('#incometypename').val().trim() == "") {
            $('#incometypename').css('border-color', 'red');
            $('#incometypename').focus();
            toastr.info('Please enter Department Name.', "Server Response")
        } else {
            $('#incometypename').css('border-color', '#cccccc');

            var dataSave = {
                incometypename: $('#incometypename').val()
            };
            //alert($('#incometypename').val());

            $.ajax({
                url: "/api/IncomeType",
                data: JSON.stringify(dataSave),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                datatype: "json",
                success: function (result) {
                    toastr.success("IncomeType has been created successfully.", "Server Response");
                    tableDepartment.ajax.reload();
                    document.getElementById('btnIncomeType').innerText = "Add New";
                    $('#incometypename').val('');
                    document.getElementById('incometypename').disabled = true;
                },
                error: function (errormessage) {
                    toastr.error("This IncomeType is already exists in Database", "Service Response");
                }
            })
        }
    } else if (action == "Update") {
        if ($('#incometypename').val().trim() == "") {
            $('#incometypename').css('border-color', 'red');
            $('#incometypename').focus();
            toastr.info('Please enter Category Name.', "Server Response")
        } else {
            $('#incometypename').css('border-color', '#cccccc');

            var data = {
                id: $('#incometypeid').val(),
                incometypename: $('#incometypename').val()
            };

            //console.log(data);

            $.ajax({
                url: "/api/IncomeType/" + data.id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                datatype: "json",
                success: function (result) {
                    toastr.success("IncomeType has been update successfully.", "Server Response");
                    tableDepartment.ajax.reload();
                    document.getElementById('btnIncomeType').innerText = "Add New";
                    $('#incometypename').val('');
                    document.getElementById('incometypename').disabled = true;
                },
                error: function (errormessage) {
                    toastr.error("This IncomeType is already exists in Database", "Service Response");
                }
            })
        }
    }

}

function incometypeEdit(id) {
    $.ajax({
        url: "/api/IncomeType/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#incometypeid').val(result.id);
            $('#incometypename').val(result.incometypename);
            document.getElementById('btnIncomeType').innerText = "Update";
            document.getElementById('incometypename').disabled = false;
            $('#incometypename').focus();
        },
        error: function (errormessage) {
            toastr.error("This IncomeType is already exists in Database", "Service Response");
        }
    });

}

function incomeDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/IncomeType/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        tableDepartment.ajax.reload();
                        toastr.success("IncomeType has been Deleted successfully!", "Service Response");
                        document.getElementById('incometypename').disabled = true;
                        document.getElementById('btnIncomeType').innerText = "Add New";
                    },
                    error: function (errormessage) {
                        toastr.error("This IncomeType cannot delete it already use in Database", "Service Response");
                    }
                });
            }
        }
    });
}