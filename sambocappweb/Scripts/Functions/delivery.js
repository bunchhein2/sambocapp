﻿$(document).ready(function () {
    //alert('hi');
    GetCompany();    
    //$('#companyModal').on('show.bs.modal', function () {

    //    //document.getElementById('companyTypeName').disable = true;
    //});
});
var tableCompany = [];

function GetCompany() {
    tableCompany = $('#tblDelivery').dataTable({
        ajax: {
            url: "/api/DeliveryTypes",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id",
                    render: function (data) {
                        return "" + ("").slice(-4);
                    }
                },
                {
                    data: "id",
                    render: function (data) {
                        return "D" + ("00" + data).slice(-4);
                    }
                },
                {
                    data: "delivery_name"
                },
                {
                    data: "status"
                },               
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='CustomerEdit (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='CustomerDelete (" + data + ")' class='btn btn-danger btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                }
            ],
        destroy: true,

    });
}

function CustomerAction() {
    var action = '';
    action = document.getElementById('btnSaveDelivery').innerText;
    //alert(action);

    if (action == "Add New") {
        document.getElementById('btnSaveDelivery').innerText = 'Save';
        EnableControl();
        $('#deliveryName').focus();
    }
    else if (action == "Save") {

        Validate();
        //var res = ValidateCustomer();

        //if (res == false) {
        //    return false;
        //}

        var data = new FormData();


        data.append("delivery_name", $('#deliveryName').val());
        data.append("status", $('#status').val());

        $.ajax({
            url: "/api/DeliveryTypes",
            data: data,
            type: "POST",
            contentType: false,
            processData:false,
            dataType: "json",
            success: function (result) {
                toastr.success("New Delivery has been Created", "Server Respond");
                $('#tblDelivery').DataTable().ajax.reload();
                document.getElementById('btnSaveDelivery').innerText = 'Add New';
               // $('#customerName').val('');
                $("#deliveryTypeModal").modal('hide');
            },
            error: function (errormesage) {
                $('#deliveryName').focus();
                toastr.error("This Name is exist in Database", "Server Respond")
            }

        });        

    } else if (action == "Update") {
        //alert('hi');
        //var res = ValidateCustomer();

        //if (res == false) {
        //    return false;
        //}
        var data = new FormData();

        data.append("id", $('#deliveryTypeID').val());
        data.append("delivery_name", $('#deliveryName').val());
        data.append("status", $('#status').val());
        
                $.ajax({
                    url: "/api/DeliveryTypes/" + $("#deliveryTypeID").val(),
                    data: data,
                    type: "PUT",
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    success: function (result) {
                        toastr.success("Delivery has been Updated", "Server Respond");
                        $('#tblDelivery').DataTable().ajax.reload();
                        $("#deliveryTypeModal").modal('hide');
                    },
                    error: function (errormesage) {
                        toastr.error("Delivery hasn't Updated in Database", "Server Respond")
                    }
                });        
    }
}

function CustomerEdit(id) {
    //alert('hi');


    ClearControl();
    EnableControl();
    action = document.getElementById('btnSaveDelivery').innerText="Update";


    $.ajax({
        url: "/api/DeliveryTypes/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#deliveryTypeID').val(result.id);

            ///console.log(result.id);
            $("#deliveryName").val(result.delivery_name);
            $("#status").val(result.status);         

            $("#deliveryTypeModal").modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });

}

function CustomerDelete(id) {
   // alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/DeliveryTypes/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#tblDelivery').DataTable().ajax.reload();
                        toastr.success("Delivery Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Customerelivery Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}

function DisableControl() {
    document.getElementById('deliveryName').disabled = true;
    document.getElementById('status').disabled = true;

}

function EnableControl() {
    document.getElementById('deliveryName').disabled = false;
    document.getElementById('status').disabled = false;

}

function ClearControl() {
    $('#deliveryName').val('');
}

function AddnewAction() {
    document.getElementById('btnSaveDelivery').innerText = "Save";
    EnableControl();
    ClearControl();
}

function Validate() {
    var isValid = true;
    var formAddEdit = $("#formTrainingProgramAdd");
    if ($('#deliveryName').val().trim() === "") {
        $('#deliveryName').css('border-color', 'red');
        $('#deliveryName').focus();
        toastr.info("Please enter date", "Required");
    }
    else {
        $('#deliveryName').css('border-color', '#cccccc');
//        if ($('#phone').val().trim() === "") {
//            $('#phone').css('border-color', 'red');
//            $('#phone').focus();
//            toastr.info("Please enter your phone", "Required");
//        }
//        else {
//            $('#phone').css('border-color', '#cccccc');
//        }
//    }
//    return isValid;
//}

//  function ValidateCustomer() {
//    var isValid = true;
//    var formAddEdit = $("#formTrainingProgramAdd");
//      if ($('#date').val().trim() == "") {
//          $(' #date').css('border-color', 'red');
//          $(' #date').focus();
//        isValid = false;
//    } else {
//          $(' #date').css('border-color', '#cccccc');
//          $(' #date').focus();
//    }
//      if ($(' #phone').val().trim() == "") {
//          $(' #phone').css('border-color', 'red');
//        isValid = false;
//    } else {
//          $(' #phone').css('border-color', '#cccccc');

//    }
//    if ($(' #currentlocation').val().trim() == "") {
//        $(' #currentlocation').css('border-color', 'red');
//        isValid = false;
//    } else {
//        $(' #currentlocation').css('border-color', '#cccccc');
//    }
//    if ($(' #customerName').val().trim() == "") {
//        $(' #customerName').css('border-color', 'red');
//        isValid = false;
//    } else {
//        $(' #customerName').css('border-color', '#cccccc');
//    }
    //if ($(' #phone').val().trim() == "") {
    //    $(' #phone').css('border-color', 'red');
    //    isValid = false;
    //} else {
    //    $(' #phone').css('border-color', '#cccccc');
    }

    return isValid;
}

function readURLCustomer(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#customerPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}