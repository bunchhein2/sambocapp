﻿function generatePDF() {
    const element = document.getElementById('container_content');
    var opt = {
        margin: 0.5,
        filename: 'សាមបុកអ៊េប.pdf',
        image: { type: 'file', quality: 12000 },
        html2canvas: { scale: 2 },
        jsPDF: { unit: 'in', format: 'A4', orientation: 'portrait' }
    };
    // Choose the element that our invoice is rendered in.
    html2pdf().set(opt).from(element).save();
    document.getElementById("btnSaveContract").disabled = false;
}
$(document).ready(function () {
    //$("#ReportShopModel").on('show.modal.bs', function () {
    //    GetContract();
        
    //    //GetContract();
    //    //oTable = $('#tblContract').DataTable();
    //    //oTable.columns(0).search($('#id').val().trim());
    //    ////hit search on server
    //    //oTable.draw();
    //});
    

});
function GetContract() {
    tableCase = $('#tblContract').DataTable({
        ajax: {
            //url: "/api/ContractShops",
            url: "/api/ContractShops/",
            dataSrc: ""
        },
        columns: [
            {

                data: "shopid",
                render: function (data) {

                    return "" + ("").slice(-4);
                }
            },
            {

                data: "shopName"
            },
            {

                data: "date",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MM-YYYY');
                }
            },
            {

                data: "attachment",
                render: function (data, type, meta, full) {
                    //console.log(data);
                    if (meta.status == 1) {
                        return "<a target='_blank' href='../Images/" + data + "'<span class='glyphicon glyphicon-eye-open btn btn-info btn-xs'></span>ViewContract</a>";
                    } else if (meta.status == 0) {
                        return "No PDF";
                    }
                }
            },
            {

                data: "note"
               
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='Delete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Deletel</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "asc"]],
        "info": true
    });
}


function Reload() {
    document.getElementById('btCreatecontrct').disabled = false;
}