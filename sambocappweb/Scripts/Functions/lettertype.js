﻿$(document).ready(function () {
    $('#letterTypeModal').on('show.bs.modal', function () {
        GetLetterType();
        DisableControlLe();
    });
});

var letterTypeTable = [];
function GetLetterType() {
    letterTypeTable = $('#letterTypetbl').dataTable({
        ajax: {
            url: "/api/LetterTypes",
            dataSrc: ""
        },
        columns:
            [
                {
                    data: "id"
                },
                {
                    data: "letter"
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='LetterTypeEdit (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'>Edit</button>" +
                            "<button OnClick='LetterTypeDelete (" + data + ")' class='btn btn-default btn-xs' >Delete</button>";
                    }
                }
            ],
        destroy: true,
        "order": [0, "desc"],
        "info": false

    });
}
function LetterTypeAction() {
    var action = '';
    action = document.getElementById('btnLetterType').innerText;
    //alert(action);

    if (action == "Add New") {
        document.getElementById('btnLetterType').innerText = 'Save';
        EnableControlLe();
        ClearControlLe();
        $('#letterType').focus();
    }
    else if (action == "Save") {
        if ($('#letterType').val().trim() == "") {
            $('#letterType').css('border-color', 'red');
            $('#letterType').focus();
            toastr.info("Please Input letter type", "Server Respon")
        }
        else {
            $('#letterType').css('border-color', '#cccccc');
            var data = {
                letter: $('#letterType').val(),
                note: $('#letternote').val(),
            };
            $.ajax({
                url: "/api/LetterTypes",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("New Data has been Created", "Server Respond");
                    letterTypeTable.DataTable().ajax.reload();
                    document.getElementById('btnLetterTypes').innerText = 'Add New';
                    ClearControlLe();
                    DisableControlLe();
                },
                error: function (errormesage) {
                    toastr.error("This Name is exist in Database", "Server Respond")
                }
            });
        }
    } else if (action == "Update") {
        if ($('#letterType').val().trim() == "") {
            $('#letterType').css('border-color', 'red');
            $('#letterType').focus();
            toastr.info("Please Input letter type", "Server Respon")
        }
        else {
            $('#letterType').css('border-color', '#cccccc');
            var data = {
                id: $('#ID').val(),
                letter: $('#letterType').val(),
                note: $('#letternote').val(),
            };
            $.ajax({
                url: "/api/LetterTypes/" + data.id,
                data: JSON.stringify(data),
                type: "PUT",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Data has been Updated", "Server Respond");
                    letterTypeTable.DataTable().ajax.reload();
                    document.getElementById('btnLetterType').innerText = 'Add New';
                    ClearControlLe();
                    DisableControlLe();
                },
                error: function (errormesage) {
                    toastr.error("Data hasn't Updated in Database", "Server Respond")
                }
            });
        }
    }
}

//Delete
function LetterTypeDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/LetterTypes/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        letterTypeTable.DataTable().ajax.reload();
                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Data hasn't deleted in Database", "Service Response");
                    }
                });
            }
        }
    });
}

//Edit
function LetterTypeEdit(id) {
    $.ajax({
        url: "/api/LetterTypes/" + id,
        type: "Get",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            $('#letternote').val(result.note);
            $('#letterType').val(result.letter);
            $('#ID').val(result.id);

            document.getElementById('btnLetterType').innerText = 'Update';
            $('#letterType').focus();

        },
        error: function (errormesage) {
            toastr.error("Unexpacted ", "Server Respond")
        }
    });
}
function DisableControlLe() {
    document.getElementById('letterType').disable = true;
    document.getElementById('letternote').disable = true;
}
function EnableControlLe() {
    document.getElementById('letterType').disable = false;
    document.getElementById('letternote').disable = false;
}
function ClearControlLe() {
    $('#letterType').val('');
    $('#letternote').val('');
}
function LetterTypeModalAction() {
    document.getElementById('btnLetterType').innerText = 'Add New';
    DisableControlLe();
    ClearControlLe();
}
