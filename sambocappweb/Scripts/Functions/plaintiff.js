﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });

    onPopulatePlaintiff();

    // show = instant call when modal pop up 
    // shown = method revoke after the modal poped up
    //$('#civilcaseModal').on('show.bs.modal', function () {
    //    // Bind Cases Select Options
    //    onPopulatePlaintiff();
    //});

    $('#plaintiffModal').on('show.bs.modal', function () {
        $('#plaintiffcivilcaseid').val('0');
        GetPlaintiff();
        document.getElementById('btnPlaintiffAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        //$('#plaintiffName').val('');
    });
});

var tableCase = [];

function onPopulatePlaintiff() {
    $.ajax({
        url: "/api/plaintiffs?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zplaintiffid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function GetPlaintiff() {
    $('#PlaintiffTable').DataTable().clear().draw();
    tableCase = $('#PlaintiffTable').DataTable({
        ajax: {
            url: "/api/plaintiffs",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "plaintiffName"
            },
            {
                data: "plaintiffSex"
            },
            {
                data: "plaintiffAge"
            },
            {
                data: "plaintiffAddress"
            },
            {
                data: "plaintiffDob",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "plaintiffIdentityno"
            },
            {
                data: "plaintiffPhone"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button​​ ​type='button' onclick='PlaintiffEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button type='button' onclick='PlaintiffDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function GetPlaintiffByCase(id) {
    $('#plaintiffModal').modal('show');
    document.getElementById('btnPlaintiffAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
    $('#plaintiffcivilcaseid').val(id);
    var tblDefendent = [];
    $('#PlaintiffTable').DataTable().clear().draw();

    tblDefendent = $('#PlaintiffTable').DataTable({
        ajax: {
            url: "/api/plaintiffs/?a=" + id + "&b=2&d=3",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "plaintiffName"
            },
            {
                data: "plaintiffSex"
            },
            {
                data: "plaintiffAge"
            },
            {
                data: "plaintiffAddress"
            },
            {
                data: "plaintiffDob",
                render: function (data) {
                    return moment(new Date(data)).format('DD-MMM-YYYY');
                }
            },
            {
                data: "plaintiffIdentityno"
            },
            {
                data: "plaintiffPhone"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button type='button' onclick='PlaintiffEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button type='button' onclick='PlaintiffDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function PlaintiffEdit(id) {
    $.ajax({
        url: "/api/plaintiffs/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#plaintiffId').val(result.id);
            $('#plaintiffName').val(result.plaintiffName);
            $('#plaintiffSex').val(result.plaintiffSex);
            $('#plaintiffAge').val(result.plaintiffAge);
            $('#plaintiffAddress').val(result.plaintiffAddress);
            $('#plaintiffIdentityno').val(result.plaintiffIdentityno);
            $('#plaintiffPhone').val(result.plaintiffPhone);
            var dr = moment(result.plaintiffDob).format("YYYY-MM-DD");
            $('#plaintiffDob').val(dr);
            $('#plaintiffcivilcaseid').val(result.civilcaseid);
            //console.log(result);
            //alert(result.photo);
            if (result.plaintiffPhoto == "") {
                $('#plaintiffPhoto').attr('src', '../Images/no_image.png');
            } else {
                //alert(result.img);
                $('#plaintiffPhoto').attr('src', '../Images/' + result.plaintiffPhoto);
            }
            if (result.plaintiffWithphoto == "") {
                $('#plaintiffWithphoto').attr('src', '../Images/no_image.png');
            } else {
                //alert(result.img);
                $('#plaintiffWithphoto').attr('src', '../Images/' + result.plaintiffWithphoto);
            }
            //alert('hi');
            document.getElementById('btnPlaintiffAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            //$('#defendentModal').modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function PlaintiffAction() {
    var action = '';
    action = document.getElementById('btnPlaintiffAction').innerText;
    if (action === " បង្កើតថ្មី / Add New") {
        $('#plaintiffDob').val(moment().format('YYYY-MM-DD'));
        document.getElementById('btnPlaintiffAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#plaintiffPhoto').attr('src', '../Images/no_image.png');
        $("#plaintiffName").focus();
        ClearPlaintiff();
    } else if (action === " រក្សាទុក / Save") {

        var data = new FormData();
        var files = $("#plaintifffile").get(0).files;
        if (files.length > 0) {
            data.append("plaintiffphoto", files[0]);
        }
        //alert($("#plaintiffwithAge").val());

        data.append("plaintiffname", $("#plaintiffName").val());
        data.append("plaintiffsex", $("#plaintiffSex").val());
        data.append("plaintiffage", $("#plaintiffAge").val());
        data.append("plaintiffaddress", $("#plaintiffAddress").val());
        data.append("plaintiffdob", $("#plaintiffDob").val());
        data.append("plaintiffidentityno", $("#plaintiffIdentityno").val());
        data.append("plaintiffphone", $("#plaintiffPhone").val());
        data.append("plaintiffcivilcaseid", $("#plaintiffcivilcaseid").val());
        //console.log(data);

        $.ajax({
            type: "POST",
            url: "/api/plaintiffs",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                var caseid = $('#plaintiffcivilcaseid').val();
                if (caseid == 0) {
                    GetPlaintiff();
                    onPopulatePlaintiff();
                } else {
                    GetPlaintiffByCase(caseid);
                }
                //tableCase.ajax.reload();
                //document.getElementById('caseNote').disabled = true;
                document.getElementById('btnPlaintiffAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                ClearPlaintiff();
            },
            error: function (errormessage) {
                toastr.error("Record save error !.", "Server Response");
                //document.getElementById('plaintiffName').disabled = true;
                //document.getElementById('caseNote').disabled = true;
                
            }
        });

        //maritalstatus

    } else if (action === " កែប្រែ / Update") {
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
        var data = new FormData();
        var files = $("#plaintifffile").get(0).files;
        if (files.length > 0) {
            data.append("plaintiffphoto", files[0]);
        }

        data.append("plaintiffname", $("#plaintiffName").val());
        data.append("plaintiffsex", $("#plaintiffSex").val());
        data.append("plaintiffage", $("#plaintiffAge").val());
        data.append("plaintiffaddress", $("#plaintiffAddress").val());
        data.append("plaintiffdob", $("#plaintiffDob").val());
        data.append("plaintiffidentityno", $("#plaintiffIdentityno").val());
        data.append("plaintiffphone", $("#plaintiffPhone").val());
        data.append("plaintiffcivilcaseid", $("#plaintiffcivilcaseid").val());
        //console.log(data);

        $.ajax({
            type: "PUT",
            url: "/api/plaintiffs/" + $('#plaintiffId').val(),
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                var caseid = $('#plaintiffcivilcaseid').val();
                if (caseid == 0) {
                    GetPlaintiff();
                    onPopulatePlaintiff();
                } else {
                    GetPlaintiffByCase(caseid);
                }
                //tableCase.ajax.reload();
                document.getElementById('btnPlaintiffAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                ClearPlaintiff();
                
            },
            error: function (errormessage) {
                toastr.error("This defendent is already exists.", "Server Response");
                document.getElementById('btnPlaintiffAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                
            }
        });

    }
}

function PlaintiffDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/plaintiffs/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                        var caseid = $('#plaintiffcivilcaseid').val();
                        if (caseid == 0) {
                            GetPlaintiff();
                            onPopulatePlaintiff();
                        } else {
                            GetPlaintiffByCase(caseid);
                        }
                        ClearText();
                    },
                    error: function () {
                        toastr.error("This plaintiff is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function readURLS(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#plaintiffPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}



function ClearPlaintiff() {
    $('#plaintiffId').val('');
    $('#plaintiffName').val('');
    //$('#plaintiffSex').val('');
    $('#plaintiffAge').val('');
    $('#plaintiffAddress').val('');
    $('#plaintiffIdentityno').val('');
    $('#plaintiffPhone').val('');
}