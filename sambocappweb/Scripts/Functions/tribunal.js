﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    onPopulateCaseTribunal();
    //$('#civilcaseModal').on('show.bs.modal', function () {
    //    // Bind Cases Select Options
    //    onPopulateCaseTribunal();
    //});

    $('#tribunalModal').on('show.bs.modal', function () {
        GetTribunal();
        document.getElementById('btnTribunalAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#tribunalname').val('');
    });
});

var tableCase = [];

function onPopulateCaseTribunal() {
    $.ajax({
        url: "/api/civilcasetribunals?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zcasetribunalid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function GetTribunal() {
    tableCase = $('#TribunalTable').DataTable({
        ajax: {
            url: "/api/tribunals",
            dataSrc: ""
        },
        columns: [
             {
                 data: "id"
             },
            {
                data: "tribunalname"
            },
            {
                data: "tribunalnote"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='tribunalEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='tribunalDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function TribunalAction() {
    var action = '';
    action = document.getElementById('btnTribunalAction').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#tribunalname').val().trim() === "") {
            $('#tribunalname').css('border-color', 'red');
            $('#tribunalname').focus();
            toastr.info("Please enter Tribunal's name", "Required");
        }
        else {
            $('#tribunalname').css('border-color', '#cccccc');

            var data = {
                tribunalname: $('#tribunalname').val(),
                tribunalnote: $('#tribunalnote').val()
            };
            $.ajax({
                url: "/api/tribunals",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableCase.ajax.reload();
                    onPopulateCaseTribunal();
                    document.getElementById('tribunalname').disabled = true;
                    document.getElementById('tribunalnote').disabled = true;
                    document.getElementById('btnTribunalAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#tribunalname').val('');
                    $('#tribunalnote').val('');
                },
                error: function (errormessage) {
                    toastr.error("This tribunal is already exists.", "Server Response");
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('tribunalname').disabled = false;
        document.getElementById('btnTribunalAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#tribunalname').val('');
        $('#tribunalnote').val('');
        $('#tribunalname').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#tribunalname').css('border-color', '#cccccc');

        var data = {
            Id: $('#tribunalid').val(),
            tribunalname: $('#tribunalname').val(),
            tribunalnote: $('#tribunalnote').val()
        };
        $.ajax({
            url: "/api/tribunals/" + data.Id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateCaseTribunal();
                document.getElementById('tribunalname').disabled = true;
                document.getElementById('tribunalnote').disabled = true;
                document.getElementById('btnTribunalAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#tribunalname').val('');
                $('#tribunalnote').val('');
            },
            error: function (errormessage) {
                toastr.error("This tribunal update error!.", "Server Response");
                
            }
        });
    }
}

function tribunalEdit(id) {

    $('#tribunalname').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/tribunals/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#tribunalid').val(result.id);
            $('#tribunalname').val(result.tribunalname);
            $('#tribunalnote').val(result.tribunalnote);
            document.getElementById('btnTribunalAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('tribunalname').disabled = false;
            document.getElementById('tribunalnote').disabled = false;
            $('#tribunalname').focus();

        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function tribunalDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/tribunals/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        onPopulateCaseTribunal();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This case is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}