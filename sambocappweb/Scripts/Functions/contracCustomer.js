﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });
    onPopulateCustomer();
    //$('#civilcaseModal').on('show.bs.modal', function () {
    //    // Bind Cases Select Options
    //    onPopulateCustomer();
    //});

    $('#ContractCustomerModal').on('show.bs.modal', function () {
      
        GetCustomer();
        DisableControlContractCustomers();
        ClearControlContractCustomers();
    });
});

var tableCase = [];
function onPopulateCustomer() {
    $.ajax({
        url: "/api/customers?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            $("#zcustomerid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

function GetCustomer() {
    tableCase = $('#ContractCustomerTable').DataTable({
        ajax: {
            url: "/api/contractCustomers",
            dataSrc: ""
        },
        columns: [
            {
                data: "id"
            },
            {
                data: "customername"
            },
            {
                data: "customersex"
            },
            {
                data: "customeraddress"
            },
            {
                data: "customeridentityno"
            },
            {
                data: "customerphone"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='CustomerEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='CustomerDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function CustomerEdit(id) {

    $('#customerName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/contractCustomers/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#customerId').val(result.id);
            $('#customerContractName').val(result.customername);
            $('#customerContractSex').val(result.customersex);
            $('#customerContractAge').val(result.customerage);
            $("#customerContractPhone").val(result.customerphone);
            $("#customerContractAddress").val(result.customeraddress);
            var dr = moment(result.customerdob).format("YYYY-MM-DD");
            $("#customerContractDob").val(dr);
            $('#customerContractIdentityno').val(result.customeridentityno);
            $('#customerfile_old').val(result.customerphoto);
            //console.log(result);
            //alert(result.photo);
            if (result.customerphoto == "") {
                $('#ContractCustomerPhoto').attr('src', '../Images/no_image.png');
            } else {
                //alert(result.img);
                $('#ContractCustomerPhoto').attr('src', '../Images/' + result.customerphoto);
            }
            EnableControlContractCustomers();
            document.getElementById('btnSaveContractCustomer').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            //$('#Model').modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });
    return false;
}

function ContractCustomerAction() {
    var action = '';
    action = document.getElementById('btnSaveContractCustomer').innerText;
    if (action === "Add New") {

        document.getElementById('btnSaveContractCustomer').innerText = 'Save';

        //document.getElementById('btnSaveCustomer').innerText = "Save";
        EnableControlContractCustomers();
        $('#customerContractDob').val(moment().format('YYYY-MM-DD'));
        $('#customerContractPhoto').attr('src', '../Images/no_image.png');
        $("#customerContractName").focus();
    } else if (action === "Save") {
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
        var data = new FormData();
        var files = $("#customerfile").get(0).files;
        if (files.length > 0) {
            data.append("customerPhoto", files[0]);
        }

        data.append("customername", $("#customerContractName").val());
        data.append("customersex", $("#customerContractSex").val());
        data.append("customerage", $("#customerContractAge").val());
        data.append("customerdob", $("#customerContractDob").val());
        data.append("customeraddress", $("#customerContractAddress").val());
        data.append("customeridentityno", $("#customerContractIdentityno").val());
        data.append("customerphone", $("#customerContractPhone").val());

        //console.log(files[0]);

        $.ajax({
            type: "POST",
            url: "/api/contractCustomers",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateCustomer();
                document.getElementById('btnSaveContractCustomer').innerText = "Add New";
                DisableControlContractCustomers();
                ClearControlContractCustomers();
            },
            error: function (errormessage) {
                toastr.error("This lawyer is already exists.", "Server Response");
            }
        });

        //maritalstatus

    } else if (action === " កែប្រែ / Update") {
        //var response = Validate();
        //if (response == false) {
        //    return false;
        //}
        var data = new FormData();
        var files = $("#customerfile").get(0).files;
        if (files.length > 0) {
            data.append("customerPhoto", files[0]);
        }
        data.append("customerid", $('#customerId'));
        data.append("customername", $("#customerContractName").val());
        data.append("customersex", $("#customerContractSex").val());
        data.append("customerdob", $("#customerContractDob").val());
        data.append("customerage", $("#customerContractAge").val());
        data.append("customeraddress", $("#customerContractAddress").val());
        data.append("customeridentityno", $("#customerContractIdentityno").val());
        data.append("customerphone", $("#customerContractPhone").val());
        data.append("customerfile_old", $("#customerfile_old").val());

        console.log(data);

        $.ajax({
            type: "PUT",
            url: "/api/contractCustomers/" + $('#customerId').val(),
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateCustomer();
                document.getElementById('customerContractName').disabled = true;
                document.getElementById('btnSaveContractCustomer').innerText = "Add New";
                ClearControlContractCustomers();
                DisableControlContractCustomers();
                //$('#caseNote').val('');
            },
            error: function (errormessage) {
                toastr.error("This Customer is already exists.", "Server Response");
            }
        });

    }
}

function CustomerDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/contractCustomers/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        onPopulateCustomer();
                        toastr.success("Deleted successfully.", "Server Response");
                        ClearControlContractCustomers();
                    },
                    error: function () {
                        toastr.error("This customer is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function DisableControlContractCustomers() {
    document.getElementById('customerContractName').disabled = true;
    document.getElementById('customerContractSex').disabled = true;
    document.getElementById('customerContractAge').disabled = true;
    document.getElementById('customerContractAddress').disabled = true;
    document.getElementById('customerContractIdentityno').disabled = true;
    document.getElementById('customerContractPhone').disabled = true;
    document.getElementById('ContractCustomerPhoto').disabled = true;

}

function EnableControlContractCustomers() {
    document.getElementById('customerContractName').disabled = false;
    document.getElementById('customerContractSex').disabled = false;
    document.getElementById('customerContractAge').disabled = false;
    document.getElementById('customerContractAddress').disabled = false;
    document.getElementById('customerContractIdentityno').disabled = false;
    document.getElementById('customerContractPhone').disabled = false;
    document.getElementById('ContractCustomerPhoto').disabled = false;

}

function ClearControlContractCustomers() {
    $('#customerContractName').val('');
    $('#customerContractSex').val('');
    $('#customerContractDob').val('');
    $('#customerContractAge').val('');
    $('#customerContractAddress').val('');
    $('#customerContractIdentityno').val('');
    $('#customerContractPhone').val('');
    $('#ContractCustomerPhoto').attr('src', '../Images/no_image.png');
}

function ContractCustomersActionModal() {
    document.getElementById('btnSaveContractCustomer').innerText = "Add New";
    DisableControlCustomers();
    ClearControlCustomers();
}


function readURLLContractCustomer(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#ContractCustomerPhoto').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}