﻿$(document).ready(function () {
    $(document).ajaxStart(function () {
        Pace.start();
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        Pace.stop();
        $("div#divLoadingModal").removeClass('show');
    });

    $('#civilcaseModal').on('show.bs.modal', function () {
        // Bind Cases Select Options
        onPopulateCase();
    });

    //GetCases();
    $('#caseModal').on('show.bs.modal', function () {
        GetCases();
        document.getElementById('caseName').disabled = true;
        document.getElementById('caseNote').disabled = true;
        document.getElementById('btnCaseAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
        $('#caseName').val('');
    });
});

function onPopulateCase() {
    $.ajax({
        url: "/api/cases?selectoption=true",
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (data) {
            //console.log(data);
            $("#zcaseid").select2({
                data: data,
                dropdownParent: $("#civilcaseModal")
            });
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
}

var tableCase = [];

function GetCases() {
    tableCase = $('#CaseTable').DataTable({
        ajax: {
            url: "/api/cases",
            dataSrc: ""
        },
        columns: [
            {
                data: "name"
            },
            {
                data: "note"
            },
            {
                data: "id",
                render: function (data) {
                    return "<button onclick='CaseEdit(" + data + ")' class='btn btn-warning btn-xs' style='border-width: 0px; width: 65px; margin-right: 5px;'><span class='glyphicon glyphicon-edit'></span> Edit</button>" + "<button onclick='CaseDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

function CaseAction() {
    var action = '';
    action = document.getElementById('btnCaseAction').innerText;
    if (action === " រក្សាទុក / Save") {
        if ($('#caseName').val().trim() === "") {
            $('#caseName').css('border-color', 'red');
            $('#caseName').focus();
            toastr.info("Please enter Case's name", "Required");
        }
        else {
            $('#caseName').css('border-color', '#cccccc');

            var data = {
                name: $('#caseName').val(),
                note: $('#caseNote').val()
            };
            $.ajax({
                url: "/api/cases",
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (result) {
                    toastr.success("Save to database successfully.", "Server Response");
                    tableCase.ajax.reload();
                    onPopulateCase();
                    document.getElementById('caseName').disabled = true;
                    document.getElementById('caseNote').disabled = true;
                    document.getElementById('btnCaseAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#caseName').val('');
                    $('#caseNote').val('');
                },
                error: function (errormessage) {
                    toastr.error("This case is already exists.", "Server Response");
                    document.getElementById('caseName').disabled = true;
                    document.getElementById('caseNote').disabled = true;
                    document.getElementById('btnCaseAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                    $('#caseName').val('');
                    $('#caseNote').val('');
                }
            });
        }
    }
    else if (action === " បង្កើតថ្មី / Add New") {
        document.getElementById('caseName').disabled = false;
        document.getElementById('caseNote').disabled = false;
        document.getElementById('btnCaseAction').innerHTML = "<span class='glyphicon glyphicon-floppy-save'></span> <span class='kh'>រក្សាទុក</span> / Save";
        $('#caseName').val('');
        $('#caseNote').val('');
        $('#caseName').focus();
    }
    else if (action === " កែប្រែ / Update") {
        $('#caseName').css('border-color', '#cccccc');

        var data = {
            Id: $('#caseId').val(),
            name: $('#caseName').val(),
            note: $('#caseNote').val()
        };
        $.ajax({
            url: "/api/cases/" + data.Id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                tableCase.ajax.reload();
                onPopulateCase();
                document.getElementById('caseName').disabled = true;
                document.getElementById('caseNote').disabled = true;
                document.getElementById('btnCaseAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#caseName').val('');
                $('#caseNote').val('');
            },
            error: function (errormessage) {
                toastr.error("This department is already exists.", "Server Response");
                document.getElementById('caseName').disabled = true;
                document.getElementById('btnCaseAction').innerHTML = "<span class='glyphicon glyphicon-plus-sign'></span> <span class='kh'>បង្កើតថ្មី</span> / Add New";
                $('#caseName').val('');
                $('#caseNote').val('');
            }
        });
    }
}

function CaseEdit(id) {

    $('#caseName').css('border-color', '#cccccc');

    $.ajax({
        url: "/api/cases/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            $('#caseId').val(result.id);
            $('#caseName').val(result.name);
            $('#caseNote').val(result.note);
            document.getElementById('btnCaseAction').innerHTML = "<span class='glyphicon glyphicon-saved'></span> <span class='kh'>កែប្រែ</span> / Update";
            document.getElementById('caseName').disabled = false;
            document.getElementById('caseNote').disabled = false;
            $('#caseName').focus();
            $('#caseNote').focus();

        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function CaseDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/cases/" + id,
                    method: "DELETE",
                    success: function () {
                        tableCase.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                        onPopulateCase();

                    },
                    
                    error: function () {
                        toastr.error("This case is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

//$("#close_case").click(function () {
//    if ($('#civilcaseModal').is(':visible')) {
//        $.ajax({
//            url: "/api/cases",
//            type: "GET",
//            contentType: "application/json;charset=utf-8",
//            dataType: "json",
//            success: function (result) {
//                console.log(result);
//                var office = $("#zcaseid");
//                office.empty();
//                office.append($('<option></option>').attr('value', "").text("---សូមជ្រើសរើស---"));

//                $.each(result, function (key, item) {
//                    // dropDown.append($('<option></option>').attr('selected', (item.Name == selectedName) ? 'selected' : undefined).attr('value', item.Id).text(item.Name));
//                    office.append($('<option></option>').attr('value', item.id).text(item.name));
//                    $('option', this).each(function () {
//                        if ($(this).html() == item.name) {
//                            $(this).attr('selected', 'selected')
//                        };
//                    });
//                });

//            },
//            error: function (errormessage) {
//                alert('Error');
//            }
//        });
//    }
//});