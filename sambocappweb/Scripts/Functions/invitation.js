﻿var tableInvitation = [];

function onGetInvitationTab() {
    tableInvitation = $('#invitationTable').DataTable({
        ajax: {
            url: "/api/invitations",
            dataSrc: ""
        },
        columns: [
            {
                data: function (data) {
                    return "<a href='#' onclick='InvitationEdit(" + data.id + ")' style='text-decoration: none;'>" + data.objective + "</a>";
                }
            },
            {
                data: "id",
                render: function (data) {
                    return "<a href='/administrative-format/invitation=" + data + "' class='btn btn-primary btn-xs' style='border-width: 0px; width: 70px; margin-right: 5px;'><span class='glyphicon glyphicon-search'></span> Display</a>" + "<button onclick='InvitationDelete(" + data + ")' class='btn btn-danger btn-xs' style='border-width: 0px;'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                },
                "width": "130px"
            }
        ],
        destroy: true,
        "order": [[0, "desc"]],
        "info": false
    });
}

$('#btnSaveInvitation').click(function () {
    if (invitationValidation() == false) {
        $('#invitationObjective').css('border-color', 'red');
        $('#invitationObjective').focus();
        return;
    }
    $.ajax({
        url: "/api/invitations?to=" + $('#invitationTo').val(),
        data: JSON.stringify(setInvitationObject()),
        type: "POST",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Save to database successfully.", "Server Response");
            tableInvitation.ajax.reload();
            $('#invitationModal').modal('hide');
        },
        error: function (errormessage) {
            toastr.error("This invitation is already exists.", "Server Response");
        }
    });
})

$('#btnUpdateInvitation').click(function () {
    if (invitationValidationEdit() == false) {
        $('#invitationObjectiveEdit').css('border-color', 'red');
        $('#invitationObjectiveEdit').focus();
        return;
    }
    $.ajax({
        url: "/api/invitations?to=" + $('#invitationToEdit').val(),
        data: JSON.stringify(setInvitationObjectEdit()),
        type: "PUT",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (result) {
            toastr.success("Save to database successfully.", "Server Response");
            tableInvitation.ajax.reload();
            $('#invitationEditModal').modal('hide');
        },
        error: function (errormessage) {
            toastr.error("This invitation is already exists.", "Server Response");
        }
    });
})

function InvitationEdit(id) {
    $.ajax({
        url: "/api/invitations/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            var civilServantTo = [];
            result.map(function (x) {
                civilServantTo.push(x.civilServantToId);
            });
            $('#invitationIdEdit').val(result[0].id);
            $('#invitationFromEdit').val(result[0].civilServantFromId).trigger('change');
            $('#invitationToEdit').val(civilServantTo).trigger('chosen:updated');
            $('#invitationObjectiveEdit').val(result[0].objective);
            $('#invitationContentEdit').val(result[0].content);
            $('#invitationReferenceEdit').val(result[0].reference);
            $('#invitationEditModal').modal('show');
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}

function InvitationDelete(id) {
    bootbox.confirm({
        title: "Confirmation",
        message: "Are you sure you want to delete this?",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default"
            },
            confirm: {
                label: "Delete",
                className: "btn-danger"
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: "/api/invitations/" + id,
                    method: "DELETE",
                    success: function () {
                        tableInvitation.ajax.reload();
                        toastr.success("Deleted successfully.", "Server Response");
                    },
                    error: function () {
                        toastr.error("This invitation is being used, cannot delete this.", "Server Response");
                    }
                });
            }
        }
    });
}

function setInvitationObject() {
    var data = {
        Id: $('#invitationId').val(),
        Objective: $('#invitationObjective').val(),
        Content: $('#invitationContent').val(),
        Reference: $('#invitationReference').val(),
        CivilServantFromId: $('#invitationFrom').val(),
        CivilServantToId: $('#invitationTo').val()
    };
    return data;
}

function setInvitationObjectEdit() {
    var data = {
        Id: $('#invitationIdEdit').val(),
        Objective: $('#invitationObjectiveEdit').val(),
        Content: $('#invitationContentEdit').val(),
        Reference: $('#invitationReferenceEdit').val(),
        CivilServantFromId: $('#invitationFromEdit').val(),
        CivilServantToId: $('#invitationToEdit').val()
    };
    return data;
}

function invitationValidation() {
    var invitationObjective = $('#invitationObjective').val();
    return (invitationObjective == "") ? false : true;
}

function invitationValidationEdit() {
    var invitationObjective = $('#invitationObjectiveEdit').val();
    return (invitationObjective == "") ? false : true;
}

function clearInputInvitation() {
    $('#invitationId').val('');
    $("#invitationFrom").val($("#invitationFrom option:first").val()).trigger('change');
    $('#invitationTo').val('').trigger("chosen:updated");
    $('#invitationObjective').val('');
    $('#invitationContent').val('');
    $('#invitationReference').val('');
    $('#invitationObjective').css('border-color', '#dce4ec');
}

$("#invitationFrom").select2({
    dropdownParent: $('#invitationModal .modal-content'),
    placeholder: "--- Please Select ---",
    allowClear: true
});

$("#invitationFromEdit").select2({
    dropdownParent: $('#invitationEditModal .modal-content'),
    placeholder: "--- Please Select ---",
    allowClear: true
});

$('#invitationTo').chosen(
    { width: "100%", disable_search_threshold: 10 }
);

$('#invitationToEdit').chosen(
    { width: "100%", disable_search_threshold: 10 }
);