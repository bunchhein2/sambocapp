﻿$(document).ready(function () {
    GetSetupFee();
});
var tableSetupFees = [];
function GetSetupFee() {
    tableSetupFees = $('#setupFeeTbl').dataTable({
        ajax: {
            url: "/api/SetupFees",
            dataSrc: ""
        },
        columns:
            [

                {
                    data: "id",
                    render: function (data) {

                        return "" + ("").slice(-4);
                    }
                },
                {
                    data: "date",
                    render: function (data) {
                        return moment(new Date(data)).format('DD-MMM-YYYY');
                    }
                },
                {
                    data: "feetype",
                },
                {
                    data: "amount",
                },
                {
                    data: "id",
                    render: function (data) {
                        return "<button OnClick='SetupFeeEdit (" + data + ")' class='btn btn-primary btn-xs' style='margin-right:5px'><span class='glyphicon glyphicon-edit'></span> Edit</button>" +
                            "<button OnClick='SetupFeeDelete (" + data + ")' class='btn btn-danger btn-xs' ><span class='glyphicon glyphicon-remove'></span> Delete</button>";
                    }
                    , "width": "130px"
                }
            ],
        destroy: true,
        "info": true

    });
}

function SetupFeeAction() {
    var action = '';
    action = document.getElementById('btnsetupfee').innerText;
    //DisableControl();
    if (action == "Add New") {
        document.getElementById('btnsetupfee').innerText = 'Save';
        EnableControlFe();
        ClearControlFe();
        $('#feetype').focus();
        $('#dates').val(moment().format('YYYY-MM-DD'));
        $('#amount').val('0.00');
    }

    if (action === "Save") {
        //Validate();
        var data = {
            feetype: $('#feetype').val(),
            amount: $('#amounts').val(),
            date: $('#dates').val(),
        };
        $.ajax({
            url: "/api/SetupFees",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Save to database successfully.", "Server Response");
                $('#setupFeeTbl').DataTable().ajax.reload();
                ClearControlFe();
                //DisableControlFe();
                $("#SetupFeeModal").modal('hide');
                //document.getElementById('btnsetupfee').innerText = 'Add New';
            },
            error: function (errormessage) {
                toastr.error("This Data is already exists.", "Server Response");
            }
        });
    }
    else if (action === "Update") {
        var data = {
            id: $('#ID').val(),
            feetype: $('#feetype').val(),
            amount: $('#amounts').val(),
            date: $('#dates').val(),
        };
        $.ajax({
            url: "/api/SetupFees/" + data.id,
            data: JSON.stringify(data),
            type: "PUT",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (result) {
                toastr.success("Updated successfully.", "Server Response");
                $('#setupFeeTbl').DataTable().ajax.reload();
                ClearControlFe();
                //DisableControlFe();
                $("#SetupFeeModal").modal('hide');
                //document.getElementById('btnsetupfee').innerText = 'Add New';

            },
            error: function (errormessage) {
                toastr.error("This Data can't Update.", "Server Response");

            }
        });
    }
}

$("#amounts").click(function () {
    $(this).select();
});
function SetupFeeEdit(id) {
    //ClearControlSh();
    $("#SetupFeeModal").modal('show');
    EnableControlFe();
    action = document.getElementById('btnsetupfee').innerText = "Update";

    $.ajax({
        url: "/api/SetupFees/" + id,
        type: "GET",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {

            $('#ID').val(result.id);
            $('#feetype').val(result.feetype);
            $('#amounts').val(result.amount);
            var dr = moment(result.date).format("YYYY-MM-DD");
            $('#dates').val(dr);
        },
        error: function (errormessage) {
            toastr.error("Something unexpected happen.", "Server Response");
        }
    });
    return false;
}


function SetupFeeDelete(id) {
    //alert('hi');
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/SetupFees/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        $('#setupFeeTbl').DataTable().ajax.reload();

                        toastr.success("Data Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("Data Can't be Deleted", "Service Response");
                    }
                });
            }
        }
    });
}

function DisableControlFe() {
    document.getElementById('dates').disabled = true;
    document.getElementById('feetype').disabled = true;
    document.getElementById('amounts').disabled = true;
}

function EnableControlFe() {
    document.getElementById('dates').disabled = false;
    document.getElementById('feetype').disabled = false;
    document.getElementById('amounts').disabled = false;
}

function ClearControlFe() {
    $('#feetype').val('');
    $('#amounts').val('0.00');
}

function AddNewFeeTypeAction() {
    document.getElementById('btnsetupfee').innerText = "Save";
    EnableControlFe();
    ClearControlFe();
    $('#feetype').focus();
    $('#dates').val(moment().format('YYYY-MM-DD'));
    $('#amount').val('0.00');
}

