﻿
$(document).ready(function () {

    $('#myModalSave').on('shown.bs.modal', function () {
        $('#txtSubject').focus();
    })

    $(document).ajaxStart(function () {
        $("div#divLoadingModal").addClass('show');
    }).ajaxStop(function () {
        $("div#divLoadingModal").removeClass('show');
    });

    var events = [];
    var selectedEvent = null;
    FetchEventAndRenderCalendar();

    function FetchEventAndRenderCalendar() {
        events = [];
        $.ajax({
            type: "GET",
            url: "/api/calendars",
            success: function (data) {
                $.each(data, function (i, v) {
                    events.push({
                        id: v.job.id,
                        title: v.job.subject,
                        desc: v.job.description != null ? v.job.description : 'គ្មាន',
                        start: v.job.startDate,
                        end: v.job.endDate != null ? v.job.endDate : null,
                        color: v.job.themeColor,
                        allDay: v.job.isFullDay,
                        isMeeting: v.job.isMeeting,
                        isTravelling: v.job.isTravelling,
                        departmentId: v.job.departmentId,
                        createdOn: v.job.createdOn,
                        createdBy: v.job.createdBy
                    })
                })
                generateCalendar(events);
            },
            error: function (error) {
                alert('failed');
            }
        })
    }

    var date = new Date;
    var hourStart = date.getHours();
    var hourEnd = date.getHours() - 6;

    function generateCalendar(events) {
        $('#calendar').fullCalendar('destroy');
        $('#calendar').fullCalendar({
            contentHeight: 550,
            themeSystem: 'bootstrap3',
            displayEventTime: true,
            displayEventEnd: false,
            displayEventStart: true,
            nowIndicator: true,
            weekNumbers: true,
            navLinks: true,
            editable: true,
            nextDayThreshold: '00:00',
            defaultDate: new Date(),
            timeFormat: 'h(:mm)a',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay,agenda,listMonth'
            },
            businessHours: {
                // days of week. an array of zero-based day of week integers (0=Sunday)
                daysOfWeek: [1, 2, 3, 4, 5], // Monday - Thursday

                startTime: '08:00', // a start time (10am in this example)
                endTime: '17:00', // an end time (6pm in this example)
            },
            eventLimit: true,
            eventColor: '#378006',
            eventAfterRender: function (events, element) {
                var content = '@sambocappweb.Resources.Language.Subject: <b>' + events.title + '</b><br/>' + '@sambocappweb.Resources.Language.From: <b>' + events.start.format("DD-MMM-YYYY hh:mm a") + '</b> - @sambocappweb.Resources.Language.To: <b>' + events.end.format("DD-MMM-YYYY hh:mm a") + '</b><br>@sambocappweb.Resources.Language.Description' + ': ' + events.desc + '<br>@sambocappweb.Resources.Language.Date: <b>' + moment(events.createdOn).format("DD-MMMM-YYYY hh:mm A") + '</b><br>@sambocappweb.Resources.Language.CreatedBy: <b>' + events.createdBy + '</b>';
                $(element).tooltip({
                    title: content,
                    container: "body",
                    html: true
                });
            },
            events: events,
            eventClick: function (calEvent, jsEvent, view) {
                selectedEvent = calEvent;
                $('#myModal #eventTitle').text(calEvent.title);
                var $description = $('<div/>');
                var $duration = $('<p/>');
                var $createdBy = $('<p/>');
                $duration.append('<b>' + '@sambocappweb.Resources.Language.From' + ': </b>' + calEvent.start.format("DD-MMM-YYYY hh:mm a"));
                if (calEvent.end != null) {
                    $duration.append('<b> - ' + '@sambocappweb.Resources.Language.To' + ': </b>' + calEvent.end.format("DD-MMM-YYYY hh:mm a"));
                }
                $description.append($('<p/>').html('<b>' + '@sambocappweb.Resources.Language.Description' + ': </b><br/>' + calEvent.desc + ""));

                $createdBy.append($('<p/>').html('<b>' + '@sambocappweb.Resources.Language.CreatedOn' + ': </b>' + moment(calEvent.createdOn).format("DD-MMMM-YYYY hh:mm A") + "<br/>" + '<b>' + '@sambocappweb.Resources.Language.CreatedBy' + ': </b>' + calEvent.createdBy));

                $('#myModal #pDetails').empty().html($description);
                $('#myModal #pDuration').empty().html($duration);
                $('#myModal #pCreatedBy').empty().html($createdBy);

                $('#myModal').modal();
            },
            selectable: true,
            select: function (start, end) {
                selectedEvent = {
                    id: 0,
                    title: '',
                    desc: '',
                    start: start.set({ hours: hourStart }),
                    end: end.set({ hours: hourEnd }),
                    isMeeting: '',
                    isTravelling: '',
                    color: '',
                    allDay: false,
                    departmentId: ''
                };

                openAddEditForm();
                $('#calendar').fullCalendar('unselect');
            },
            editable: true,
            eventDrop: function (event) {
                var data = {
                    Id: event.id,
                    Subject: event.title,
                    StartDate: event.start.format('MM/DD/YYYY HH:mm A'),
                    EndDate: event.end != null ? event.end.format('MM/DD/YYYY HH:mm A') : null,
                    Description: event.desc,
                    ThemeColor: event.color,
                    IsFullDay: event.allDay,
                    IsTravelling: event.isTravelling,
                    IsMeeting: event.isMeeting,
                    DepartmentId: event.departmentId
                }
                SaveEvent(data);
            }
        })
    }

    $('#btnEdit').click(function () {
        openAddEditForm();
    })

    $('#btnSave').click(function () {
        if ($('#txtSubject').val().trim() == "") {
            toastr.info('Subject is required.', "Server Response");
            $('#txtSubject').focus();
            return;
        }
        if ($('#txtStartDate').val().trim() == "") {
            toastr.info('Start date is required.', "Server Response");
            $('#txtStartDate').focus();
            return;
        }
        if ($('#txtDepartment').val() == null) {
            toastr.info('Department is required.', "Server Response");
            $('#txtDepartment').focus();
            return;
        }
        else {
            var startDate = moment($('#txtStartDate').val(), "MM/DD/YYYY HH:mm A").toDate();
            var endDate = moment($('#txtEndDate').val(), "MM/DD/YYYY HH:mm A").toDate();
            if (startDate > endDate) {
                alert('Invalid end date.');
                return;
            }
        }

        var data = {
            Id: $('#txtId').val(),
            Subject: $('#txtSubject').val(),
            StartDate: $('#txtStartDate').val(),
            EndDate: $('#txtEndDate').val(),
            Description: $('#txtDescription').val(),
            ThemeColor: $('#cboThemeColor').val(),
            IsFullDay: $('#chkIsFullDay').is(':checked'),
            IsMeeting: $('#chkIsMeeting').is(':checked'),
            IsTravelling: $('#chkIsTravelling').is(':checked'),
            DepartmentId: $('#txtDepartment').val()
        }
        SaveEvent(data);
    })

    function SaveEvent(data) {
        $.ajax({
            type: "POST",
            url: "/api/calendars",
            data: data,
            success: function (result) {
                if (data.Id > 0 || data.IsMeeting == false) {
                    toastr.success("Save successfully.", "Success");
                    FetchEventAndRenderCalendar();
                    $('#myModalSave').modal('hide');
                }
                else {
                    var message = "Meeting Room is Booked" + "%0A" + "-------------------------" + "%0A" + "Subject: " + encodeURIComponent(result.subject) + "%0A" + "From: " + moment(result.startDate).format("DD-MMM-YYYY hh:mm a") + "%0A" + "To: " + moment(result.endDate).format("DD-MMM-YYYY hh:mm a") + "%0A" + "Booked by: " + result.createdBy;
                    $.ajax({
                        url: "https://api.telegram.org/bot852562361:AAHYbvP_wGtVgsK04-QQg4k3Gf-fTl6D16Y/sendMessage?chat_id=-371042985&text=" + message,
                        type: "GET",
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        success: function (result) {
                            toastr.success("Successfully sent to Telegram", "Success");
                            FetchEventAndRenderCalendar();
                            $('#myModalSave').modal('hide');
                        },
                        erorr: function (result) {
                            toastr.error('Cannot send to telegram.');
                        }
                    });
                }
            },
            error: function () {
                FetchEventAndRenderCalendar();
                toastr.error("You do not have permission to update others schedule.", "Server Response");
            }
        })
    }

    $('#btnDelete').click(function () {
        bootbox.confirm({
            title: "Confirmation",
            message: "Are you sure you want to delete this?",
            buttons: {
                cancel: {
                    label: "Cancel",
                    className: "btn-default btn-sm"
                },
                confirm: {
                    label: "Delete",
                    className: "btn-danger btn-sm"
                }
            },
            callback: function (result) {
                if (result) {
                    $.ajax({
                        type: "DELETE",
                        url: "/api/calendars/" + selectedEvent.id,
                        success: function () {
                            FetchEventAndRenderCalendar();
                            $('#myModal').modal('hide');
                        },
                        error: function () {
                            alert('You do not have permission to delete others schedule.');
                        }
                    })
                }
            }
        });
    })

    $('#dtpStartDate, #dtpEndDate').datetimepicker({
        format: 'MM/DD/YYYY HH:mm A'
    });

    function openAddEditForm() {

        if (selectedEvent != null) {
            $('#txtId').val(selectedEvent.id);
            $('#txtSubject').val(selectedEvent.title);
            $('#txtStartDate').val(selectedEvent.start.format('MM/DD/YYYY HH:mm A'));
            $('#chkIsMeeting').prop("checked", selectedEvent.isMeeting || false);
            $('#chkIsMeeting').change();
            $('#chkIsTravelling').prop("checked", selectedEvent.isTravelling || false);
            $('#chkIsTravelling').change();
            $('#chkIsFullDay').prop("checked", selectedEvent.isMeeting || false);
            $('#chkIsFullDay').change();
            $('#txtDescription').val(selectedEvent.desc);
            $('#cboThemeColor').val(selectedEvent.color);
            if (selectedEvent.departmentId == "") {
                $("#txtDepartment").val($("#txtDepartment option:first").val());
            }
            else {
                $('#txtDepartment').val(selectedEvent.departmentId)
            }

            if (selectedEvent.id > 0) {
                $('#txtEndDate').val(selectedEvent.end != null ? selectedEvent.end.format('MM/DD/YYYY HH:mm A') : '');
            } else {
                var newEndDate = new Date(selectedEvent.end);
                newEndDate.setDate(newEndDate.getDate() - 1);
                $('#txtEndDate').val(selectedEvent.end != null ? moment(newEndDate).format('MM/DD/YYYY HH:mm A') : '');
            }
        }
        $('#myModal').modal('hide');
        $('#myModalSave').modal();
    }
})