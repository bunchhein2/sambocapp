﻿$(document).ready(function () {

    $(document).ajaxStart(function () {
        $('#loadingGif').addClass('show');
    }).ajaxStop(function () {
        $('#loadingGif').removeClass('show');
    });

    GetIncome();
    //$('#displayshowroom').on('change', function () {
    //    var departmentid = this.value;
    //    if (departmentid == "---Select Showroom----") {
    //        GetOtherExpense("all");
    //    } else {
    //        //alert(departmentid);
    //        GetOtherExpense(departmentid);
    //    }
    //})
})

var tableEmployee = [];
toastr.optionsOverride = 'positionclass = "toast-bottom-right"';
toastr.options.positionClass = 'toast-bottom-right';

function GetIncome() {
    tableEmployee = $('#tableIncome').DataTable({
        ajax: {
            //url: (departmentId == "all") ? "/api/Income?showroomid=all" : "/api/Income?showroomid=" + departmentId,
            url: "/api/Income",
            dataSrc: ""
        },
        columns: [
                {
                    data: "id"
                },
                {
                    data: "date",
                    render: function (data) {
                        return moment(new Date(data)).format('DD-MMM-YYYY');
                    }
                },
                //{
                //     data: "incometypeid"
                //},
                {
                    data: "amount"
                },
                {
                    data: "paymentmethodname"
                },
                {
                    data: "note"
                },

                {
                    data: "incometypename"
                },

                {
                    data: "subincometypename"
                },
                //{
                //    data: "createdate"
                //},

            {
                data: "id",
                render: function (data) {
                    return "<button onclick='PrintReceipts(" + data + ")' class='btn btn-success btn-xs' style='margin-right:5px;'​>Print</button>" + "<button onclick='IncomeEdit(" + data + ")' class='btn btn-warning btn-xs' style='margin-right:5px;'​>Edit</button>" + "<button onclick='IncomeDelete(" + data + ")' class='btn btn-danger btn-xs' >Delete</button>";
                }
            },
        ],
        destroy: true,
        "order": [0, "desc"],
        "info": false
    });
}

function IncomeAction() {
    var action = '';
    action = document.getElementById('btnIncome').innerText;
    if (action == "Add New") {
        $('#date').val(moment().format('YYYY-MM-DD'));
        document.getElementById('btnIncome').innerText = "Save";
        document.getElementById('date').disabled = false;
        document.getElementById('incometypeids').disabled = false;
        document.getElementById('subincometypeids').disabled = false;
        document.getElementById('paymentmethodid').disabled = false;
        document.getElementById('amount').disabled = false;
        document.getElementById('note').disabled = false;
        //document.getElementById('showroomid').disabled = false;
        $("#incometypeids").focus();
        $("#amount").val(0);
    } else if (action == "Save") {
        var response = Validate();
        if (response == false) {
            return false;
        }
        //alert($("#subincometypeids").val());
        var data = new FormData();
        data.append("date", $("#date").val());
        data.append("incometypeid", $("#incometypeids").val());
        data.append("subincometypeids", $("#subincometypeids").val());
        data.append("paymentmethodid", $("#paymentmethodid").val());
        data.append("amount", $("#amount").val());
        data.append("note", $("#note").val());
        //data.append("showroomid", $("#showroomid").val());
        //data.append("showroomid", 1);
        console.log(data);

        $.ajax({
            type: "POST",
            url: "/api/Income",
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {

                //console.log(result);

                toastr.success("Income has been created successfully.", "Server Response");
                tableEmployee.ajax.reload();

                $('#id').val(result.id);
                $('#incomeModel').modal('hide');
                document.getElementById('btnIncome').innerText = "Add New";
                $('#date').val('');
                $('#incometypeids').val('');
                $('#subincometypeids').val('');
                $('#amount').val('0.00');
                $('#note').val('');

            },
            error: function (error) {
                //console.log(error);
                toastr.error("Income Already Exists!.", "Server Response");
            }
        });

        //maritalstatus

    } else if (action == "Update") {
        var response = Validate();
        if (response == false) {
            return false;
        }
        var data = new FormData();
        data.append("id", $('#id'));
        data.append("date", $("#date").val());
        data.append("incometypeid", $("#incometypeids").val());
        data.append("subincometypeids", $("#subincometypeids").val());
        data.append("paymentmethodid", $("#paymentmethodid").val());
        data.append("amount", $("#amount").val());
        data.append("note", $("#note").val());
        //data.append("showroomid", $("#showroomid").val());
        //data.append("showroomid", 1);
        console.log(data);

        $.ajax({
            type: "PUT",
            url: "/api/Income/" + $('#id').val(),
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {

                //console.log(result);

                toastr.success("Income has been updated successfully.", "Server Response");
                tableEmployee.ajax.reload();

                //$('#employeeId').val(result.id);
                $('#incomeModel').modal('hide');

                document.getElementById('btnIncome').innerText = "Add New";
                $('#date').val('');
                $('#incometypeids').val('');
                $('#subincometypeids').val('');
                $('#amount').val('0.00');
                $('#note').val('');

            },
            error: function (error) {
                //console.log(error);
                toastr.error("Income Already Exists!.", "Server Response");
            }
        });

    }
}

function IncomeEdit(id) {
    document.getElementById('btnIncome').innerText = "Update";

    $.ajax({
        url: "/api/Income/" + id,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (result) {
            $('#id').val(result.id);
            var pd = moment(result.date).format("YYYY-MM-DD");
            $('#date').val(pd);
            $('#incometypeids').val(result.incometypeid).change();
            $('#subincometypeids').val(result.subincometypeids).change();
            $('#paymentmethodid').val(result.paymentmethodid).change();
            $("#amount").val(result.amount);
            //$("#showroomid").val(result.showroomid);
            //$("#showroomid").val(1);
            $('#note').val(result.note);
            //console.log(result);


            //Enable Control
            document.getElementById('date').disabled = false;
            document.getElementById('incometypeid').disabled = false;
            document.getElementById('subincometypeids').disabled = false;
            document.getElementById('paymentmethodid').disabled = false;
            document.getElementById('amount').disabled = false;
            document.getElementById('note').disabled = false;
            //document.getElementById('showroomid').disabled = false;
            $('#incomeModel').modal('show');
        },
        error: function (errormessage) {
            toastr.error("No Record Select!", "Service Response");
        }
    });

}

function IncomeDelete(id) {
    bootbox.confirm({
        title: "",
        message: "Are you sure want to delete this?",
        button: {
            cancel: {
                label: "Cancel",
                ClassName: "btn-default",
            },
            confirm: {
                label: "Delete",
                ClassName: "btn-danger"
            }
        },
        callback: function (result) {
            //alert(id);
            if (result) {
                $.ajax({
                    url: "/api/Income/" + id,
                    type: "DELETE",
                    contentType: "application/json;charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        tableEmployee.ajax.reload();
                        toastr.success("Income has been Deleted successfully!", "Service Response");
                    },
                    error: function (errormessage) {
                        toastr.error("This Income is already exists in Database", "Service Response");
                    }
                });
            }
        }
    });
}

function Validate() {
    var isValid = true;
    if ($('#amount').val().trim() == "") {
        $('#amount').css('border-color', 'red');
        $('#amount').focus();
        isValid = false;
    } else {
        $('#amount').css('border-color', '#cccccc');
        $('#amount').focus();
    }
    return isValid;
}

function ClickAddnewIncome() {
    document.getElementById('date').disabled = true;
    document.getElementById('incometypeids').disabled = true;
    document.getElementById('subincometypeids').disabled = true;
    document.getElementById('paymentmethodid').disabled = true;
    document.getElementById('amount').disabled = true;
    document.getElementById('note').disabled = true;
    //document.getElementById('showroomid').disabled = true;

    $('#amount').val('');
    $('#note').val('');
    $('#date').val('');
    $('#amount').focus();
    document.getElementById('btnIncome').innerText = "Add New";
}

function PrintReceipts(id) {
    window.open("/invoice-income/" + id, "_blank")
}


$('#incometypeids').on('change', function () {
    //var dropDownVal = $("#subexpensetypeids").val();
    //var exid = this.value;
    //alert(exid);
    //$.ajax({
    //    url: "/api/SubExpenseType/" + exid,
    //    type: "GET",
    //    contentType: "application/json;charset=utf-8",
    //    datatype: "json",
    //    success: function (result) {
    //        $('#id').val(result.id);
    //    },
    //    error: function (errormessage) {
    //        toastr.error("No Record Select!", "Service Response");
    //    }
    //});
    var id = this.value;
    $.getJSON("/api/Income/?id=" + id + "&x=99", function (json) {
        $('#subincometypeids').empty();
        //$('#subexpensetypeids').append($('<option>').text("Select"));
        $.each(json, function (i, obj) {
            $('#subincometypeids').append($('<option>').text(obj.subincometypename).attr('value', obj.id));
        });
    });
})