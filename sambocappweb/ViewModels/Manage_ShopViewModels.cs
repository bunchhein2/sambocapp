﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.ViewModels
{
    public class Manage_ShopViewModels
    {
        public IEnumerable<Shop> shopes { get; set; }
        public IEnumerable<SetupFee> SetupFees { get; set; }

    }
}