﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.ViewModels
{
    public class IncomeViewModel
    {
        public IEnumerable<Income> Incomes { get; set; }
        public IEnumerable<IncomeType> IncomeTypes { get; set; }
        public IEnumerable<SubIncomeType> SubIncomeTypes { get; set; }
        public IEnumerable<PaymentMethod> Paymentmethods { get; set; }

    }
}