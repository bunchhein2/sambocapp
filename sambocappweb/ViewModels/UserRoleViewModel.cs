﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.ViewModels
{
    public class UserRoleViewModel
    {
        public ApplicationUser User { get; set; }
        public RoleManager<IdentityRole> Role { get; set; }
    }

    public class Roles
    {
        public string RoleId { get; set; }

        public string RoleName { get; set; }
    }
}