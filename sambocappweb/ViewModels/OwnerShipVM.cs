﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.ViewModels
{
    public class OwnerShipVM
    {
        public IEnumerable<LetterType> LetterTypes { get; set; }
        public IEnumerable<LandType> LandType { get; set; }
        public IEnumerable<Property> Property { get; set; }
    }
}