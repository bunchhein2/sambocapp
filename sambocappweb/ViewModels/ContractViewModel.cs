﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.ViewModels
{
    public class ContractViewModel
    {
		public IEnumerable<CivilCase> Civilcases { get; set; }
		public IEnumerable<Buyer> buyers { get; set; }
        public IEnumerable<Seller> sellers { get; set; }
        public IEnumerable<Depute> deputes { get; set; }
        public IEnumerable<Lawyer> lawyer { get; set; }
		public IEnumerable<Office> offices { get; set; }
		public IEnumerable<Lawrelate> lawrelates { get; set; }
		public IEnumerable<Customer> customers { get; set; }
		public IEnumerable<ContractPosition> contractPositions { get; set; }
		public IEnumerable<Dispute> disputes { get; set; }
		public IEnumerable<Contract> contracts { get; set; }
		public IEnumerable<CivilcaseProceeding> civilcaseProceedings { get; set; }
		public IEnumerable<Lawyer> Lawyers { get; set; }
		public IEnumerable<CivilcaseproccedingType> CaseproccedingTypes { get; set; }
		public IEnumerable<PaymentMethod> PaymentMethods { get; set; }
		public IEnumerable<Contract> Contracts { get; set; }


		public int id { get; set; }
		public DateTime? date { get; set; }
		public int officeid { get; set; }
		public string contractid { get; set; }
		public int lawyerlateid { get; set; }
		public int disputeid { get; set; }
		public int sellerid { get; set; }
		public int buyerid { get; set; }
		public int lawyerid { get; set; }
		public int contractposition { get; set; }
		public int customerid { get; set; }
		public string charges { get; set; }
		public decimal service { get; set; }
		public string contractstatus { get; set; }
		public string paidstatus { get; set; }
		public string contractaddress { get; set; }
		public string datechankate { get; set; }
		public string datesorikate { get; set; }
		public string createby { get; set; }
		public DateTime? createdate { get; set; }
		public string updateby { get; set; }
		public DateTime? updatedate { get; set; }
		public bool status { get; set; }
	}
}
 