﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sambocappweb.Models;

namespace sambocappweb.ViewModels
{
    public class CloseBalanceViewModel
    {
        public IEnumerable<Civilcasepayment> Civilcasepayments { get; set; }
        public IEnumerable<ContractPayment> ContractPayments { get; set; }

        public IEnumerable<OtherExpense> OtherExpenses { get; set; }

    }
}