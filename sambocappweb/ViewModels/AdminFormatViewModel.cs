﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sambocappweb.Models;

namespace sambocappweb.ViewModels
{
    public class AdminFormatViewModel
    {
        public IEnumerable<Rank> Ranks { get; set; }

       // public IEnumerable<CivilServant> CivilServants { get; set; }

        public IEnumerable<ReportType> ReportTypes { get; set; }
    }
}