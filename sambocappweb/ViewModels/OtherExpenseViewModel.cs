﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.ViewModels
{
    public class OtherExpenseViewModel
    {
        public IEnumerable<ExpenseType> ExpenseTypes { get; set; }
        public IEnumerable<SubExpenseType> SubExpenseTypes { get; set; }
        public IEnumerable<PaymentMethod> Paymentmethods { get; set; }

    }
}