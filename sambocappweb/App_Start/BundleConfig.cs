﻿using System.Web;
using System.Web.Optimization;

namespace sambocappweb
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/lib").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/bootbox.js",
                        "~/scripts/datatables/jquery.datatables.js",
                        "~/scripts/datatables/datatables.bootstrap.js",
                        "~/Scripts/toastr.js",
                        "~/Scripts/moment.js",
                        "~/Scripts/jquery-ui.js",
                        "~/Scripts/fullcalendar.min.js",
                        "~/Scripts/bootstrap-datetimepicker.min.js",
                        "~/Scripts/charts-loader.js",
                        "~/Scripts/select2.min.js",
                        "~/Scripts/chosen.jquery.min.js",
                        "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap-flatly.css",
                      "~/content/datatables/css/datatables.bootstrap.css",
                      "~/content/toastr.css",
                      "~/Content/fullcalendar.min.css",
                      "~/Content/bootstrap-datetimepicker.min.css",
                      "~/Content/select2.min.css",
                      "~/fonts/font-battambang.css",
                      "~/fonts/font-roboto.css",
                      "~/Content/chosen.min.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/site.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
