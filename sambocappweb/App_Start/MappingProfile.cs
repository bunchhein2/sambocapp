﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.App_Start
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            //Domain to Dto   
            Mapper.CreateMap<SetupFee, SetupFeeDto>();
            Mapper.CreateMap<Qrcode, QrcodeDto>();
            Mapper.CreateMap<AdvertisePayment, AdvertisePaymentDto>();
            Mapper.CreateMap<ShopPayment, ShopPaymentDto>();
            Mapper.CreateMap<DeliveryType, DeliveryTypeDto>();
            Mapper.CreateMap<Shop, ShopDto>();
            Mapper.CreateMap<Department, DepartmentDto>();
            Mapper.CreateMap<BlogCategory, BlogCategoryDto>();
            Mapper.CreateMap<BlogPost, BlogPostDto>();
            Mapper.CreateMap<Feedback, FeedbackDto>();
            Mapper.CreateMap<Rank, RankDto>();            
            Mapper.CreateMap<ReportType, ReportTypeDto>();
            Mapper.CreateMap<Report, ReportDto>();     
            Mapper.CreateMap<Customer, CustomerDto>();
            Mapper.CreateMap<ProductImage, ProductImageDto>();
            Mapper.CreateMap<PaymentMethod, PaymentMethodDto>();
            Mapper.CreateMap<Location, LocationDto>();
            Mapper.CreateMap<Contract, ContractDto>();



            //Dto to Domain
            Mapper.CreateMap<SetupFeeDto, SetupFee>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<QrcodeDto, Qrcode>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<AdvertisePaymentDto, AdvertisePayment>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<DepartmentDto, Department>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<BlogCategoryDto, BlogCategory>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<BlogPostDto, BlogPost>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<FeedbackDto, Feedback>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<RankDto, Rank>().ForMember(c => c.Id, opt => opt.Ignore());            
            Mapper.CreateMap<ReportTypeDto, ReportType>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<ReportDto, Report>().ForMember(c => c.Id, opt => opt.Ignore());     
            Mapper.CreateMap<CustomerDto, Customer>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<ShopDto, Shop>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<DeliveryTypeDto, DeliveryType>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<Product, ProductDto>();
            Mapper.CreateMap<ProductDto, Product>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<Order, OrderDto>();
            Mapper.CreateMap<OrderDto, Order>().ForMember(c => c.Id, opt => opt.Ignore());
            Mapper.CreateMap<Exchange, ExchangeDto>();
            Mapper.CreateMap<ExchangeDto, Exchange>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<Currency, CurrencyDto>();
            Mapper.CreateMap<CurrencyDto, Currency>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<Privacy, PrivacyDto>();
            Mapper.CreateMap<PrivacyDto, Privacy>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<Orderdetail, OrderdetailDto>();
            Mapper.CreateMap<OrderdetailDto, Orderdetail>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<Banner, bannerDto>();
            Mapper.CreateMap<bannerDto, Banner>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<ProductImageDto, ProductImage>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<PaymentMethodDto, PaymentMethod>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<LocationDto, Location>().ForMember(c => c.id, opt => opt.Ignore());
            Mapper.CreateMap<ContractDto, Contract>().ForMember(c => c.contractid, opt => opt.Ignore());
            Mapper.CreateMap<ShopPaymentDto, ShopPayment>().ForMember(c => c.id, opt => opt.Ignore());


        }
    }
}