﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("landtype_tbl")]
    public class LandType
    {
        public int id { get; set; }
        public string land { get; set; }
        public string note { get; set; }

    }
}