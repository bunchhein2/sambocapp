﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("shipproccess")]
    public class ShipProccess
    {
        public int id { get; set; }
        public string type { get; set; }
        public DateTime? date{get;set;}
        public DateTime? datein { get; set; }
        public DateTime? dateout { get; set; }
        public string note { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
        public int ownershipid { get; set; }
    }
}