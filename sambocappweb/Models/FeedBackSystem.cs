﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("tbl_feedBackSystem")]
    public class FeedBackSystem
    {
        public int id { get; set; }
        public DateTime feedbackDate { get; set; }
        public int memberId { get; set; }
        public string note { get; set; }
    }
}