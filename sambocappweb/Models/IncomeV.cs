﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{

    [Table("income_tbl")]
    public class IncomeV
    {
        [Key]
        public int id { get; set; }
        public DateTime date { get; set; }
        [Required]
        public int expensetypeid { get; set; }
        public ExpenseType ExpenseTypes { get; set; }
        [Required]
        public int subexpensetypeid { get; set; }
        public SubExpenseType SubExpenseTypes { get; set; }
        public int paymentmethodid { get; set; }
        public double amount { get; set; }
        public string note { get; set; }
        public string createby { get; set; }
        public DateTime createdate { get; set; }
        public string incometypename { get; set; }
        public string subincometypename { get; set; }
        public string paymentmethodname { get; set; }
    }
}