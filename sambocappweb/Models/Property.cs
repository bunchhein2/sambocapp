﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("property_tbl")]
    public class Property
    {
        public int id { get; set; }
        public string property { get; set; }
        public string note { get; set; }
    }
}