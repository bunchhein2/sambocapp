﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("question_tbl")]
    public class Question
    {
        public int id { get; set; }
        public int questiontypeid { get; set; }
        public QuestionType questionType { get; set; }
        [Required]
        [StringLength(255)]
        public string question { get; set; }
        [StringLength(255)]

        public string image { get; set; }
        public decimal minute { get; set; }
        public decimal score { get; set; }
    }
}