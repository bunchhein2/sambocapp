﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("currency_tbl")]
    public class Currency
    {
        [Required]
        public int id { get; set; }
        public string currencyname { get; set; }
        public string sign { get; set; }
        public string status { get; set; }
       // public bool statusss { get; set; }
    }
}