﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class CivilcaseTribunalV
    {
        [Required]
        public int id { get; set; }
        public DateTime date { get; set; }
        public int civilcaseid { get; set; }
        public int tribunalid { get; set; }
        public int lawyerid { get; set; }
        public int judgeid { get; set; }
        public string location { get; set; }
        public string note { get; set; }
        public Boolean status { get; set; }
        public int civilcaseno { get; set; }
        public string tribunalname { get; set; }
        public string lawyername { get; set; }
        public string judgename { get; set; }
        public string clerkname { get; set; }
        public string officername { get; set; }
    }
}