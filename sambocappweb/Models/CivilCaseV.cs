﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class CivilCaseV
    {
        public int id { get; set; }
        public DateTime civilcasedate { get; set; }
        [Required]
        public int civilcaseno { get; set; }
        [Required]
        public int casetypeid { get; set; }
        public string casetypename { get; set; }
        [Required]
        public int defendentid { get; set; }
        public string defendentname { get; set; }
        [Required]
        public int plaintiffid { get; set; }
        public string plaintiffname { get; set; }
        [Required]
        public int caseid { get; set; }
        public string casename { get; set; }
        [Required]
        public int lawyerid { get; set; }
        public string Lawyername { get; set; }
        public decimal charges { get; set; }
        public decimal services { get; set; }
        public string casestatus { get; set; }
        public string paidstatus { get; set; }
        public Boolean status { get; set; }
        public string createby { get; set; }
        public DateTime createdate { get; set; }
        public string customername { get; set; }
        public string relatename { get; set; }
        public string documentno { get; set; }
        public string positionname { get; set; }
        public string officename { get; set; }
    }
}