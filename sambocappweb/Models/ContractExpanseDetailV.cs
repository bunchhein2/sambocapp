﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class ContractExpanseDetailV
    {
        public int id { get; set; }
        public int contractexpenseid { get; set; }
        public int contractExpance { get; set; }
        public string description { get; set; }
        public decimal amount { get; set; }
        public int paymentmethodid { get; set; }
        public string paymentmethodname { get; set; }
    }
}