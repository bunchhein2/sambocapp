﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("privacy_tbl")]
    public class Privacy
    {
        public int id { get; set; }
        [Required]
        public string description { get; set; }
    }
}