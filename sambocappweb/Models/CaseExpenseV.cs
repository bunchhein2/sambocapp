﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class CaseExpenseV
    {
		public int id { get; set; }
		public int civilcaseid { get; set; }

		public int civilCase { get; set; }
		public DateTime? datetime { get; set; }
		public string userid { get; set; }
		public string note { get; set; }
		public DateTime? startdate { get; set; }
		public DateTime? enddate { get; set; }
		public string offerby { get; set; }
		public bool active { get; set; }
		public DateTime? createdate { get; set; }
	}
}