﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class CivilServant
    {
        public int Id { get; set; }

        public int RankId { get; set; }

        public Rank Rank { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string FullTitle { get; set; }

        [Required]
        [StringLength(255)]
        public string ShortTitle { get; set; }

        [Required]
        [StringLength(255)]
        public string Institution { get; set; }
    }
}