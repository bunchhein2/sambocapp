﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("officer_tbl")]
    public class Officer
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string officername { get; set; }
        public string officersex { get; set; }
        public string officerage { get; set; }
        public string officeraddress { get; set; }
        public string officeridentityno { get; set; }
        public string officerphone { get; set; }
        public string officerphoto { get; set; }
        public Boolean status { get; set; }
    }
}