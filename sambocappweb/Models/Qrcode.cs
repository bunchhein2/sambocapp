﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("Qrcode_tbl")]
    public class Qrcode
    {
        public int id { get; set; }
        public string qrcode { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}