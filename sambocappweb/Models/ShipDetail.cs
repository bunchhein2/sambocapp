﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("shipdetail_tbl")]
    public class ShipDetail
    {
        public int id { get; set; }
        public int ownership_id { get; set; }
        public string detailtype { get; set; }
        public string note { get; set; }
        public string attachment { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}