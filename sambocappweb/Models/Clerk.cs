﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("clerk_tbl")]
    public class Clerk
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string clerkname { get; set; }
        public string clerksex { get; set; }
        public string clerkage { get; set; }
        public string clerkaddress { get; set; }
        public string clerkidentityno { get; set; }
        public string clerkphone { get; set; }
        public string clerkphoto { get; set; }
        public Boolean status { get; set; }
    }
}