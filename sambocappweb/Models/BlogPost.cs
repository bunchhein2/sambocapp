﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class BlogPost
    {
        public int Id { get; set; }

        [Required]
        public int BlogCategoryId { get; set; }

        public BlogCategory BlogCategory { get; set; }

        [StringLength(255)]
        public string Author { get; set; }

        [Required]
        public DateTime PostDate { get; set; }

        public string Title { get; set; }

        [Required]
        public string TitleKh { get; set; }

        public string Content { get; set; }

        [Required]
        public string ContentKh { get; set; }

        [Required]
        public string Thumbnail { get; set; }

        [Required]
        public string PostStatus { get; set; }

        public int ViewCounter { get; set; }
    }
}