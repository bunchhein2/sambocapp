﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("banner_tbl")]
    public class Banner
    {
        public int id { get; set; }
        [Required]
        public DateTime date { get; set; }
        public String userid { get; set; }
        public int shopid { get; set; }
        public DateTime exireddate { get; set; }
        public string bannerimage { get; set; }
        [Required]
        public int qtymonth { get; set; }
        public string bannerstatus { get; set; }

    }
}