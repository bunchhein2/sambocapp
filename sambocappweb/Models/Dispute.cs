﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("Dispute_tbl")]
    public class Dispute
    {
        public int id { get; set; }
        public string deputenNme { get; set; }
        public string deputeNote { get; set; }
        public bool status { get; set; }
    }
}