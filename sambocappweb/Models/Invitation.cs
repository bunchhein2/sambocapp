﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class Invitation
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        public ApplicationUser User { get; set; }

        [Required]
        [StringLength(1020)]
        public string Objective { get; set; }

        [StringLength(1020)]
        public string Reference { get; set; }

        public string Content { get; set; }

        [Required]
        public int CivilServantFromId { get; set; }
        public CivilServant CivilServantFrom { get; set; }
    }
}