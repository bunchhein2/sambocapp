﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("judge_tbl")]
    public class Judge
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string judgename { get; set; }
        public string judgesex { get; set; }
        public string judgeage { get; set; }
        public string judgeaddress { get; set; }
        public string judgeidentityno { get; set; }
        public string judgephone { get; set; }
        public string judgephoto { get; set; }
        public Boolean status { get; set; }
    }
}