﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("tbl_registration")]
    public class Registration
    {
        public int id { get; set; }
        public DateTime date { get; set; }

        [StringLength(100)]
        public string userid { get; set; }
        [StringLength(100)]
        public string firstName { get; set; }
        [StringLength(100)]
        public string lastName { get; set; }
        [StringLength(100)]
        public string firstNameInKh { get; set; }
        [StringLength(100)]
        public string lastNameInKh { get; set; }
        public DateTime dob { get; set; }
        [StringLength(100)]
        public string gender { get; set; }
        [StringLength(100)]
        public string marrientalStatus { get; set; }
        [StringLength(100)]
        public string address { get; set; }
        [StringLength(100)]
        public string phone { get; set; }
        [StringLength(100)]
        public string email { get; set; }
        [StringLength(100)]
        public string workPlace { get; set; }
        [StringLength(100)]
        public string workAddress { get; set; }
        public int genderalEducationId { get; set; }
        //public GeneralEducation generalEducation { get; set; }

        [StringLength(100)]
        public string language { get; set; }
        public int skillId { get; set; }
        public Skill skill { get; set; }
        [StringLength(100)]
        public string status { get; set; }



    }
}