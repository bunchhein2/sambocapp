﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("tbl_shop")]
    public class Shop
    {
        public int id { get; set; }
        public string shopid { get; set; }
        [Required]
        [StringLength(100)]
        public string shopName { get; set; }
        [Required]
        [StringLength(100)]
        public string gender { get; set; }
        public DateTime dob { get; set; }
        public string nationality { get; set; }
        public string ownerName { get; set; }
        [Required]        
        public string phone { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string tokenid { get; set; }
        public string facebookPage { get; set; }
        [Required]
        public string location { get; set; }
        public string logoShop { get; set; }
        [Required]
        public string paymentType { get; set; }
        public string qrCodeImage { get; set; }
        [Required]
        public int bankNameid { get; set; }
        [Required]
        public string accountNumber { get; set; }
        [Required]
        [StringLength(100)]
        public string accountName { get; set; }
        [Required]

        public string feetype { get; set; }
        public decimal feecharge { get; set; }
        [Required]
        public DateTime shophistorydate { get; set; }
        [Required]
        public string note { get; set; }

        public string status { get; set; }
        //public Privacy description { get; set; }

    }
}