﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace sambocappweb.Models
{
        [Table("tbl_DeliveryType")]
    public class DeliveryType
    {
        public int id { get; set; }
        [Required]
        [StringLength(100)]
        public string delivery_name { get; set; }
        
        public string status { get; set; }
    }
}