﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class Feedback
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        [Required]
        [StringLength(510)]
        public string Subject { get; set; }

        [Required]
        [StringLength(2400)]
        public string Message { get; set; }
    }
}