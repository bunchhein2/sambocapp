﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("Location_tbl")]
    public class Location
    {
        public int id { get; set; }
        [Required]
        public string location { get; set; }
        public string active { get; set; }

    }
}