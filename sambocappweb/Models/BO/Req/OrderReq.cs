﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models.BO.Req
{
    public class OrderReq
    {
        
        public int Id { get; set; }
        public int InvoiceNo { get; set; }
        public DateTime Date { get; set; }
        public int ShopId { get; set; }
        //public Shop Shop { get; set; }

        public int CustomerId { get; set; }
        //public Customer Customer { get; set; }
        public string DeliveryTypeIn { get; set; }

        public string CurrentLocation { get; set; }

        [Required]
        [StringLength(50)]
        public string Phone { get; set; }

        public string PaymentType { get; set; }

        [Required]
        [StringLength(50)]
        public string QrcodeShopName { get; set; }

        [Required]
        [StringLength(50)]
        public string BankName { get; set; }

        [Required]
        [StringLength(50)]
        public string AccountNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string AccountName { get; set; }

        [Required]
        [StringLength(100)]
        public string ReceiptUpload { get; set; }
        public decimal AmountTobePaid { get; set; }
        public int ExchangeId { get; set; }
        //public Exchange Exchange { get; set; }

        [Required]
        [StringLength(50)]
        public string Status { get; set; }
    }
}