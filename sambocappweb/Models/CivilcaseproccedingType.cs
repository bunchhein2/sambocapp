﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("civilcaseproccedingtype_tbl")]
    public class CivilcaseproccedingType
    {
        [Required]
        public int id { get; set; }
        public string caseproccedingtype { get; set; }
        public string caseproccedingnote { get; set; }
    }
}