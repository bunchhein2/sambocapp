﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("CustomerContract_tbl")]
    public class ContractCustomer
    {
        [Required]
        public int id { get; set; }
        public string customername { get; set; }
        public string customersex { get; set; }
        public string customerage { get; set; }
        public string customeraddress { get; set; }
        public DateTime? customerdob { get; set; }
        public string customeridentityno { get; set; }
        public string customerphone { get; set; }
        public string customerphoto { get; set; }
        public Boolean? customerstatus { get; set; }
    }
}