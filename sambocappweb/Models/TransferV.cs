﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class TransferV
    {
        [Required]
        public int id { get; set; }
        public DateTime transferdate { get; set; }
        public int fromid { get; set; }
        public int toid { get; set; }
        public Decimal transferamount { get; set; }
        public string transfernote { get; set; }
        public string createby { get; set; }
        public DateTime createdate { get; set; }
        public string fromname { get; set; }
        public string toname { get; set; }
    }
}