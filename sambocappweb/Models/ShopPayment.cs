﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
	[Table("shopPaymentT_tbl")]
    public class ShopPayment
    {
		public int id { get; set; }
		public DateTime? date {get;set;}
		public int shopid { get; set; }
        //public Shop shop { get; set; }
        public string paytype { get; set; }
		public DateTime? startdate { get; set; }
		public DateTime? enddate { get; set; }
		public decimal amount { get; set; }
		public string note { get; set; }
        public string screenshot { get; set; }
        public int currentcyid { get; set; }
        //public Currency currency { get; set; }
        public int qtymonth { get; set; }

    }
}