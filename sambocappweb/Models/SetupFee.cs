﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("SetupFee_tbl")]
    public class SetupFee
    {
        public int id { get; set; }
        public DateTime? date{get;set;}
        public string feetype { get; set; }
        public decimal amount { get; set; }
        public string createby { get; set; }
        public DateTime? createdate { get; set; }
    }
}