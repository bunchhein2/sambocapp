﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("AdvetisePayment_tbl")]
    public class AdvertisePayment
    {
        public int id { get; set; }
        public DateTime? date {get;set;}
        public int advertiseid { get; set; }
        public decimal amount { get; set; }
        public string screenshot { get; set; }
        public string note { get; set; }
    }
}