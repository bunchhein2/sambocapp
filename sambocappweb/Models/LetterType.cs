﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("lettertype_tbl")]
    public class LetterType
    {
        public int id { get; set; }
        public string letter { get; set; }
        public string note { get; set; }
    }
}