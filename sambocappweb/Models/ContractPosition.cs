﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("contractposition_tbl")]
    public class ContractPosition
    {
        public int id { get; set; }
        public string name { get; set; }
        public string note { get; set; }
    }
}