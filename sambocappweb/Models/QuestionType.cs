﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("questiontype_tbl")]
    public class QuestionType
    {
        public int id { get; set; }
        [Required]
        [StringLength(255)]
        public string questiontypenote { get; set; }
    }
}