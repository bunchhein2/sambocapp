﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    public class CivilcaseAll
    {
        public int id { get; set; }
        public DateTime civilcasedate { get; set; }
        [Required]
        public int civilcaseno { get; set; }
        [Required]
        public int officeid { get; set; }
        [Required]
        public string documentno { get; set; }
        [Required]
        public int caseid { get; set; }
        [Required]
        public int casetypeid { get; set; }
        [Required]
        public int defendentid { get; set; }
        public int plaintiffid { get; set; }
        public int lawyerid { get; set; }
        public int customerid { get; set; }
        public int casepositionid { get; set; }
        public int lawrelateid { get; set; }
        public Decimal charges { get; set; }
        public Decimal services { get; set; }
        public String casestatus { get; set; }
        public string paidstatus { get; set; }
        public string createby { get; set; }
        public DateTime createdate { get; set; }
       

        //For CaseProcceding
        public int pid { get; set; }
        public DateTime caseproceedingdate { get; set; }
        public int caseprocedinglawyerid { get; set; }
        public int caseprocedingtypeid { get; set; }

        //For CaseTribunal
        public int tid { get; set; }
        public DateTime casetribunaldate { get; set; }
        public int casetribunalid { get; set; }
        public int caselawyerids { get; set; }
        public int casejudgeid { get; set; }
        public int caseclerkid { get; set; }
        public int caseofficerid { get; set; }
        public string caselocation { get; set; }
    }
}