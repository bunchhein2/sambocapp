﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("tbl_guideline")]
    public class GuideLine
    {
        public int id { get; set; }
        [Required]
        [StringLength(100)]
        public string titleGuideline { get; set; }
        [StringLength(100)]
        public string guidelineNote { get; set; }
        [StringLength(100)]
        public string createBy { get; set; }
        public DateTime createDate { get; set; }
    }
}