﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("tbl_newEvent")]
    public class NewEvent
    {
        public int id { get; set; }
        [Required]
        public DateTime newEventDate { get; set; }
        [Required]
        [StringLength(100)]
        public string newEventTitle { get; set; }
        public string newEventImage { get; set; }
        [StringLength(100)]
        public string newEventNote { get; set; }
        [StringLength(100)]
        public string status { get; set; }
        [StringLength(100)]
        public string createBy { get; set; }
        public DateTime createDate { get; set; }

    }
}