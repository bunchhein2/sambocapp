﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using sambocappweb.Models;

namespace sambocappweb.Models
{
    [Table("tbl_generalEducation")]
    public class GeneralEducation
    {
        [Key]
        public int id { get; set; }
        [StringLength(100)]
        public string userid { get; set; }
        public int fromYear { get; set; }
        public int toYear { get; set; }
        public string note { get; set; }
    }
}

/* 
 id
userid
fromyear
toyear
note
 */