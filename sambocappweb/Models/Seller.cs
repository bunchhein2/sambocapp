﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace sambocappweb.Models
{
    [Table("Seller_tbl")]
    public class Seller
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string sellerName { get; set; }
        public string sellerSex { get; set; }
        public string sellerAge { get; set; }
        public string sellerAddress { get; set; }
        public DateTime? sellerDob { get; set; }
        public string sellerIdentityno { get; set; }
        public string sellerPhone { get; set; }
        public string sellerPhoto { get; set; }

        public string sellerWithname { get; set; }
        public string sellerWithsex { get; set; }
        public string sellerWithage { get; set; }
        public string sellerWithaddress { get; set; }
        public DateTime? sellerWithdob { get; set; }
        public string sellerWithidentityno { get; set; }
        public string sellerWithphone { get; set; }
        public string sellerWithphoto { get; set; }
        public Boolean? status { get; set; }
    }
}