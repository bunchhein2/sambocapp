﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using sambocappweb.Models;

namespace sambocappweb.Controllers
{
    public class ProductsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        // GET: Products
        public ActionResult Index( int id=0)
        {
            //var products = db.Products.ToList();
            ViewBag.ShopList = new SelectList(db.Shops, "Id","ShopName");
            ViewBag.CurrencyList = new SelectList(db.Currencies, "Id", "CurrencyName");
            ViewBag.productList = new SelectList(db.Products, "Id", "ProductName");
            count();
            return View();
        }
        public ActionResult create(int id = 0)
        {
           var  Pro = new Product();
            var proCode = db.Products.OrderByDescending(c => c.Id).FirstOrDefault();
            if (id != 0)
            {
                proCode = db.Products.Where(x => x.Id == id).FirstOrDefault<Product>();
            }
            else if (proCode == null)
            {
                Pro.ProductCode = "Code 001";

            }
            else
            {
                Pro.ProductCode = "Code" + (Convert.ToInt32(Pro.ProductCode.Substring(9, Pro.ProductCode.Length - 9)) + 1).ToString();
            }
            return View (Pro);
        }
        public void count()
        {
            var viewbage = db.Shops.Count();
            var view1 = viewbage + 1;
            ViewBag.Count = "Code00" + view1;
            
        }
    }
}
