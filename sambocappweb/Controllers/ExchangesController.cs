﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Globalization;

namespace sambocappweb.Controllers
{
    public class ExchangesController : Controller
    {
        private ApplicationDbContext _context;
        public ExchangesController()
        {
            _context = new ApplicationDbContext();

        }
        // GET: Exchanges
        public ActionResult Index()
        {
            //var exchangeIC = _context.Exchanges.Include(c => c.currencyid).ToList();
            ViewBag.Shoplist = new SelectList(_context.Shops, "Id", "ShopName");
            ViewBag.Currencylist = new SelectList(_context.Currencies, "id", "currencyname");

            return View();
        }
        
    }
}