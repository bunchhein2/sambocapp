﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace sambocappweb.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }
        [Route("saledetail-list")]
        [System.Web.Mvc.HttpGet]
        public ActionResult GetSaleDetailReport()
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("Select * From SaleDetailV", con);
            adp.Fill(ds);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(100);
            reportViewer.Height = Unit.Percentage(100);
            ReportParameter username = new ReportParameter("username", User.Identity.Name);
            //reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\SaleDetailRpt.rdlc";
            //reportViewer.LocalReport.SetParameters(new ReportParameter[] { fdate, tdate, stus, username });
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds));
            ViewBag.ReportViewer = reportViewer;
            return View("_saledetailrpt");
        }
        [Route("saledetail-list/{fromdate}/{todate}")]
        [System.Web.Mvc.HttpGet]
        public ActionResult GetSaleDetialReport(string fromdate, string todate, string shopname)
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("Select * From SaleDetailV where date >=@fromdate and date <=@todate", con);
            adp.SelectCommand.Parameters.AddWithValue("@fromdate", fromdate);
            adp.SelectCommand.Parameters.AddWithValue("@todate", todate);
            adp.Fill(ds);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(100);
            reportViewer.Height = Unit.Percentage(100);
            ReportParameter fdate = new ReportParameter("fromdate", fromdate);
            ReportParameter tdate = new ReportParameter("todate", todate);
            ReportParameter username = new ReportParameter("username", User.Identity.Name);
            //reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\SaleDetailRpt.rdlc";
            reportViewer.LocalReport.SetParameters(new ReportParameter[] { fdate, tdate, username });
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds));
            ViewBag.ReportViewer = reportViewer;
            return View("_saledetailrpt");
        }
        [Route("sale-list")]
        [System.Web.Mvc.HttpGet]
        public ActionResult GetSaleReport()
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("Select * From sale_v", con);
            //adp.SelectCommand.Parameters.AddWithValue("@fromdate", fromdate);
            //adp.SelectCommand.Parameters.AddWithValue("@todate", todate);
            //adp.SelectCommand.Parameters.AddWithValue("@status", status);
            adp.Fill(ds);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(100);
            reportViewer.Height = Unit.Percentage(100);
            //ReportParameter fdate = new ReportParameter("fromdate", fromdate);
            //ReportParameter tdate = new ReportParameter("todate", todate);
            //ReportParameter stus = new ReportParameter("status", status);
            ReportParameter username = new ReportParameter("username", User.Identity.Name);
            //reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\SaleRpt.rdlc";
            //reportViewer.LocalReport.SetParameters(new ReportParameter[] { fdate, tdate, stus, username });
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds));
            ViewBag.ReportViewer = reportViewer;
            return View("_salerpt");
        }

        [Route("sale-list/{fromdate}/{todate}/{status}")]
        [System.Web.Mvc.HttpGet]
        public ActionResult GetSaleReport(string fromdate, string todate, string shopname)
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("Select * From sale_v where date >=@fromdate and date <=@todate and shopid=@shopname", con);
            adp.SelectCommand.Parameters.AddWithValue("@fromdate", fromdate);
            adp.SelectCommand.Parameters.AddWithValue("@todate", todate);
            adp.SelectCommand.Parameters.AddWithValue("@shopname", shopname);
            adp.Fill(ds);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(100);
            reportViewer.Height = Unit.Percentage(100);
            ReportParameter fdate = new ReportParameter("fromdate", fromdate);
            ReportParameter tdate = new ReportParameter("todate", todate);
            ReportParameter stus = new ReportParameter("shopname", shopname);
            ReportParameter username = new ReportParameter("username", User.Identity.Name);
            //reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\SaleRpt.rdlc";
            reportViewer.LocalReport.SetParameters(new ReportParameter[] { fdate, tdate, stus, username });
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds));
            ViewBag.ReportViewer = reportViewer;
            return View("_salerpt");
        }
        [Route("banner-list")]
        [System.Web.Mvc.HttpGet]
        public ActionResult GetBannerReport()
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("Select * From BannerV", con);
            adp.Fill(ds);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(100);
            reportViewer.Height = Unit.Percentage(100);
            ReportParameter username = new ReportParameter("username", User.Identity.Name);
            //reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\BannerRpt.rdlc";
            reportViewer.LocalReport.SetParameters(new ReportParameter[] { username });
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds));
            ViewBag.ReportViewer = reportViewer;
            return View("_bannerrpt");
        }
        [Route("banner-list/{fromdate}/{todate}")]
        [System.Web.Mvc.HttpGet]
        public ActionResult GetBannerbydatReport(string fromdate, string todate, string status)
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("Select * From BannerV where date >=@fromdate and date <=@todate", con);
            adp.SelectCommand.Parameters.AddWithValue("@fromdate", fromdate);
            adp.SelectCommand.Parameters.AddWithValue("@todate", todate);
            adp.Fill(ds);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(100);
            reportViewer.Height = Unit.Percentage(100);
            ReportParameter fdate = new ReportParameter("fromdate", fromdate);
            ReportParameter tdate = new ReportParameter("todate", todate);
            ReportParameter username = new ReportParameter("username", User.Identity.Name);
            //reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\BannerRpt.rdlc";
            reportViewer.LocalReport.SetParameters(new ReportParameter[] { fdate, tdate,username });
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds));
            ViewBag.ReportViewer = reportViewer;
            return View("_bannerrpt");
        }
        [Route("banner-list/{fromdate}/{todate}/{status}")]
        [System.Web.Mvc.HttpGet]
        public ActionResult GetBannerbydateReport(string fromdate, string todate, string status)
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("Select * From BannerV where date >=@fromdate and date <=@todate and status=@status", con);
            adp.SelectCommand.Parameters.AddWithValue("@fromdate", fromdate);
            adp.SelectCommand.Parameters.AddWithValue("@todate", todate);
            adp.SelectCommand.Parameters.AddWithValue("@status", status);
            adp.Fill(ds);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(100);
            reportViewer.Height = Unit.Percentage(100);
            ReportParameter fdate = new ReportParameter("fromdate", fromdate);
            ReportParameter tdate = new ReportParameter("todate", todate);
            ReportParameter stus = new ReportParameter("status", status);
            ReportParameter username = new ReportParameter("username", User.Identity.Name);
            //reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\BannerRpt.rdlc";
            reportViewer.LocalReport.SetParameters(new ReportParameter[] { fdate, tdate, stus, username });
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds));
            ViewBag.ReportViewer = reportViewer;
            return View("_bannerrpt");
        }
        [Route("customer-list")]
        [System.Web.Mvc.HttpGet]
        public ActionResult GetCustomerReport()
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("Select * From tbl_customer", con);
            adp.Fill(ds);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(100);
            reportViewer.Height = Unit.Percentage(100);
            ReportParameter username = new ReportParameter("username", User.Identity.Name);
            //reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\CustomerRpt.rdlc";
            reportViewer.LocalReport.SetParameters(new ReportParameter[] {username});
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds));
            ViewBag.ReportViewer = reportViewer;
            return View("_customerrpt");
        }
        [Route("customer-list/{fromdate}/{todate}")]
        [System.Web.Mvc.HttpGet]
        public ActionResult GetCustomerByDateReport(string fromdate, string todate)
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("Select * From tbl_customer where date >=@startdate and date <=@enddate", con);
            adp.SelectCommand.Parameters.AddWithValue("@startdate", fromdate);
            adp.SelectCommand.Parameters.AddWithValue("@enddate", todate);
            adp.Fill(ds);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(100);
            reportViewer.Height = Unit.Percentage(100);
            ReportParameter fdate = new ReportParameter("startdate", fromdate);
            ReportParameter tdate = new ReportParameter("enddate", todate);
            ReportParameter username = new ReportParameter("username", User.Identity.Name);
            //reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\CustomerRpt.rdlc";
            reportViewer.LocalReport.SetParameters(new ReportParameter[] { fdate, tdate, username });
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds));
            ViewBag.ReportViewer = reportViewer;
            return View("_customerrpt");
        }
        [Route("shop-list")]
        [System.Web.Mvc.HttpGet]
        public ActionResult GetInvoiceReport()
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("Select * From tbl_shop", con);
            //adp.SelectCommand.Parameters.AddWithValue("@fromdate", fromdate);
            //adp.SelectCommand.Parameters.AddWithValue("@todate", todate);
            //adp.SelectCommand.Parameters.AddWithValue("@status", status);
            adp.Fill(ds);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(100);
            reportViewer.Height = Unit.Percentage(100);
            //ReportParameter fdate = new ReportParameter("fromdate", fromdate);
            //ReportParameter tdate = new ReportParameter("todate", todate);
            //ReportParameter stus = new ReportParameter("status", status);
            ReportParameter username = new ReportParameter("username", User.Identity.Name);
            //reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\ShopRpt.rdlc";
            //reportViewer.LocalReport.SetParameters(new ReportParameter[] { fdate, tdate, stus, username });
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds));
            ViewBag.ReportViewer = reportViewer;
            return View("_shoprpt");
        }
        
        [Route("shop-list/{fromdate}/{todate}/{status}")]
        [System.Web.Mvc.HttpGet]
        public ActionResult GetInvoiceReport(string fromdate, string todate,string status)
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("Select * From tbl_shop where shophistorydate >=@fromdate and shophistorydate <=@todate and status=@status", con);
            adp.SelectCommand.Parameters.AddWithValue("@fromdate", fromdate);
            adp.SelectCommand.Parameters.AddWithValue("@todate", todate);
            adp.SelectCommand.Parameters.AddWithValue("@status", status);
            adp.Fill(ds);

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(100);
            reportViewer.Height = Unit.Percentage(100);
            ReportParameter fdate = new ReportParameter("fromdate", fromdate);
            ReportParameter tdate = new ReportParameter("todate", todate);
            ReportParameter stus = new ReportParameter("status", status);
            ReportParameter username = new ReportParameter("username", User.Identity.Name);
            //reportViewer.LocalReport.EnableExternalImages = true;
            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\ShopRpt.rdlc";
            reportViewer.LocalReport.SetParameters(new ReportParameter[] { fdate, tdate, stus,username });
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds));
            ViewBag.ReportViewer = reportViewer;
            return View("_shoprpt");
        }
    }
}