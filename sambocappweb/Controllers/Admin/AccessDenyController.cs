﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sambocappweb.Controllers.Admin
{
    public class AccessDenyController : Controller
    {
        // GET: AccessDeny
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
    }
}