﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using sambocappweb.Models;

namespace sambocappweb.Controllers.Admin
{
    public class RoleController : Controller
    {
        ApplicationDbContext context;
        private ApplicationUserManager _userManager;

        public RoleController()
        {
            context = new ApplicationDbContext();
        }
        public RoleController(ApplicationUserManager userManager)
        {
            UserManager = userManager;

        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Role
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {


                if (!isSuperAdmin())
                {
                    return RedirectToAction("Index", "AccessDeny");
                }
            }
            else
            {
                return RedirectToAction("Index", "Role");
            }
            var Roles = context.Roles.ToList();
            return View(Roles);
        }
        public ActionResult Create()
        {
            if (User.Identity.IsAuthenticated)
            {


                if (!isSuperAdmin())
                {
                    return RedirectToAction("Index", "AccessDeny");
                }
            }
            else
            {
                return RedirectToAction("Index", "Role");
            }

            var Role = new IdentityRole();
            return View(Role);
        }
        [HttpPost]
        public ActionResult Create(IdentityRole Role)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (!isSuperAdmin())
                {
                    return RedirectToAction("Index", "AccessDeny");
                }
            }
            else
            {
                return RedirectToAction("Index", "Role");
            }
            if (Role.Name!="")
            {
               var result= context.Roles.Add(Role);
                if (result != null)
                {
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Error add role");
                }
                
            }
            return View(Role);   
        }
        #region helper admin
        public Boolean isSuperAdmin()
        {
            if (User.Identity.IsAuthenticated)
            {
                //var usersign = User.Identity;
               
                var user = UserManager.FindByEmail(User.Identity.Name);
                if (user.IsSuperUser==true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        #endregion helper admin
    }
}