﻿using sambocappweb.Models;
using sambocappweb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sambocappweb.Controllers
{
    public class ShopPaymentsController : Controller
    {
        private ApplicationDbContext _context;

        public ShopPaymentsController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: ShopPayments
        public ActionResult Index()
        {
            var shopViewModel = new Manage_ShopViewModels()
            {
                shopes = _context.Shops.ToList(),
                SetupFees=_context.SetupFees.ToList()
            };
            return View(shopViewModel);
        }
        
    }
}