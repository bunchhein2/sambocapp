﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class DashboardsController : ApiController
    {
        private ApplicationDbContext _context;

        public DashboardsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: /api/dashboards?name={name}
        [HttpGet]
        [Route("api/Dashboards/{name}")]
        public IHttpActionResult GetDashboards(string name)
        {
            DataTable dt = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection conx = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("SELECT * FROM PerformanceDashboard WHERE Name='" + name + "'", conx);
            adp.Fill(dt);

            return Ok(dt);
        }

        

        // GET: /api/dashboards?type={type}
        [HttpGet]
        [Route("api/Dashboards/{type}/{a}")]
        public IHttpActionResult GetDepartmentPerformance(string type,string a)
        {
            DataTable dt = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection conx = new SqlConnection(connectionString);
            if (type == "CurrentTasks")
            {
                SqlDataAdapter adp = new SqlDataAdapter("SELECT Name,SUM(CT) AS CT FROM PerformanceDashboard GROUP BY NAME", conx);
                adp.Fill(dt);
            }
            else if (type == "OneMonthPerformance")
            {
                SqlDataAdapter adp = new SqlDataAdapter("SELECT Name,SUM(CMCT) AS CMCT FROM PerformanceDashboard GROUP BY NAME", conx);
                adp.Fill(dt);
            }
            else if (type == "ThreeMonthsPerformance")
            {
                SqlDataAdapter adp = new SqlDataAdapter("SELECT Name,SUM(TMCT) AS TMCT FROM PerformanceDashboard GROUP BY NAME", conx);
                adp.Fill(dt);
            }
            else if (type == "SixMonthsPerformance")
            {
                SqlDataAdapter adp = new SqlDataAdapter("SELECT Name,SUM(SMICT) AS SMICT FROM PerformanceDashboard GROUP BY NAME", conx);
                adp.Fill(dt);
            }
            else if (type == "OneYearPerformance")
            {
                SqlDataAdapter adp = new SqlDataAdapter("SELECT Name,SUM(OYCT) AS OYCT FROM PerformanceDashboard GROUP BY NAME", conx);
                adp.Fill(dt);
            }
            
            return Ok(dt);
        }

        [Route("api/Dashboards")]
        [HttpGet]
        public IHttpActionResult GetLoggedInRecords()
        {
            var userId = User.Identity.GetUserId();
            var user = _context.Users.SingleOrDefault(c => c.Id == userId);
            var records = _context.LoginHistories.Where(c => c.LoggedBy == user.FirstName + " " + user.LastName).ToList().OrderByDescending(c => c.Id).Take(20);

            return Ok(records);
        }
    }
}
