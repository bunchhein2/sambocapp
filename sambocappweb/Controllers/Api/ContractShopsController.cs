﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class ContractShopsController : ApiController
    {
        private ApplicationDbContext _context;
        public ContractShopsController()
        {
            _context = new ApplicationDbContext();

        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        ////GET : /api/ContractShops/id={di} for get all record
        //[HttpGet]
        //public IHttpActionResult GetFiltter(string findid)
        //{
        //    if (findid == "all")
        //    {
        //        var filterid = _context.Contracts.Select(Mapper.Map<Contract, ContractDto>);
        //        return Ok(filterid);
        //    }
        //    else
        //    {
        //        var filterid = _context.Contracts.Select(Mapper.Map<Contract, ContractDto>)
        //                                    .Where(c => c.id == c.id); //(c => c.is_active == true);
        //        return Ok(filterid);

        //    }

        //}
        //GET : /api/ContractShops/id={di} for get all record
        [HttpGet]
        public IHttpActionResult GetFilter(string id)
        {
            if (id == "id")
            {
                var request = _context.Contracts.Select(Mapper.Map<Contract, ContractDto>);
                return Ok(request);
            }
            else
            {
                var request = _context.Contracts.Select(Mapper.Map<Contract, ContractDto>)
                                            .Where(c => c.status == true);
                return Ok(request);
            }

        }

        [HttpGet]
        //Get : api/ContractShops{id}
        public IHttpActionResult GetEditContract(int id)
        {
            var getById = _context.Contracts.SingleOrDefault(c => c.contractid == id);
            if (getById == null)
                return NotFound();

            return Ok(Mapper.Map<Contract, ContractDto>(getById));
        }


        //GET : /api/ContractShops (get all record)
        [HttpGet]
        public IHttpActionResult GetRecord( )
        {

            var Contract = (from c in _context.Contracts
                            join s in _context.Shops on c.shopid equals s.id
                            where c.status == true
                            select new
                            {
                                id = c.contractid,
                                shopid = c.shopid,
                                shopName = s.shopName,
                                date = c.date,
                                attachment = c.attachment,
                                note = c.note,
                                status = c.status
                            }).ToList();
            return Ok(Contract);


            //return Ok(_context.Contracts.ToList().Select(Mapper.Map<Contract, ContractDto>).Where(c => c.status == true));

        }

        //POS : /api/ContractShops  for Insert record
        [HttpPost]
        public IHttpActionResult Create()
        {

            //var contractid = HttpContext.Current.Request.Form["id"];
            var shopid = HttpContext.Current.Request.Form["shopid"];
            var date = HttpContext.Current.Request.Form["date"];
            var attachment = HttpContext.Current.Request.Files["attachment"];
            var note = HttpContext.Current.Request.Form["note"];
            //var status = HttpContext.Current.Request.Form["status"];

            string filsname = "";

            if (attachment != null)
            {
                filsname = Path.Combine(Path.GetDirectoryName(attachment.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(attachment.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(attachment.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), filsname);
                attachment.SaveAs(savePath);
            }
            var Contract1 = new ContractDto()
            {
                //id = Int32.Parse(id),
                shopid = int.Parse(shopid),
                date = DateTime.Parse(date),
                attachment = filsname,
                note = note,
                status = true,
            };
            var contract = Mapper.Map<ContractDto, Contract>(Contract1);
            _context.Contracts.Add(contract);
            _context.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + Contract1.contractid), Contract1);
        }


        //PUT : /api/ContractShops/{id}  for Update record
        [HttpPut]
        public IHttpActionResult Update(int id)
        {
            //var contractid = HttpContext.Current.Request.Form["id"];
            var shopid = HttpContext.Current.Request.Form["shopid"];
            var date = HttpContext.Current.Request.Form["date"];
            var attachment = HttpContext.Current.Request.Files["attachment"];
            var note = HttpContext.Current.Request.Form["note"];
            var status = HttpContext.Current.Request.Form["status"];
            var file_old = HttpContext.Current.Request.Form["file_old"];

            var ContractIN = _context.Contracts.SingleOrDefault(c => c.contractid == id);

            string file_New = "";
            file_New = file_old;

            if (attachment != null)
            {
                file_New = Path.Combine(Path.GetDirectoryName(attachment.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(attachment.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(attachment.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), file_New);
                attachment.SaveAs(savePath);
                //Delete Old Image
                var oldFilePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), ContractIN.attachment);

                if (File.Exists(oldFilePath))
                {
                    File.Delete(oldFilePath);
                }

            }

            var contractDto = new ContractDto()
            {
                contractid = id,
                shopid = int.Parse(shopid),
                date = DateTime.Parse(date),
                attachment = file_New,
                note = note,
                status = true,

            };
            Mapper.Map(contractDto, ContractIN);
            _context.SaveChanges();

            return Ok(new { });

        }
        
        //DELETE : /api/ContractShops/{id}  for Delete record
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var ExchangeInDb = _context.Contracts.SingleOrDefault(c => c.contractid == id);
            if (ExchangeInDb == null)
                return NotFound();

            //_context.Contracts.Remove(ExchangeInDb);
            ExchangeInDb.status = false;
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
