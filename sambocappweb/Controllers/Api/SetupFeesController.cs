﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace sambocappweb.Controllers.Api
{
    public class SetupFeesController : ApiController
    {
        private ApplicationDbContext _context;

        public SetupFeesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        
        [HttpGet]
        //Get : api/Exchanges
        [Route("api/SetupFees")]
        public IHttpActionResult GetSetupFees()
        {
            var getSetupFees = _context.SetupFees.ToList().Select(Mapper.Map<SetupFee, SetupFeeDto>);

            return Ok(getSetupFees);
        }


        [HttpGet]
        //Get : api/Exchanges{id}
        [Route("api/SetupFees/{id}")]
        public IHttpActionResult GetSetupFeesId(int id)
        {
            var getSetupFeesById = _context.SetupFees.SingleOrDefault(c => c.id == id);

            if (getSetupFeesById == null)
                return NotFound();

            return Ok(Mapper.Map<SetupFee, SetupFeeDto>(getSetupFeesById));
        }
        [HttpPost]
        [Route("api/SetupFees")]
        public IHttpActionResult CreateSetupFees(SetupFeeDto SetupFeeDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.SetupFees.SingleOrDefault(c => c.feetype == SetupFeeDtos.feetype);
            if (isExist != null)
                return BadRequest();

            var SetupFees = Mapper.Map<SetupFeeDto, SetupFee>(SetupFeeDtos);
            SetupFeeDtos.createby = User.Identity.GetUserName();
            SetupFeeDtos.date = DateTime.Today;
            _context.SetupFees.Add(SetupFees);
            _context.SaveChanges();

            SetupFeeDtos.id = SetupFees.id;

            return Created(new Uri(Request.RequestUri + "/" + SetupFeeDtos.id), SetupFeeDtos);
        }

        [HttpPut]
        [Route("api/SetupFees/{id}")]
        //PUT : /api/ProductTypes/{id}
        public IHttpActionResult EditSetupFees(int id, SetupFeeDto SetupFeeDtos)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var isExist = _context.SetupFees.SingleOrDefault(c => c.id == SetupFeeDtos.id && c.id != SetupFeeDtos.id);
            if (isExist != null)
                return BadRequest();

            var SetupFeesInDb = _context.SetupFees.SingleOrDefault(c => c.id == id);
            SetupFeeDtos.date = DateTime.Today;
            Mapper.Map(SetupFeeDtos, SetupFeesInDb);
            _context.SaveChanges();

            return Ok(SetupFeeDtos);
        }

        [HttpDelete]
        [Route("api/SetupFees/{id}")]
        //PUT : /api/Exchanges/{id}
        public IHttpActionResult DeleteSetupFees(int id)
        {

            var SetupFeesInDb = _context.SetupFees.SingleOrDefault(c => c.id == id);
            if (SetupFeesInDb == null)
                return NotFound();
            _context.SetupFees.Remove(SetupFeesInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
