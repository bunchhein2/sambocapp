﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using sambocappweb.Dtos;
using sambocappweb.Models;
using sambocappweb.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace sambocappweb.Controllers.Api
{
    public class ContractsController : ApiController
    {
        private ApplicationDbContext _context;
        public ContractsController()
        {
            _context = new ApplicationDbContext();

        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        //[HttpGet]
        //[Route("api/Banners/{a}/{b}")]
        //public IHttpActionResult GetMaxID(String a, String b)
        //{
        //    //For Get Max PaymentNo +1
        //    DataTable ds = new DataTable();
        //    var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        //    SqlConnection conx = new SqlConnection(connectionString);
        //    SqlDataAdapter adp = new SqlDataAdapter("SELECT CASE WHEN MAX(civilcaseno) IS NULL THEN (1)ELSE MAX(civilcaseno)+1 END AS ID,'INV' + RIGHT('000000' + CONVERT(NVARCHAR, (SELECT CASE WHEN MAX(civilcaseno) IS NULL THEN (1)ELSE MAX(civilcaseno)+1 END )) , 6) AS InvoiceID FROM Civilcase_tbl", conx);
        //    adp.Fill(ds);
        //    string InvNo = ds.Rows[0][0].ToString();
        //    string InvNoFormat = ds.Rows[0][1].ToString();
        //    return Ok(InvNo + "," + InvNoFormat);

        //}

        //GET : /api/Parents/{id} for get record by id
        //[HttpGet]
        //[Route("api/Banners/{a}")]
        //public IHttpActionResult GetInvoiceNotPaid(string a)
        //{
        //    DataTable dt = new DataTable();
        //    var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        //    SqlConnection conx = new SqlConnection(connectionString);
        //    SqlDataAdapter adp = new SqlDataAdapter("select * from invoice_view where id in (select invoiceid from invoicedetail_tbl where deliverytype in(N'កំពុងដឹក',N'ដឹកបន្ត')) and cast(date as date)< cast(getdate() as date) group by id,invoiceno,date,customerid,showroomid,showroomname, customername,totalamount,totalcarprice,totalshipprice,alreadypaid,status,createby,createdate,paid,alreadymove", conx);
        //    adp.Fill(dt);
        //    return Ok(dt);

        //}

    }
}
