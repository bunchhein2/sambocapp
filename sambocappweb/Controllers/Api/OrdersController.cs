﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class OrdersController : ApiController
    {
        private ApplicationDbContext _context;

        public OrdersController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        //[HttpGet]
        //[Route("api/Orders/{shopName},{qrCodeImage},{id}")]
        //public IHttpActionResult ShopInfoSelect(string shopName, string qrCodeImage,int id)
        //{
        //    DataTable ds = new DataTable();
        //    var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        //    SqlConnection conx = new SqlConnection(connectionString);
        //    SqlDataAdapter adp = new SqlDataAdapter("select shopName,qrCodeImage from tbl_shop where id=id", conx);
        //    adp.Fill(ds);
        //    string ShopInfoSelect = ds.Rows[0][0].ToString();
        //    return Ok(ShopInfoSelect);

        //}
        //GET : /api/Balances/date={date}  for get all record
        //[HttpGet]
        //[Route("api/Orders/{a}/{b}/{c}/{d}")]
        //public IHttpActionResult ShopInfo(String a, String b)
        //{
        //    var result = _context.Exchanges.OrderByDescending(a => a.id).FirstOrDefault();

        //    //DataTable ds = new DataTable();
        //    //var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        //    //SqlConnection conx = new SqlConnection(connectionString);
        //    //SqlDataAdapter adp = new SqlDataAdapter("SELECT TOP 1 feecharge FROM tbl_shop order by id desc", conx);
        //    //adp.Fill(ds);
        //    //string ShopInfo = ds.Rows[0][0].ToString();
        //    //return Ok(ShopInfo);
        //    return Ok(result.id);

        //}
        [HttpGet]
        [Route("api/Orders/{aa}/{bb}/{cc}")]
        public IHttpActionResult exid(String aa, String bb)
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection conx = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("SELECT TOP 1 rate FROM exchange_tbl order by id desc", conx);
            adp.Fill(ds);
            string Exid = ds.Rows[0][0].ToString();
            return Ok(Exid);

        }
        [HttpGet]
        [Route("api/Orders/{a}/{b}")]
        public IHttpActionResult GetMaxID(String a, String b)
        {
            //For Get Max PaymentNo +1
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection conx = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("SELECT CASE WHEN MAX(InvoiceNo) IS NULL THEN (1)ELSE MAX(InvoiceNo)+1 END AS ID,'INV' + RIGHT('000000' + CONVERT(NVARCHAR, (SELECT CASE WHEN MAX(InvoiceNo) IS NULL THEN (1)ELSE MAX(InvoiceNo)+1 END )) , 6) AS InvoiceID FROM Orders", conx);
           
            adp.Fill(ds);
            string InvNo = ds.Rows[0][0].ToString();
            string InvNoFormat = ds.Rows[0][1].ToString();
            return Ok(InvNo + "," + InvNoFormat);

        }

        // GET: /api/orders
        [HttpGet]
        [Route("api/Orders")]
        public IHttpActionResult GetOrders()
        {

            var orders = (from o in _context.Orders
                          join s in _context.Shops on o.ShopId equals s.id
                          join c in _context.Customers on o.CustomerId equals c.id
                          join e in _context.Exchanges on o.ExchangeId equals e.id
                          join cu in _context.Currencies on e.currencyid equals cu.id
                          select new
                          {
                              Id=o.Id,
                              InvoiceNo=""+"No00"+o.InvoiceNo,
                              Date=o.Date,
                              ShopId =""+"ID00"+s.id,
                              shopName=s.shopName,
                              customerId=c.customerName,
                              DeliveryTypeIn=o.DeliveryTypeIn,
                              CurrentLocation=o.CurrentLocation,
                              PaymentType=o.PaymentType,
                              phone =o.Phone,
                              QrcodeShopName=o.QrcodeShopName,
                              BankName=o.BankName,
                              AccountNumber=o.AccountNumber,
                              AccountName=o.AccountName,
                              ReceiptUpload=o.ReceiptUpload,
                              AmountTobePaid=o.AmountTobePaid,
                              ExchangeId=e.rate,
                              Status=o.Status,
                              currencyid=cu.currencyname

                          }).ToList();


            //var orders = _context.Orders.Include(c => c.Shop).Include(c => c.Customer);

            //ExchangeList
            return Ok(orders);
        }
        // GET: /api/orders/{id}
        [HttpGet]
        [Route("api/Orders/{Id}")]
        public IHttpActionResult GetOrder(int Id)
        {
            var order = _context.Orders.SingleOrDefault(c => c.Id == Id);

            if (order == null)
                return NotFound();

            return Ok(Mapper.Map<Order, OrderDto>(order));
        }

        // POST: /api/orders
        [HttpPost]
        public IHttpActionResult CreateOder()
        {
            var invoiceNo = HttpContext.Current.Request.Form["InvoiceNo"];
            var shopId = HttpContext.Current.Request.Form["ShopId"];
            var customerId = HttpContext.Current.Request.Form["CustomerId"];
            
            var phone = HttpContext.Current.Request.Form["Phone"];
            var bankName = HttpContext.Current.Request.Form["BankName"];
            var accountNumber = HttpContext.Current.Request.Form["AccountNumber"];
            var accountName = HttpContext.Current.Request.Form["AccountName"];
            var currentLocation = HttpContext.Current.Request.Form["CurrentLocation"];
            var deliveryTypeIn = HttpContext.Current.Request.Form["DeliveryTypeIn"];

            var Photo = HttpContext.Current.Request.Files["ReceiptUpload"];
            var qrcodeShopName = HttpContext.Current.Request.Files["QrcodeShopName"];

            var paymentType = HttpContext.Current.Request.Form["PaymentType"];
            var amountTobePaid = HttpContext.Current.Request.Form["AmountTobePaid"];
            var exchangeId = HttpContext.Current.Request.Form["ExchangeId"];
            var date = HttpContext.Current.Request.Form["Date"];
            var status = HttpContext.Current.Request.Form["Status"];
            int InvoiceNo = 0;
            if (invoiceNo != null)
            {
                InvoiceNo = int.Parse (invoiceNo);
            }

            //Check if not null
            var orderInDB = _context.Orders.SingleOrDefault(c => c.InvoiceNo== InvoiceNo);
            if (orderInDB != null)
                return BadRequest();

            string photoName = "";

            if (Photo != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(Photo.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(Photo.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(Photo.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                Photo.SaveAs(savePath);
            }
            string photofile = "";
            if (qrcodeShopName != null)
            {
                photofile = Path.Combine(Path.GetDirectoryName(qrcodeShopName.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(qrcodeShopName.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(qrcodeShopName.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photofile);
                qrcodeShopName.SaveAs(savePath);
            }
            var orderDto = new OrderDto()
            {

                InvoiceNo =Int32.Parse(invoiceNo),
                ShopId =Int32.Parse(shopId),
                CustomerId = Int32.Parse(customerId),
                QrcodeShopName = photofile,
                Phone = phone,
                BankName = bankName,
                AccountNumber = accountNumber,
                AccountName = accountName,
                CurrentLocation = currentLocation,
                DeliveryTypeIn = deliveryTypeIn,
                PaymentType = paymentType,
                AmountTobePaid = decimal.Parse(amountTobePaid),
                ExchangeId = Int32.Parse(exchangeId),
                Date = DateTime.Parse(date),
                Status = status,
                ReceiptUpload = photoName
            };
            var order = Mapper.Map<OrderDto, Order>(orderDto);
            _context.Orders.Add(order);
            _context.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + orderDto.Id), orderDto);
        }

        // PUT: /api/orders/{id}
        [HttpPut]
        public IHttpActionResult UpdateProduct(int Id)
        {
            var invoiceNo = HttpContext.Current.Request.Form["InvoiceNo"];
            var shopId = HttpContext.Current.Request.Form["ShopId"];
            var customerId = HttpContext.Current.Request.Form["CustomerId"];
            //var qrcodeShopName = HttpContext.Current.Request.Form["QrcodeShopName"];
            //var qrcodeShopName = HttpContext.Current.Request.Files["QrcodeShopName"];
            var phone = HttpContext.Current.Request.Form["Phone"];
            var bankName = HttpContext.Current.Request.Form["BankName"];
            var accountNumber = HttpContext.Current.Request.Form["AccountNumber"];
            var accountName = HttpContext.Current.Request.Form["AccountName"];
            var currentLocation = HttpContext.Current.Request.Form["CurrentLocation"];
            var deliveryTypeIn = HttpContext.Current.Request.Form["DeliveryTypeIn"];
            //var Photo = HttpContext.Current.Request.Files["ReceiptUpload"];
            var paymentType = HttpContext.Current.Request.Form["PaymentType"];
            var amountTobePaid = HttpContext.Current.Request.Form["AmountTobePaid"];
            var exchangeId = HttpContext.Current.Request.Form["ExchangeId"];
            var date = HttpContext.Current.Request.Form["Date"];
            var status = HttpContext.Current.Request.Form["Status"];

            var ReceiptUpload = HttpContext.Current.Request.Files["ReceiptUpload"];
            var QrcodeShopName = HttpContext.Current.Request.Files["QrcodeShopName"];

            var receip_old = HttpContext.Current.Request.Form["receip_old"];
            var qrcode_old = HttpContext.Current.Request.Form["qrcode_old"];
            
            int InvoiceNo = 0;
            if (invoiceNo != null)
            {
                InvoiceNo = int.Parse(invoiceNo);
            }
            
            //Check if not null
            var orderInDB = _context.Orders.SingleOrDefault(c => c.Id == Id);
           
            string receip_New = "", photofile_New = "";
            receip_New = receip_old;
            photofile_New = qrcode_old;

            if (ReceiptUpload != null)
            {
                receip_New = Path.Combine(Path.GetDirectoryName(ReceiptUpload.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(ReceiptUpload.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(ReceiptUpload.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), receip_New);
                ReceiptUpload.SaveAs(savePath);
                //Delete OldPhoto
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), orderInDB.ReceiptUpload);

                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }
            }
            
            if (QrcodeShopName != null)
            {
                photofile_New = Path.Combine(Path.GetDirectoryName(QrcodeShopName.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(QrcodeShopName.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(QrcodeShopName.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photofile_New);
                QrcodeShopName.SaveAs(savePath);
                //Delete OldPhoto
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), orderInDB.QrcodeShopName);

                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }
            }
            
                var userId = User.Identity.GetUserId();

                var user = _context.Orders.SingleOrDefault(c => c.Id == Id);

                var orderDto = new OrderDto()
                {

                    //Id = Int32.Parse(Id),
                    InvoiceNo = Int32.Parse(invoiceNo),
                    ShopId = Int32.Parse(shopId),
                    CustomerId = Int32.Parse(customerId),
                    QrcodeShopName = photofile_New,
                    Phone = phone,
                    BankName = bankName,
                    AccountNumber = accountNumber,
                    AccountName = accountName,
                    CurrentLocation = currentLocation,
                    DeliveryTypeIn = deliveryTypeIn,
                    PaymentType = paymentType,
                    AmountTobePaid = decimal.Parse(amountTobePaid),
                    ExchangeId = Int32.Parse(exchangeId),
                    Date = DateTime.Parse(date),
                    Status = status,
                    ReceiptUpload = receip_New
                };
                Mapper.Map(orderDto, orderInDB);
                _context.SaveChanges();
           
            return Ok(new { });
        }


        // DELETE: /api/orders/{id}
        [HttpDelete]
        public IHttpActionResult DeleteOrder(int id)
        {
            var orderInDb = _context.Orders.SingleOrDefault(c => c.Id == id);

            if (orderInDb == null)
                return NotFound();

            _context.Orders.Remove(orderInDb);

            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
