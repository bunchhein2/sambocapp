﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.IO;
using System.Data.Entity;

namespace sambocappweb.Controllers.Api
{
    public class ShopPaymentsController : ApiController
    {
        private ApplicationDbContext _context;

        public ShopPaymentsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        [Route("api/ShopPayment")]
        //Get : api/ShopPayment
        public IHttpActionResult GetShopPayment()
        {
            //var shopPayment = _context.ShopPayments
            //                    //.Include(c => c.shop)
            //                    //.Include(c => c.currency)
            //                    .ToList();

            var shopPayment = (from sp in _context.ShopPayments
                               join s in _context.Shops on sp.shopid equals s.id
                               select new
                               {
                                   id = sp.id,
                                   date = sp.date,
                                   shopid = sp.shopid,
                                   shop = s.shopName,
                                   paytype = sp.paytype,
                                   startdate = sp.startdate,
                                   enddate = sp.enddate,
                                   amount = sp.amount,
                                   note = sp.note,
                                   currentcyid = sp.currentcyid,
                                   qtymonth = sp.qtymonth,
                                   screenshot = sp.screenshot
                               }).ToList();
            return Ok(shopPayment);
        }
        [HttpGet]
        [Route("api/ShopPayment/{id}")]
        //Get : api/ShopPayment
        public IHttpActionResult GetShopPaymentById(int id)
        {
            var shopPayment = _context.ShopPayments.SingleOrDefault(c => c.id == id);
            //var shopPayment = _context.ShopPayments
            //                  .Include(c => c.shop)
            //                  .Include(c => c.currency)
            //                  .SingleOrDefault(c => c.id == id);
            if (shopPayment == null)
                return NotFound();
            return Ok(shopPayment);
        }
        // POST: /api/departments
        [HttpPost]
        [Route("api/ShopPayment")]
        public IHttpActionResult CreateShopPayment()
        {
            var date = HttpContext.Current.Request.Form["date"];
            var shopId = HttpContext.Current.Request.Form["shopid"];
            var payType = HttpContext.Current.Request.Form["paytype"];
            var startDate = HttpContext.Current.Request.Form["startdate"];
            var endDate = HttpContext.Current.Request.Form["enddate"];
            var Amount = HttpContext.Current.Request.Form["amount"];
            var Note = HttpContext.Current.Request.Form["note"];
            var CurrencyId = HttpContext.Current.Request.Form["currentcyid"];
            var qtymonth = HttpContext.Current.Request.Form["qtymonth"];
            var screenshot = HttpContext.Current.Request.Files["screenshot"];

            string photoName = "";
            if (screenshot != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(screenshot.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(screenshot.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(screenshot.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                screenshot.SaveAs(fileSavePath);

            }

            var shoppaymentdto = new ShopPaymentDto()
            {
                //id = Int32.Parse(id),
                date = DateTime.Parse(date),
                shopid = Int32.Parse(shopId),
                paytype = payType,
                startdate = DateTime.Parse(startDate),
                enddate = DateTime.Parse(endDate),
                amount = decimal.Parse(Amount),
                note = Note,
                screenshot = photoName,
                currentcyid = int.Parse(CurrencyId),
                qtymonth=int.Parse(qtymonth)

            };

            var shopPayment = Mapper.Map<ShopPaymentDto, ShopPayment>(shoppaymentdto);
            //shopPayment.date = DateTime.Today;
            _context.ShopPayments.Add(shopPayment);
            _context.SaveChanges();

            shoppaymentdto.id = shopPayment.id;

            return Created(new Uri(Request.RequestUri + "/" + shoppaymentdto.id), shopPayment);


        }
        // PUT: /api/departments/{id}
        [HttpPut]
        [Route("api/ShopPayment/{id}")]
        public IHttpActionResult UpdateShopPayment(int id)
        {

            if (!ModelState.IsValid)
                return BadRequest();
            var date = HttpContext.Current.Request.Form["date"];
            var shopId = HttpContext.Current.Request.Form["shopid"];
            var payType = HttpContext.Current.Request.Form["paytype"];
            var startDate = HttpContext.Current.Request.Form["startdate"];
            var endDate = HttpContext.Current.Request.Form["enddate"];
            var Amount = HttpContext.Current.Request.Form["amount"];
            var Note = HttpContext.Current.Request.Form["note"];
            var CurrencyId = HttpContext.Current.Request.Form["currentcyid"];
            var qtymonth = HttpContext.Current.Request.Form["qtymonth"];
            var screenshot = HttpContext.Current.Request.Files["screenshot"];

            var shopPayInDb = _context.ShopPayments.SingleOrDefault(c => c.id == id/* && c.status == true*/);



            string photoName = "";
            if (screenshot != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(screenshot.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(screenshot.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(screenshot.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                screenshot.SaveAs(fileSavePath);
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), shopPayInDb.screenshot);
                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }
                var shoppaymentdto = new ShopPaymentDto()
                {
                    //id = id,
                    date = DateTime.Parse(date),
                    shopid = Int32.Parse(shopId),
                    paytype = payType,
                    startdate = DateTime.Parse(startDate),
                    enddate = DateTime.Parse(endDate),
                    amount = decimal.Parse(Amount),
                    note = Note,
                    screenshot = photoName,
                    currentcyid = int.Parse(CurrencyId),
                    qtymonth = int.Parse(qtymonth)
                };
                Mapper.Map(shoppaymentdto, shopPayInDb);
                _context.SaveChanges();
            }
            else
            {
                var shoppaymentdto = new ShopPaymentDto()
                {
                    //id = id,
                    date = DateTime.Parse(date),
                    shopid = Int32.Parse(shopId),
                    paytype = payType,
                    startdate = DateTime.Parse(startDate),
                    enddate = DateTime.Parse(endDate),
                    amount = decimal.Parse(Amount),
                    note = Note,
                    screenshot = shopPayInDb.screenshot,
                    currentcyid = int.Parse(CurrencyId),
                    qtymonth = int.Parse(qtymonth)
                };
                Mapper.Map(shoppaymentdto, shopPayInDb);
                _context.SaveChanges();
            }
            return Ok(new { });
        }
        // DELETE: /api/departments/{id}
        [HttpDelete]
        [Route("api/ShopPayment/{id}")]
        public IHttpActionResult DeleteShopPayment(int id)
        {

            var shopPayment = _context.ShopPayments.SingleOrDefault(c => c.id == id);
            if (shopPayment == null)
                return NotFound();

            _context.ShopPayments.Remove(shopPayment);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
