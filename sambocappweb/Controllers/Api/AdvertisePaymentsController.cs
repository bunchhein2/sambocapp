﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace sambocappweb.Controllers.Api
{
    public class AdvertisePaymentsController : ApiController
    {
        private ApplicationDbContext _context;

        public AdvertisePaymentsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        [Route("api/AdvertisePayment")]

        public IHttpActionResult GetAdvertisePayments()
        {
            var getAdvertisePayments = from ap in _context.AdvertisePayments
                                       join b in _context.Banners on ap.advertiseid equals b.id
                                       select new
                                       {
                                           id = ap.id,
                                           date=ap.date,
                                           advertiseid=ap.advertiseid,
                                           amount=ap.amount,
                                           screenshot=ap.screenshot,
                                           note=ap.note
                                       };
            return Ok(getAdvertisePayments);
        }
        [HttpGet]
        [Route("api/AdvertisePayment/{id}/{a}")]

        public IHttpActionResult GetAdvertisePaymentsIdd(int id, string a)
        {
            var getAdvertisePayments = from ap in _context.AdvertisePayments
                                       join b in _context.Banners on ap.advertiseid equals b.id
                                       where ap.advertiseid == id
                                       select new
                                       {
                                           id = ap.id,
                                           date = ap.date,
                                           advertiseid = ap.advertiseid,
                                           amount = ap.amount,
                                           screenshot = ap.screenshot,
                                           note = ap.note
                                       };
            return Ok(getAdvertisePayments);
        }

        [HttpGet]
        [Route("api/AdvertisePayment/{id}")]
        //Get : api/Customers{id}
        public IHttpActionResult GetAdvertisePaymentID(int id)
        {
            var getAdvertisePaymentsById = _context.AdvertisePayments.SingleOrDefault(c => c.id == id);

            if (getAdvertisePaymentsById == null)
                return NotFound();

            return Ok(Mapper.Map<AdvertisePayment, AdvertisePaymentDto>(getAdvertisePaymentsById));
        }
        [HttpPost]
        [Route("api/AdvertisePayment")]
        public IHttpActionResult CreateAdvertisePayments()
        {
            if (!ModelState.IsValid)
                return BadRequest();

            //var id = HttpContext.Current.Request.Form["Id"];
            var advertiseid = HttpContext.Current.Request.Form["advertiseid"];
            var date = HttpContext.Current.Request.Form["date"];
            var amount = HttpContext.Current.Request.Form["amount"];
            var note = HttpContext.Current.Request.Form["note"];
            var screenshot = HttpContext.Current.Request.Files["screenshot"];

            //var cusInDb = _context.AdvertisePayments.SingleOrDefault(c => c.name == name);

            //if (cusInDb != null)
            //    return BadRequest();

            string photoName = "";
            if (screenshot != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(screenshot.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(screenshot.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(screenshot.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                screenshot.SaveAs(fileSavePath);

            }

            var advertisePaymentDto = new AdvertisePaymentDto()
            {
                date = DateTime.Parse(date),
                advertiseid = int.Parse(advertiseid),
                amount = decimal.Parse(amount),
                note = note,
                screenshot = photoName,
            };
            var advertisePayment = Mapper.Map<AdvertisePaymentDto, AdvertisePayment>(advertisePaymentDto);
            _context.AdvertisePayments.Add(advertisePayment);
            _context.SaveChanges();

            advertisePaymentDto.id = advertisePayment.id;

            return Created(new Uri(Request.RequestUri + "/" + advertisePaymentDto.id), advertisePaymentDto);
        }

        [HttpPut]
        [Route("api/AdvertisePayment/{id}")]
        //PUT : /api/Customers/{id}
        public IHttpActionResult EditAdvertisePayments(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var advertiseid = HttpContext.Current.Request.Form["advertiseid"];
            var date = HttpContext.Current.Request.Form["date"];
            var amount = HttpContext.Current.Request.Form["amount"];
            var note = HttpContext.Current.Request.Form["note"];
            var screenshot = HttpContext.Current.Request.Files["screenshot"];

            var advInDb = _context.AdvertisePayments.SingleOrDefault(c => c.id == id/* && c.status == true*/);



            string photoName = "";
            if (screenshot != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(screenshot.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(screenshot.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(screenshot.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                screenshot.SaveAs(fileSavePath);

                //Delete OldPhoto
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), advInDb.screenshot);
                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }
                var advertisePaymentDto = new AdvertisePaymentDto()
                {
                    id = id,
                    date = DateTime.Parse(date),
                    advertiseid = int.Parse(advertiseid),
                    amount = decimal.Parse(amount),
                    note = note,
                    screenshot = photoName,
                };
                Mapper.Map(advertisePaymentDto, advInDb);
                _context.SaveChanges();
            }
            else
            {
                var advertisePaymentDto = new AdvertisePaymentDto()
                {
                    id = id,
                    date = DateTime.Parse(date),
                    advertiseid = int.Parse(advertiseid),
                    amount = decimal.Parse(amount),
                    note = note,
                    screenshot = advInDb.screenshot,
                };
                Mapper.Map(advertisePaymentDto, advInDb);
                _context.SaveChanges();
            }
            return Ok(new { });
        }

        [HttpDelete]
        [Route("api/AdvertisePayment/{id}")]
        //PUT : /api/Customers/{id}
        public IHttpActionResult DeleteAdvertisePays(int id)
        {
            var AdvertisePaysInDb = _context.AdvertisePayments.SingleOrDefault(c => c.id == id);
            if (AdvertisePaysInDb == null)
                return NotFound();
            _context.AdvertisePayments.Remove(AdvertisePaysInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
