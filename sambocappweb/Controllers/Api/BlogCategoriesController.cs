﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class BlogCategoriesController : ApiController
    {
        private ApplicationDbContext _context;
        public BlogCategoriesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: /api/blogcategories
        [HttpGet]
        public IHttpActionResult GetBlogCategories()
        {
            return Ok(_context.BlogCategories.ToList().Select(Mapper.Map<BlogCategory, BlogCategoryDto>));
        }

        // GET: /api/blogcategories/{id}
        [HttpGet]
        public IHttpActionResult GetBlogCategory(int id)
        {
            var blogCategory = _context.BlogCategories.SingleOrDefault(c => c.Id == id);

            if (blogCategory == null)
                return NotFound();

            return Ok(Mapper.Map<BlogCategory, BlogCategoryDto>(blogCategory));
        }

        // POST: /api/blogcategories
        [HttpPost]
        public IHttpActionResult CreateBlogCategory(BlogCategoryDto blogCategoryDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.BlogCategories.SingleOrDefault(c => c.Name == blogCategoryDto.Name);

            if (isExist != null)
                return BadRequest();

            var blogCategory = Mapper.Map<BlogCategoryDto, BlogCategory>(blogCategoryDto);

            _context.BlogCategories.Add(blogCategory);

            _context.SaveChanges();

            blogCategoryDto.Id = blogCategory.Id;

            return Created(new Uri(Request.RequestUri + "/" + blogCategoryDto.Id), blogCategoryDto);
        }

        // PUT: /api/blogcategories/{id}
        [HttpPut]
        public IHttpActionResult UpdateBlogCategory(int id, BlogCategoryDto blogCategoryDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var isExist = _context.BlogCategories.SingleOrDefault(c => c.Name == blogCategoryDto.Name && c.Id != blogCategoryDto.Id);

            if (isExist != null)
                return BadRequest();

            var blogCategoryInDb = _context.BlogCategories.SingleOrDefault(c => c.Id == id);

            if (blogCategoryInDb == null)
                return NotFound();

            Mapper.Map(blogCategoryDto, blogCategoryInDb);

            _context.SaveChanges();

            return Ok(blogCategoryDto);
        }

        // DELETE: /api/blogcategories/{id}
        [HttpDelete]
        public IHttpActionResult DeleteBlogCategory(int id)
        {
            var blogCategoryInDb = _context.BlogCategories.SingleOrDefault(c => c.Id == id);

            if (blogCategoryInDb == null)
                return NotFound();

            _context.BlogCategories.Remove(blogCategoryInDb);

            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
