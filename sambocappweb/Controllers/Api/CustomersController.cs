﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class CustomersController : ApiController
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        [Route("api/Customers/{aa}/{bb}")]
        public IHttpActionResult exid(String aa, String bb)
        {
            DataTable ds = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection conx = new SqlConnection(connectionString);
            SqlDataAdapter adp = new SqlDataAdapter("SELECT TOP 1 tokenid FROM tbl_customer order by id desc", conx);
            adp.Fill(ds);
            string Exid = ds.Rows[0][0].ToString();
            return Ok(Exid);

        }
        [HttpGet]
        [Route("api/Customers")]
        //Get : api/Customers
        public IHttpActionResult GetCustomer()
        {
            var getCompanyType = _context.Customers.ToList().Select(Mapper.Map<Customer, CustomerDto>);

            return Ok(getCompanyType);

            //var customer = (from c in _context.Customers
            //                //join l in _context.Locations on c.currentLocation equals l.id
            //                select new CustomerDto
            //                {
            //                    id = c.id,
            //                    tokenid = c.tokenid,
            //                    gender=c.gender,
            //                    customerName = c.customerName,
            //                    phone = c.phone,
            //                    date =c.date,
            //                    currentLocation = c.currentLocation
            //                }).ToList();
            //return Ok(customer);
        }


        [HttpGet]

        //Get : api/companyTypes{id}
        [Route("api/Customers/{id}")]
        public IHttpActionResult GetCustomer(int id)
        {
            var getCustomerById = _context.Customers.SingleOrDefault(c => c.id == id);

            if (getCustomerById == null)
                return NotFound();

            return Ok(Mapper.Map<Customer, CustomerDto>(getCustomerById));
        }
        [HttpPost]
        [Route("api/Customers")]
        public IHttpActionResult CreateCompany()
        {
            //var id = HttpContext.Current.Request.Form["id"];
            var date = HttpContext.Current.Request.Form["date"];
            var phone = HttpContext.Current.Request.Form["phone"];
            var tokenid = HttpContext.Current.Request.Form["tokenid"];
            var currentLocation = HttpContext.Current.Request.Form["currentLocation"];
            var customerName = HttpContext.Current.Request.Form["customerName"];
            var gender = HttpContext.Current.Request.Form["gender"];
            var password = HttpContext.Current.Request.Form["password"];
            var imageProfile = HttpContext.Current.Request.Files["imageProfile"];
            
            //Check if not null
            var isExist = _context.Customers.FirstOrDefault(c => c.customerName == customerName);
            if (isExist != null)
                return BadRequest();

            string photoName = "";

            if (imageProfile != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(imageProfile.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(imageProfile.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(imageProfile.FileName)
                    ));
                var savePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                imageProfile.SaveAs(savePath);
            }

            var customerDto = new CustomerDto()
            {
                //id = Int32.Parse(id),
                date = DateTime.Parse(date),
                phone = phone,
                tokenid = tokenid,
                gender = gender,
                currentLocation = currentLocation,
                customerName = customerName,
                password=password,
                imageProfile = photoName
            };
            var customer = Mapper.Map<CustomerDto, Customer>(customerDto);
            _context.Customers.Add(customer);
            _context.SaveChanges();

            return Created(new Uri(Request.RequestUri + "/" + customerDto.id), customerDto);
        }

        [HttpPut]
        [Route("api/Customers/{id}")]
        public IHttpActionResult UpdateCustomers(int id)
        {
            //var id = HttpContext.Current.Request.Form["id"];
            var date = HttpContext.Current.Request.Form["date"];
            var phone = HttpContext.Current.Request.Form["phone"];
            var tokenid = HttpContext.Current.Request.Form["tokenid"];
            var currentLocation = HttpContext.Current.Request.Form["currentLocation"];
            var customerName = HttpContext.Current.Request.Form["customerName"];
            var gender = HttpContext.Current.Request.Form["gender"];
            var password = HttpContext.Current.Request.Form["password"];
            var imageProfile = HttpContext.Current.Request.Files["imageProfile"];
            var photo_old = HttpContext.Current.Request.Form["photo_old"];
            
            var isExist = _context.Customers.SingleOrDefault(c =>  c.id == id);
            
            string photoName = "";
            photoName = photo_old;
            if (imageProfile != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(imageProfile.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(imageProfile.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(imageProfile.FileName)
                    ));
                var photoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                imageProfile.SaveAs(photoPath);

                //Delete Old Photo path
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), isExist.imageProfile);
                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }

            }
                var customerDto = new CustomerDto()
                {
                    id = id,
                    date = DateTime.Parse(date),
                    phone = phone,
                    tokenid = tokenid,
                    gender = gender,
                    currentLocation = currentLocation,
                    customerName = customerName,
                    password=password,
                    imageProfile = photoName
                };

                Mapper.Map(customerDto, isExist);
                _context.SaveChanges();
            
            return Ok(new { });
        }

        [HttpDelete]
        [Route("api/Customers/{id}")]
        //DELETE : /api/companyTypes/{id}
        public IHttpActionResult DeleteCustomer(int id)
        {

            var customerInDb = _context.Customers.SingleOrDefault(c => c.id == id);
            if (customerInDb == null)
                return NotFound();
            _context.Customers.Remove(customerInDb);
            _context.SaveChanges();

            return Ok(new { });
        }

    }
}
