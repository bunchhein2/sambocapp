﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class FeedbacksController : ApiController
    {
        private ApplicationDbContext _context;
        public FeedbacksController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: /api/feedbacks
        [HttpGet]
        public IHttpActionResult GetFeedbacks()
        {
            return Ok(_context.Feedbacks.ToList().Select(Mapper.Map<Feedback, FeedbackDto>));
        }

        // GET: /api/feedbacks/{id}
        [HttpGet]
        public IHttpActionResult GetFeedback(int id)
        {
            var feedback = _context.Feedbacks.SingleOrDefault(c => c.Id == id);

            if (feedback == null)
                return NotFound();

            return Ok(Mapper.Map<Feedback, FeedbackDto>(feedback));
        }

        // POST: /api/feedbacks
        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult CreateFeedback(FeedbackDto feedbackDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection cn = new SqlConnection(connectionString);
            cn.Open();
            var cmd = new SqlCommand("proc_Create_Feedback", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@Name", HttpUtility.HtmlEncode(feedbackDto.Name));
            cmd.Parameters.AddWithValue("@Subject", HttpUtility.HtmlEncode(feedbackDto.Subject));
            cmd.Parameters.AddWithValue("@Email", HttpUtility.HtmlEncode(feedbackDto.Email));
            cmd.Parameters.AddWithValue("@Message", HttpUtility.HtmlEncode(feedbackDto.Message));
            try
            {
                cmd.ExecuteNonQuery();
                cn.Close();
                return Ok(new { });
            }
            catch (Exception)
            {
                cn.Close();
                return BadRequest();
            }

            //var feedback = Mapper.Map<FeedbackDto, Feedback>(feedbackDto);

            //_context.Feedbacks.Add(feedback);

            //_context.SaveChanges();

            //feedbackDto.Id = feedback.Id;

            //return Created(new Uri(Request.RequestUri + "/" + feedbackDto.Id), feedbackDto);
        }

        // PUT: /api/feedbacks/{id}
        [HttpPut]
        public IHttpActionResult UpdateFeedback(int id, FeedbackDto feedbackDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var feedbackInDb = _context.Feedbacks.SingleOrDefault(c => c.Id == id);

            if (feedbackInDb == null)
                return NotFound();

            Mapper.Map(feedbackDto, feedbackInDb);

            _context.SaveChanges();

            return Ok(feedbackDto);
        }

        // DELETE: /api/feedbacks/{id}
        [HttpDelete]
        public IHttpActionResult DeleteFeedback(int id)
        {
            var feedbackInDb = _context.Feedbacks.SingleOrDefault(c => c.Id == id);

            if (feedbackInDb == null)
                return NotFound();

            _context.Feedbacks.Remove(feedbackInDb);

            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
