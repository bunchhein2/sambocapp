﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class ReportsController : ApiController
    {
        private ApplicationDbContext _context;
        public ReportsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: /api/reports
        [HttpGet]
        public IHttpActionResult GetReports()
        {
            var userId = User.Identity.GetUserId();
            return Ok(_context.Reports.ToList().Select(Mapper.Map<Report, ReportDto>).Where(c => c.UserId == userId));
        }

        // GET: /api/reports/{id}
        [HttpGet]
        public IHttpActionResult GetReport(int id)
        {
            var userId = User.Identity.GetUserId();
            var report = _context.Reports
                            .Include(c => c.User)
                            .Include(c => c.ReportType)
                            //.Include(c => c.CivilServantFrom)
                            //.Include(c => c.CivilServantTo)
                            .SingleOrDefault(c => c.Id == id && c.UserId == userId);

            if (report == null)
                return NotFound();

            return Ok(Mapper.Map<Report, ReportDto>(report));
        }

        // POST: /api/reports
        [HttpPost]
        public IHttpActionResult CreateReport(ReportDto reportDto)
        {
            var report = Mapper.Map<ReportDto, Report>(reportDto);

            var userId = User.Identity.GetUserId();

            report.UserId = userId;

            _context.Reports.Add(report);

            _context.SaveChanges();

            reportDto.Id = report.Id;

            return Created(new Uri(Request.RequestUri + "/" + reportDto.Id), reportDto);
        }

        // PUT: /api/reports/{id}
        [HttpPut]
        public IHttpActionResult UpdateReport(int id, ReportDto reportDto)
        {
            var reportInDb = _context.Reports.SingleOrDefault(c => c.Id == id);

            if (reportInDb == null)
                return NotFound();

            Mapper.Map(reportDto, reportInDb);

            var userId = User.Identity.GetUserId();

            reportInDb.UserId = userId;

            _context.SaveChanges();

            return Ok(reportDto);
        }

        // DELETE: /api/reports/{id}
        [HttpDelete]
        public IHttpActionResult DeleteReport(int id)
        {
            var reportInDb = _context.Reports.SingleOrDefault(c => c.Id == id);

            if (reportInDb == null)
                return NotFound();

            _context.Reports.Remove(reportInDb);

            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
