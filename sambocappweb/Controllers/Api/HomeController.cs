﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class HomeController : ApiController
    {
        private ApplicationDbContext _context;
        public HomeController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: /api/home/getAnnouncements
        
        [HttpGet]
        [Route("api/Home")]
        public IHttpActionResult Getcount()
        {
            var count = _context.Shops.ToList().Select(Mapper.Map<Shop, ShopDto>);
            return Ok(count);
        }
    }
}
