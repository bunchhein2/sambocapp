﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace sambocappweb.Controllers.Api
{
    public class QrcodesController : ApiController
    {
        private ApplicationDbContext _context;

        public QrcodesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpGet]
        [Route("api/Qrcode")]

        public IHttpActionResult GetQrcodes()
        {
            var getQrcodes = _context.Qrcodes.ToList().Select(Mapper.Map<Qrcode, QrcodeDto>);
            return Ok(getQrcodes);
        }


        [HttpGet]
        [Route("api/Qrcode/{id}")]
        //Get : api/Customers{id}
        public IHttpActionResult GetQrcodeID(int id)
        {
            var getQrcodesById = _context.Qrcodes.SingleOrDefault(c => c.id == id);

            if (getQrcodesById == null)
                return NotFound();

            return Ok(Mapper.Map<Qrcode, QrcodeDto>(getQrcodesById));
        }
        [HttpPost]
        [Route("api/Qrcode")]
        public IHttpActionResult CreateQrcodes()
        {
            if (!ModelState.IsValid)
                return BadRequest();

            //var id = HttpContext.Current.Request.Form["Id"];
            var qrcode = HttpContext.Current.Request.Files["qrcode"];
            var createdate = DateTime.Today;
            var createby = User.Identity.GetUserName();

            //var cusInDb = _context.AdvertisePayments.SingleOrDefault(c => c.name == name);

            //if (cusInDb != null)
            //    return BadRequest();

            string photoName = "";
            if (qrcode != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(qrcode.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(qrcode.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(qrcode.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                qrcode.SaveAs(fileSavePath);

            }

            var qrcodeDto = new QrcodeDto()
            {
                
                qrcode = photoName,
                createby=createby,
                createdate=createdate
            };
            var qrCode = Mapper.Map<QrcodeDto, Qrcode>(qrcodeDto);
            _context.Qrcodes.Add(qrCode);
            _context.SaveChanges();

            qrcodeDto.id = qrCode.id;

            return Created(new Uri(Request.RequestUri + "/" + qrcodeDto.id), qrcodeDto);
        }

        [HttpPut]
        [Route("api/Qrcode/{id}")]
        //PUT : /api/Customers/{id}
        public IHttpActionResult EditQrcodes(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var qrcode = HttpContext.Current.Request.Files["qrcode"];
            var createdate = DateTime.Today;
            var createby = User.Identity.GetUserName();

            var qrcodeInDb = _context.Qrcodes.SingleOrDefault(c => c.id == id/* && c.status == true*/);



            string photoName = "";
            if (qrcode != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(qrcode.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(qrcode.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(qrcode.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                qrcode.SaveAs(fileSavePath);

                //Delete OldPhoto
                var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), qrcodeInDb.qrcode);
                if (File.Exists(oldPhotoPath))
                {
                    File.Delete(oldPhotoPath);
                }
                var qrcodeDto = new QrcodeDto()
                {
                    id = id,
                    qrcode = photoName,
                    createby = createby,
                    createdate = createdate
                };
                Mapper.Map(qrcodeDto, qrcodeInDb);
                _context.SaveChanges();
            }
            else
            {
                var qrcodeDto = new QrcodeDto()
                {
                    id = id,
                    qrcode = qrcodeInDb.qrcode,
                    createby = createby,
                    createdate = createdate
                };
                Mapper.Map(qrcodeDto, qrcodeInDb);
                _context.SaveChanges();
            }
            return Ok(new { });
        }

        [HttpDelete]
        [Route("api/Qrcode/{id}")]
        //PUT : /api/Customers/{id}
        public IHttpActionResult DeleteQrcode(int id)
        {
            var QrcodesInDb = _context.Qrcodes.SingleOrDefault(c => c.id == id);
            if (QrcodesInDb == null)
                return NotFound();
            _context.Qrcodes.Remove(QrcodesInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
