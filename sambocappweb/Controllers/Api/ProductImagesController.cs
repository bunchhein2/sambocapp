﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.IO;
using System.Data.Entity.Validation;

namespace sambocappweb.Controllers.Api
{
    public class ProductImagesController : ApiController
    {
        private ApplicationDbContext _context;

        public ProductImagesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: /api/ProductImages
        [HttpGet]
        public IHttpActionResult GetImage()
        {
            //var productimage = _context.ProductImages.ToList().Select(Mapper.Map<ProductImage, ProductImageDto>);
            //return Ok(productimage);

            var productimage = (from b in _context.ProductImages
                                join u in _context.Products on b.productid equals u.Id
                                //join s in _context.Products on b.id equals s.ShopId
                                //join sh in _context.Shops on b.id equals sh.id
                                select new
                                {
                                    id = b.id,
                                    productid =b.productid ,
                                    productname = u.ProductName,
                                    //shopname = s.ShopId + "" + sh.shopName,
                                    productimage = b.productimage
                                }).ToList();
            return Ok(productimage);
        }
        // GET: /api/ProductImages/{id}
        [HttpGet]
        public IHttpActionResult GetProductimages(int id)
        {
            var prouducts = _context.ProductImages.SingleOrDefault(c => c.id == id);
            if (prouducts == null)
                return NotFound();

            return Ok(Mapper.Map<ProductImage, ProductImageDto>(prouducts));
        }
        //POS : /api/ProductImages   for Insert record
        public IHttpActionResult CreateProductImage()
        {
            var productid = HttpContext.Current.Request.Form["productid"];
            var productimage = HttpContext.Current.Request.Files["productimage"];

            string photoName = "";
            if (productimage != null)
            {
                photoName = Path.Combine(Path.GetDirectoryName(productimage.FileName)
                    , string.Concat(Path.GetFileNameWithoutExtension(productimage.FileName)
                    , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                    , Path.GetExtension(productimage.FileName)
                    ));
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                productimage.SaveAs(fileSavePath);

            }

            var productimageDto = new ProductImageDto()
            {
                //id = Int32.Parse(id),
                productid = Int32.Parse(productid),
                productimage = photoName,
            };


            try
            {
                var image = Mapper.Map<ProductImageDto, ProductImage>(productimageDto);
                if (photoName.Length < 1000000)
                {
                _context.ProductImages.Add(image);
                _context.SaveChanges();

                    productimageDto.id = image.id;
                }
                else
                {
                    
                }
                

                return Created(new Uri(Request.RequestUri + "/" + productimageDto.id), image);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

            
        }

        // PUT: /api/ProductImages/{id}
        [HttpPut]
        public IHttpActionResult UpdateProduct(int Id)
        {
            var id = HttpContext.Current.Request.Form["Imamgeid"];
            var productid = HttpContext.Current.Request.Form["productid"];
            var productimage = HttpContext.Current.Request.Files["productimage"];
            var file_old = HttpContext.Current.Request.Form["file_old"];


            var ProImgInDb = _context.ProductImages.SingleOrDefault(c => c.id == Id);

            string photoName = "";
            photoName = file_old;
            if (productimage != null)
            {
                if (productimage != null)
                {
                    photoName = Path.Combine(Path.GetDirectoryName(productimage.FileName)
                            , string.Concat(Path.GetFileNameWithoutExtension(productimage.FileName)
                            , DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss")
                            , Path.GetExtension(productimage.FileName)
                            ));
                    var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), photoName);
                    productimage.SaveAs(fileSavePath);

                    //Delete OldPhoto
                    var oldPhotoPath = Path.Combine(HttpContext.Current.Server.MapPath("~/Images"), ProImgInDb.productimage);
                    if (File.Exists(oldPhotoPath))
                    {
                        File.Delete(oldPhotoPath);
                    }
                    var ProimgDtm = new ProductImageDto()
                    {
                        id = Id,
                        productid = Int32.Parse(productid),
                        productimage = photoName,

                    };
                    Mapper.Map(ProimgDtm, ProImgInDb);
                    _context.SaveChanges();
                }

            }
            else
            {

                var ProimgDtm = new ProductImageDto()
                {
                    id = Id,
                    productid = Int32.Parse(productid),
                    productimage = photoName,
                };
                Mapper.Map(ProimgDtm, ProImgInDb);
                _context.SaveChanges();

            }
            return Ok(new { });
            
        }


        // DELETE: /api/ProductImages/{id}
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var ProductInDb = _context.ProductImages.SingleOrDefault(c => c.id == id);

            if (ProductInDb == null)
                return NotFound();

            _context.ProductImages.Remove(ProductInDb);

            _context.SaveChanges();

            return Ok(new { });
        }
    }
}
