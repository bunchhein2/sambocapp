﻿using AutoMapper;

using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Globalization;
using System.Web.Http.Results;
//using System.Web.Mvc;

namespace sambocappweb.Controllers.Api
{
    [Authorize]
    public class ExchangesController : ApiController
    {
        private ApplicationDbContext _context;
        public ExchangesController()
        {
            _context = new ApplicationDbContext();

        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        //GET : /api/Exchanges?currencyid={de..id}  for get all record
        [HttpGet]
        public IHttpActionResult GetExchange()
        {
            var exchange = (from b in _context.Exchanges
                          join u in _context.Currencies on b.currencyid equals u.id
                          join s in _context.Shops on b.shopid equals s.id 
                          select new
                          {
                              id = b.id,
                              date = b.date,
                              currencyid = u.id,
                              currencyname = u.currencyname ,
                              shopid = b.shopid,
                              shopname =s.shopName ,
                              rate = b.rate,
                          }).ToList();
            return Ok(exchange);
            //return Ok(_context.Exchanges.ToList().Select(Mapper.Map<Exchange, ExchangeDto>));
            //.Where(c => c.currencyid == true ,false));

        }


        //GET : /api/Exchanges/{id} for get record by id
        [HttpGet]
        public IHttpActionResult GetExchange(int id)
        {
            var currencyid = _context.Exchanges.SingleOrDefault(c => c.id == id);
            if (currencyid == null)
                return NotFound();

            return Ok(Mapper.Map<Exchange, ExchangeDto>(currencyid));
        }




        //POS : /api/Exchanges  for Insert record
        [HttpPost]
        public IHttpActionResult CreateExchange()
        {

            //var id = HttpContext.Current.Request.Form["exchangeid"];
            var date = HttpContext.Current.Request.Form["exchangedate"];
            var currencyid = HttpContext.Current.Request.Form["exchangecurrencyid"];
            var shopid = HttpContext.Current.Request.Form["exchangeshopid"];
            var rate = HttpContext.Current.Request.Form["exchangerate"];

            var ExchangeDto = new ExchangeDto()
            {
                //id = Int32.Parse(id),
                date = DateTime.Parse(date),
                currencyid = Int32.Parse(currencyid),
                shopid = Int32.Parse(shopid),
                rate = decimal.Parse(rate),
            };


            try
            {
                var Exchange = Mapper.Map<ExchangeDto, Exchange>(ExchangeDto);
                _context.Exchanges.Add(Exchange);
                _context.SaveChanges();

                ExchangeDto.id = Exchange.id;

                return Created(new Uri(Request.RequestUri + "/" + ExchangeDto.id), Exchange);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }


        //PUT : /api/Exchanges/{id}  for Update record
        [HttpPut]
        public IHttpActionResult UpdateExchange(int id)
        {
            //var id = HttpContext.Current.Request.Form["exchangeid"];
            var date = HttpContext.Current.Request.Form["exchangedate"];
            var currencyid = HttpContext.Current.Request.Form["exchangecurrencyid"];
            var shopid = HttpContext.Current.Request.Form["exchangeshopid"];
            var rate = HttpContext.Current.Request.Form["exchangerate"];

            var ExchangeInDb = _context.Exchanges.SingleOrDefault(c => c.id == id);
            var ExchangeDto = new ExchangeDto()
            {
                id =id,
                date = DateTime.Parse(date),
                currencyid = Int32.Parse(currencyid),
                shopid = Int32.Parse(shopid),
                rate = decimal.Parse(rate),

            };
            Mapper.Map(ExchangeDto, ExchangeInDb);
            _context.SaveChanges();

            return Ok(new { });

        }

        //DELETE : /api/Exchanges/{id}  for Delete record
        [HttpDelete]
        public IHttpActionResult DeleteExchange(int id)
        {
            var ExchangeInDb = _context.Exchanges.SingleOrDefault(c => c.id == id);
            if (ExchangeInDb == null)
                return NotFound();

            _context.Exchanges.Remove(ExchangeInDb);
            _context.SaveChanges();

            return Ok(new { });
        }
        
    }
}