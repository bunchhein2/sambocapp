﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sambocappweb.Controllers
{
    public class LoginHistoriesController : Controller
    {
        private ApplicationDbContext _context;
        public LoginHistoriesController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: LoginHistories
        public ActionResult Index()
        {
            var loginHistory = _context.LoginHistories.ToList().OrderByDescending(c => c.Id);
            return View(loginHistory);
        }
    }
}