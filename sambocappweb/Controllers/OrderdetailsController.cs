﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sambocappweb.Controllers
{
    public class OrderdetailsController : Controller
    {
        private ApplicationDbContext _context;
        public OrderdetailsController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Orderdetails
        public ActionResult Index()
        {
            List<SelectListItem> orderList = new List<SelectListItem>();
            var odedr = (from od in _context.Orders
                         join c in _context.Customers on od.CustomerId equals c.id
                         select new
                         {
                             od.Id,
                             od.Date,
                             c.customerName
                         }

                       ).ToList();
             foreach(var item in odedr)
            {
                SelectListItem temp = new SelectListItem();
                temp.Text = "ID: " + item.Id + " ( Name:" + item.customerName + ")";
                temp.Value = item.Id.ToString();
                orderList.Add(temp);
            }
            
            ViewBag.Orderlist = new SelectList(_context.Products, "Id", "ProductName");
            //ViewBag.Orderidlist = new SelectList(_context.Orders, "Id", "Date");
            ViewBag.Orderidlist = orderList;

            return View();
        }
    }
}