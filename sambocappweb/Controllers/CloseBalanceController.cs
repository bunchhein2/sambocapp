﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using sambocappweb.Models;

namespace sambocappweb.Controllers
{
    public class CloseBalanceController : Controller
    {
        // GET: CloseBalance
        ApplicationDbContext _context = new ApplicationDbContext();
        public CloseBalanceController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}