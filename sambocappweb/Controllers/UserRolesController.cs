﻿using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using sambocappweb.ViewModels;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace sambocappweb.Controllers
{
    [Authorize]
    public class UserRolesController : Controller
    {
        private ApplicationDbContext _context;
        private ApplicationUserManager _userManager;

        public UserRolesController()
        {
            _context = new ApplicationDbContext();
        }

        public UserRolesController(ApplicationUserManager userManager)
		{
			UserManager = userManager;
		}

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: UserRoles
        [Route("manage-user-roles")]
        public ActionResult Index()
        {
            var users = _context.Users.ToList();

            return View(users);
        }

        // GET: /users/{id}
        public ActionResult GetUser(string id)
        {
            var roleStore = new RoleStore<IdentityRole>(new ApplicationDbContext());
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var user = _context.Users.Include(c => c.Roles).SingleOrDefault(c => c.Id == id);

            var roles = roleManager;

            var viewModel = new UserRoleViewModel()
            {
                User = user,
                Role = roles
            };

            return View("AssignRoles", viewModel);
        }

        [HttpGet]
        public ActionResult EditUser(string id)
        {
            var user = _context.Users.SingleOrDefault(c => c.Id == id);

            if (user == null)
                return HttpNotFound();

            var viewModel = new RegisterViewModel()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                FullNameInKhmer = user.FullNameInKhmer,
                Gender = user.Gender,
                Email = user.Email,
                ContactPhone = user.ContactPhone,
                Position = user.Position,
                TelegramId = user.TelegramId,
                IsSuperUser = user.IsSuperUser
            };

            return View("EditUser", viewModel);
        }

        public ActionResult DeleteUser(string id)
        {
            var user = UserManager.FindById(id);

            UserManager.Delete(user);

            return RedirectToAction("Index", "UserRoles");
        }
    }
}