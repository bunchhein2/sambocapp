﻿using sambocappweb.Models;
using sambocappweb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace sambocappweb.Controllers
{
    public class ShopsController : Controller
    {
        private ApplicationDbContext _context;
        private ApplicationDbContext db = new ApplicationDbContext();
        public ShopsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Shops
        [Route("manage-shop")]
        public ActionResult Index()
        {
            var viewmodol = new Manage_ShopViewModels()
            {
                SetupFees = _context.SetupFees.ToList(),
            };

            ViewBag.PaymenttypeList = new SelectList(db.PaymentMethods, "id", "methodname");


            //ViewBag.Total = new SelectList(db.Orders, "id", "InvoiceNo");
            ViewBag.Orderlist = new SelectList(db.Products, "Id", "ProductName");
            count();
            return View(viewmodol);
        }
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult create(int id = 0)
        {
            var Pro = new Customer();
            var Code = db.Shops.OrderByDescending(c => c.id).FirstOrDefault();
            if (id != 0)
            {
                Code = db.Shops.Where(x => x.id == id).FirstOrDefault<Shop>();
            }
            else if (Code == null)
            {
                Pro.tokenid = "2022";

            }
            else
            {
                Pro.tokenid = "" + (Convert.ToInt32(Pro.tokenid.Substring(9, Pro.tokenid.Length - 9)) + 1).ToString();
            }
            return View(Pro);
        }
        public void count()
        {
            var viewbage = db.Shops.Count();
            var view1 = viewbage + 1;
            ViewBag.Count = "2022" + view1;

        }
    }
}