﻿using AutoMapper;
using sambocappweb.Dtos;
using sambocappweb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace sambocappweb.Controllers
{
    public class CurrenciesController : Controller
    {
        private ApplicationDbContext _context;
        public CurrenciesController()
        {
            _context = new ApplicationDbContext();

        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Currencies
        public ActionResult Index()
        {
            return View();
        }


    }
}